import * as constants from '../constants/expedition'

const initialState = {
  expeditions: [],
  barcodes: [],
  query: {
    fromDate: new Date().toISOString(),
    toDate: new Date().toISOString(),
  },
  expedition: {
    // expedition entity
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_EXPEDITIONS:
      return { ...state, expeditions: action.payload.expeditions }
    case constants.SET_BARCODES:
      return { ...state, barcodes: action.payload.barcodes }
    default:
      return state
  }
}
