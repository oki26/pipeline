// @flow
import type { ShopType } from '../../../types'
import React from "react";
import ReactTable from "react-table";
import qrcode from 'qrcode'
import { connect } from 'react-redux'
import checkboxHOC from "react-table/lib/hoc/selectTable";
import {
	Card, CardBody, CardTitle,
	Button, Col, InputGroup, InputGroupAddon,
	Row, CardSubtitle, Input
} from 'reactstrap';
import moment from "moment"
import Picker from 'rc-calendar/lib/Picker'
import RangeCalendar from 'rc-calendar/lib/RangeCalendar'
import enUS from 'rc-calendar/lib/locale/en_US'
import "react-table/react-table.css";
import Backendless from '../../../services/backendless'
// import { history } from '../../../index'

const CheckboxTable = checkboxHOC(ReactTable);
type Props = {
	account: Object,
}

type RowDataType = {
	objectId: ?string,
	store_name: ?string,
	description: ?string,
	email: ?string,
	phone_number: ?string,
	rating: ?number,
	created: ?number,
	order: ShopType,
}

type State = {
	isOpenModal: boolean,
	modalType: ?string,
	loading: boolean,
	obj: Object,
	orders: Array<RowDataType>,
	loadingOrders: boolean,
	loadingDetail: boolean,
	dateRange: Array<string>,
	searchText: string,
	selection: Array<string>,
	selectionOrder: Array<ShopType>,
	selectAll: boolean,
	pages: number,
  pageSize: number,
  page: number,
}

const tableHeaders = [
	{ Header: "ID", accessor: "objectId"	},
	{ Header: "Nama Toko", accessor: "store_name" },
	{ Header: "Deskripsi", accessor: "description" },
	{ Header: "Email", accessor: "email" },
	{ Header: "No Telfon", accessor: "phone_number" },
	{ Header: "Rating", accessor: "rating" },
	{ Header: "Tanggal Dibuat", accessor: "created" }
]

const calendar = (
	<RangeCalendar
		showWeekNumber={false}
		dateInputPlaceholder={['Dari Tanggal', 'Sampai Tanggal']}
		defaultValue={[moment().subtract(1, 'month').format(), moment()]}
		locale={enUS}
		disabledDate={current => current.isAfter(moment().add(1, 'days').hour(0))}
	/>
);

class ListToko extends React.Component<Props, State> {
	tableRef = null
	constructor(props: Props) {
		super(props);
		this.state = {
			loading: false,
			modalType: '',
			obj: {},
			orders: [],	
			loadingOrders: false,
			loadingDetail: false,
			dateRange: [],
			searchText: '',
			selectAll: false,
			selection: [],
			selectionOrder: [],
			pageSize: 20,
			isOpenModal: false,
			orderOfBag: [],
			pages: -1,
			page: 0,
		};
	}

	/* retrieve data orders */
	getOrders = async (
		pageSize: number = 20, offset: number = 0, page: number = 0,
		sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		try {
			this.setState({ loadingOrders: true })
			const { dateRange, searchText } = this.state
			
			// set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(2).setSortBy(sortQuery)
				// .setWhereClause(`bagging is null`)

			// filter by date
			if (dateRange.length === 2) {
					// dateRange.sort((a, b) => moment(a).isAfter(b) ? -1 : 1)
					queryBuilder.setWhereClause(`created >= "${moment(dateRange[0]).format('YYYY-MMM-DD')}" and created <= "${moment(dateRange[1]).format('YYYY-MMM-DD')}"`)
			}
			
			// query search
			if (searchText !== '') {
				queryBuilder.setWhereClause(`
					name LIKE '%${searchText}%' or
					email LIKE '%${searchText}%' or
					phone_number LIKE '%${searchText}%'`)
			}
			const res: Array<ShopType> = await Backendless.Data.of('tripstore_shop').find(queryBuilder)
			const count: number = await Backendless.Data.of('tripstore_shop').getObjectCount(queryBuilder)
			const transformed: Array<RowDataType> =  res.map((data: ShopType, index): RowDataType => {
				return ({
					_id: data.objectId,
					objectId: data.objectId,
					store_name: data.name ? data.name : 'Null',
					description: data.description ? data.description : null,
					email: data.email ? data.email : null,
					phone_number: data.phone_number ? data.phone_number : null,
					rating: data.rating ? data.rating : 0,
					created: data.created ? moment(data.created).format("DD MMMM YYYY") : null,
					order: data
				})
			})
			this.setState({
				orders: transformed, loadingOrders: false,
				pages: count / offset, pageSize: pageSize, page: page
			})
		} catch(er) {
			this.setState({ loadingOrders: false })
			console.log(er)
		}
	}

	/** navigate to order detail page */
	// goDetailPage = (event: Event, row: { original: RowDataType }) => {
	// 	event.preventDefault();
	// 	history.push(`/order/${row.original.objectId || 'null'}`)
	// }

	timeout = null
	onChage = (e: Object) => {
		this.setState({ [e.target.name]: e.target.value }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getOrders, 1000)
		})
	}


  toggleSelection = async (key: string, shift: any, row: Object) => {
		let selection = [...this.state.selection];
		let selectionOrder = [...this.state.selectionOrder]
    const keyIndex = selection.indexOf(key);
    if (keyIndex >= 0) {
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ];
      selectionOrder = [
        ...selectionOrder.slice(0, keyIndex),
        ...selectionOrder.slice(keyIndex + 1)
      ];
    } else {
			selection.push(key);
			try {
				const barcode_uri = await qrcode.toDataURL(row.order.tracking_id)
				selectionOrder.push({ ...row.order, barcode_uri })
			} catch (er) {}
    }
    this.setState({ selection, selectionOrder });
  };

	/** toggle selection row at table */
  toggleAll = async () => {
    const selectAll = this.state.selectAll ? false : true;
    const selection = [];
		const selectionOrder = [];
		const promises = []
    if (selectAll && this.tableRef !== null) {
      const wrappedInstance = this.tableRef.getWrappedInstance();
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      currentRecords.forEach((item) => {
				selection.push(item._original._id);
				selectionOrder.push(item._original.order);
				if (item._original.order.tracking_id) {
					promises.push(qrcode.toDataURL(item._original.order.tracking_id))
				} else {
					promises.push(Promise.resolve(''))
				}
			});
			const results = await Promise.all(promises)
			results.forEach((barcode_uri, idx) => {
				selectionOrder[idx] = { ...selectionOrder[idx], barcode_uri }
			})
		}
    this.setState({ selectAll, selection, selectionOrder });
  };

	isSelected = (key: string) => this.state.selection.includes(key)

	renderPagination = ({ canNext, canPrevious, page, pageSize, sorted }: Object) => {
		const pages = [10, 20, 30, 40, 50]
		return (
			<Row className="pl-3 pr-3">
				<Col className="d-flex flex-row pt-2 pb-2 pr-3 pl-3">	
					<Button
						disabled={!canPrevious} color={canPrevious ?  'success' : 'secondary'} outline
						onClick={() => this.getOrders(pageSize, (page - 1) * pageSize, (page - 1), sorted)}
					>
						Previous
					</Button>
					<Input
						type="select" name="pageSize" value={pageSize} className="mr-3 ml-3"
						onChange={(e) => this.getOrders(e.target.value, page * e.target.value, page, sorted)}
					>
						{pages.map(num => <option key={`page-${num}`} value={num}>{num} Baris</option>)}
					</Input>
					<InputGroup className="mr-3" >
						<InputGroupAddon addonType="prepend" >Page</InputGroupAddon>
						<Input
							type="number" name="page" value={page}
							onChange={(e) => this.getOrders(pageSize, e.target.value * pageSize, e.target.value, sorted)}
						/>
					</InputGroup>
					<Button
						disabled={!canNext} color={canNext ?  'success' : 'secondary'}
						onClick={() => this.getOrders(pageSize, (page + 1) * pageSize, (page + 1), sorted)}
					>
						Next
					</Button>
				</Col>
			</Row>
		)
	}

	
	render() {
		const {
			dateRange, searchText, orders, loadingOrders,
			selectAll, pageSize, page, pages
		} = this.state
		const { isSelected, toggleSelection, toggleAll } = this
		const tableProps = {
			selectAll,
      isSelected,
      toggleSelection,
			toggleAll,
			pageSize,
			selectType: "checkbox",
			defaultPageSize: 20,
			filterable: false,
			manual: true,
		}

		return (
			<div>
				<Card>
					<CardBody>
						<Row>
							<Col sm="12" xs="12" md="4" lg="4" xl="4">
							<CardTitle>
								<i className="mdi mdi-database mr-2"></i> Toko Trip Store
							</CardTitle>
								<CardSubtitle>List Toko Trip Store</CardSubtitle>
							</Col>
							<Col sm="12" xs="12" md="8" lg="8" xl="8">
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<InputGroup style={{ maxHeight: 36 }}>
											<Input
												type="text" placeholder="Cari"
												name="searchText" value={searchText}
												onChange={this.onChage}
											/>
											<InputGroupAddon addonType="append">
												<Button color="primary" 
												// onClick={() => onChangeSearch(searchText)}
												>
													<i className="ti-search"></i>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</Col>
								</Row>
							</Col>
						</Row>
					</CardBody>
					<CardBody>
						<CheckboxTable
							ref={r => (this.tableRef = r)}
							manual
              pages={pages}
							page={page}
							columns={tableHeaders
								// .concat([{
								// Header: "Actions",
								// accessor: "actions",
								// sortable: false,
								// filterable: false,
								// Cell: (row) => (
								// <Button size="sm" color="link" onClick={e => this.goDetailPage(e, row)}>
								// 	Detail
								// </Button>)
								// }])
							}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={orders}
							loading={loadingOrders}
							onFetchData={(tableState: Object) =>
								this.getOrders(tableState.pageSize, page * tableState.pageSize, page, tableState.sorted)}
							PaginationComponent={this.renderPagination}
							{...tableProps}
						/>
					</CardBody>
				</Card>
			</div>
		);
	}
}

const mapState = (state: Object) => ({
	account: state.auth.account
})
export default connect(mapState, null)(ListToko);
