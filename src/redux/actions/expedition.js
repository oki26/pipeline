// @flow
import qrcode from 'qrcode'
import * as constants from '../constants/expedition'
import { showAlert, dissmissAlert, setLoading } from './general'
import { post } from '../../services/api-rest'

/**
 * 
 * @param {Array<Expedition>} expeditions 
 */
export function setExpeditions(expeditions: Array<Object>) {
  return ({
    type: constants.SET_EXPEDITIONS,
    payload: { expeditions }
  })
}

/**
 * 
 * @param {Array<Barcode>} barcodes
 */
export function setBarcodes(barcodes: Array<Object>) {
  return ({
    type: constants.SET_BARCODES,
    payload: { barcodes }
  })
}


/**
 * 
 * @param {ISO Date String} fromDate 
 * @param {ISO Date String} toDate
 */
export function getExpeditions(fromDate: string, toDate: string, partnerCode?: string = "KLNT"){
  return async (dispatch: Function) => {
    dispatch(dissmissAlert())
    dispatch(setLoading(true))
    try {
      const query = `
        query {
          getOrdersByPartnerAndDate(
            partnerCode: "${partnerCode}"
            dateIn: { from: "${fromDate}" to: "${toDate}" }
          ) {
            id senderName receiverName
            receiverPhone senderPhone pickupAddress dropAddress
            pakets {
              id note name
            }
          }
        }`
      const res = await post('/paket-service/dev', { query })
      let mainPromises = [];
      if (res.data) {
        dispatch(setExpeditions(res.data.getOrdersByPartnerAndDate))
        console.log(res.data.getOrdersByPartnerAndDate)

        // generating paket barcodes
        res.data.getOrdersByPartnerAndDate.forEach((ekspedisi) => {
          const promises = ekspedisi.pakets.map(async (paket) => {
            try {
              const url = await qrcode.toDataURL(`${ekspedisi.id};${paket.id}`)
              return {
                url: url, 
                senderName: ekspedisi.senderName,
                reciverName: ekspedisi.reciverName,
                senderPhone: ekspedisi.senderPhone,
                receiverPhone: ekspedisi.reciverPhone,
                pickAddr: ekspedisi.pickupAddress,
                dropAddr: ekspedisi.dropAddress,
                exped_id: ekspedisi.id, 
                pack_id: paket.id,
                note: paket.note,
                name: paket.name,
              }
            } catch (er) {
              return {
                url: null,
                senderName: ekspedisi.senderName,
                reciverName: ekspedisi.reciverName,
                senderPhone: ekspedisi.senderPhone,
                receiverPhone: ekspedisi.reciverPhone,
                pickAddr: ekspedisi.pickupAddress,
                dropAddr: ekspedisi.dropAddress,
                exped_id: ekspedisi.id, 
                pack_id: paket.id,
                note: paket.note
              }
            }
          })
          mainPromises = mainPromises.concat(promises);
        })
        const barcodes = await Promise.all(mainPromises)
        dispatch(setLoading(false))
        dispatch(setBarcodes(barcodes));
      }
    } catch (err) {
      dispatch(setLoading(false))
      console.warn(err)
      if (!err.response && err.config) {
        return dispatch(showAlert('warning', 'Gagal memuat data', 'Gagal mendapat data dari server. silakan coba lagi'))
      }
      dispatch(showAlert('danger', 'Terjadi kesalahan', 'Silakan coba lagi atau hubungi tim IT'))
    }
  } 
}