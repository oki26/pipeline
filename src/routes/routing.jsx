import Home from '../views/home'
import Orders from '../views/logistics/orders';
import RouteAndPrice from '../views/logistics/routeAndPrice';
import TripStoreOrder from '../views/trip_store/orders';
import TripStoreToko from '../views/trip_store/list_toko';
import RevenueLogistics from '../views/revenue_logistics';
import Feeders from '../views/logistics/feeders';
import TravelTrain from '../views/logistics/travel_train';
import TravelAirplane from '../views/logistics/travel_airplane';
import TravelCar from '../views/logistics/travel_car';
import formRegister from '../views/form-register/form-register'
import Kulina from '../views/partnership/kulina'
import TriplogisticsOrderDetail from '../views/logistics/triplogistics-order-detail'
import Manifests from '../views/logistics/manifests'
import Users from '../views/CRM/users'
import Flights from '../views/tickets/flight'

import UserBalance from '../views/fee/user-balance'
import UserPoint from '../views/fee/user-point'
import WithdrawRequest from '../views/fee/withdraw-request'

var DashboardRoutes = [
  {
    path: '/home',
    name: 'Home',
    icon: 'mdi mdi-home',
    component: Home
  },
  {
    collapse: true,
    name: 'Statistik',
    state: 'statistics',
    icon: 'mdi mdi-database',
    child: [
      {
        path: '/statistics-logistics',
        name: 'Revenue Logistik',
        component: RevenueLogistics
      },
      {
        path: '/statistics-tripstore',
        name: 'Transaksi Tripstore',
        component: Feeders
      },
      {
        path: '/statistics-travelers',
        name: 'Travelers',
        component: Feeders
      },
      {
        path: '/statistics-users',
        name: 'Pertumbuhan Users',
        component: Feeders
      },
      {
        path: '/statistics-flight',
        name: 'Revenue Tiket Pesawat',
        component: Feeders
      },
      {
        path: '/statistics-hotel',
        name: 'Renenue Hotel',
        component: Feeders
      },
      {
        path: '/statistics-feeder-performance',
        name: 'Feeder Performance',
        component: Feeders
      },
    ]
  },
  {
    collapse: true,
    name: 'Fee',
    state: 'fee',
    icon: 'mdi mdi-cash-multiple',
    child: [
      {
        path: '/user-balance',
        name: 'User Balance',
        component: UserBalance
      },
      {
        path: '/user-point',
        name: 'User Point',
        component: UserPoint
      },
      {
        path: '/withdraw-request',
        name: 'Withdraw Request',
        component: WithdrawRequest
      }
    ]
  },
  {
    collapse: true,
    name: 'Logistik',
    state: 'logistik',
    icon: 'mdi mdi-truck',
    child: [
      {
        path: '/orders',
        name: 'Orders',
        component: Orders
      },
      {
        path: '/manifests',
        name: 'Manifest',
        component: Manifests
      },
      {
        path: '/feeders',
        name: 'Feeders',
        component: Feeders
      },
      {
        path: '/traveler-car',
        name: 'Traveler Dengan Mobil',
        component: TravelCar
      },
      {
        path: '/traveler-train',
        name: 'Traveler Dengan Kereta',
        component: TravelTrain
      },
      {
        path: '/traveler-plane',
        name: 'Traveler Dengan Pesawat',
        component: TravelAirplane
      },
      {
        path: '/cargo',
        name: 'Kargo',
        component: Feeders
      },
    ]
  },
  {
    collapse: true,
    name: 'Daftar SLA',
    state: 'slalist',
    icon: 'mdi mdi-routes',
    child: [
      {
        path: '/route-instant',
        name: 'Instant',
        component: RouteAndPrice
      },
      {
        path: '/route-sameday',
        name: 'Sameday',
        component: RouteAndPrice
      },
      {
        path: '/route-nextday',
        name: 'Nextday',
        component: RouteAndPrice
      },
      {
        path: '/route-regular',
        name: 'Regular',
        component: RouteAndPrice
      },
    ]
  },
  {
    collapse: true,
    name: 'CRM',
    state: 'crm',
    icon: 'mdi mdi-account',
    child: [
      {
        path: '/users-customer',
        name: 'User',
        component: Users
      },
      {
        path: '/email-to-customer',
        name: 'Email to Customer',
        component: Feeders
      },
      {
        path: '/live-chat',
        name: 'Live Chat Support',
        component: Feeders
      },
      {
        path: '/banner-promo',
        name: 'Banner Promo',
        component: Feeders
      },
      {
        path: '/refferal-code',
        name: 'Referral Code',
        component: Feeders
      },
      {
        path: '/promo-code',
        name: 'Promo Code',
        component: Feeders
      },
    ]
  },
  {
    collapse: true,
    name: 'Tiket Pesawat dan Hotel',
    state: 'flightandhotel',
    icon: 'mdi mdi-ticket',
    child: [
      {
        path: '/flight-orders',
        name: 'Tiket Pesawat',
        component: Flights
      },
      {
        path: '/hotel-orders',
        name: 'Hotel',
        component: Feeders
      },
    ]
  },
  {
    collapse: true,
    path: '/partnership',
    name: 'Partnership',
    state: 'partnership',
    icon: 'mdi mdi-home-modern',
    child: [
      {
        path: '/partnership/kulina',
        name: 'Kulina',
        mini: 'B',
        icon: 'mdi mdi-adjust',
        component: Kulina
      },
      {
        path: '/partnership/kulina',
        name: 'Blibli',
        mini: 'B',
        icon: 'mdi mdi-adjust',
        component: Feeders
      },
      {
        path: '/partnership/kulina',
        name: 'BNI',
        mini: 'B',
        icon: 'mdi mdi-adjust',
        component: Feeders
      }
    ]
  },
  {
    collapse: true,
    name: 'Trip Store',
    state: 'tripstore',
    icon: 'mdi mdi-store',
    child: [
      {
        path: '/tripstore-order',
        name: 'Order Trip Store',
        component: TripStoreOrder
      },
      {
        path: '/tripstore-toko',
        name: 'Daftar Toko',
        component: TripStoreToko
      }
    ]
  },
  {
    collapse: true,
    path: '/admin',
    name: 'Administrasi',
    state: 'administrasi',
    icon: 'mdi mdi-account-plus',
    child: [{
      path: '/register',
      name: 'Buat Akun Admin',
      component: formRegister

    }]
  },
  { path: '/order/:orderID', name: 'Order Detail', hide_menu: true, component: TriplogisticsOrderDetail },
  { path: '/', pathTo: '/home', name: 'Home', redirect: true }
];
export default DashboardRoutes;
