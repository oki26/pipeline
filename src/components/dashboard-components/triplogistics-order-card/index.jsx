// @flow
import type { OrderType } from '../../../types'
import React, { PureComponent } from 'react'
import { Row, Col, Card, CardTitle, CardBody } from 'reactstrap'
import { List } from 'react-content-loader'
import moment from 'moment'

type Props = {
  order: OrderType,
  loading: boolean,
}

export default class TriplogisticsOrderDetailCard extends PureComponent<Props> {
  renderLoading = () => (
    <Row>
        <Col sm="12" xs="12" md="4" lg="4" xl="4">
          <List />
        </Col>
        <Col sm="12" xs="12" md="4" lg="4" xl="4">
          <List />
        </Col>
        <Col sm="12" xs="12" md="4" lg="4" xl="4">
          <List />
        </Col>
      </Row>
  )
  
  render() {
    const { order, loading } = this.props
    return (
      <Card className="p-3">
        <CardTitle>Order Detail</CardTitle>
        <CardBody>
          {loading && this.renderLoading()}
          {!order && !loading &&
            <Col>
              <center>Tidak Ada Data</center>
            </Col>
          }
          {order !== null &&
            <Row>
              <Col sm="12" xs="12" md="4" lg="4" xl="4">
                <h6>Detail Order</h6>
                <div>Order ID: {order.objectId}</div>
                <div>SLA: {order.sla ? order.sla.name_sla : 'null'}</div>
                <div>Jam Pickup: {moment(order.pickup_time).format('DD-MMM-YYYY | HH:mm')}</div>
                <div>Isi Paket: {order.package.name}</div>
                <div>Berat: {order.package.weight_packages} Kg</div>
                <div>Dimensi: {order.package.dimensions}</div>
                <div>User: {order.owner.first_name} {order.owner.last_name}</div>
              </Col>
              <Col sm="12" xs="12" md="4" lg="4" xl="4">
                <h6>Detail Pengirim</h6>
                <div>Nama: {order.from.contact_name}</div>
                <div>Telp: {order.from.phone}</div>
                <div>Alamat: {order.from.address}</div>
                <div>Kota: {order.from.city.name}</div>
              </Col>
              <Col sm="12" xs="12" md="4" lg="4" xl="4">
                <h6>Detail Penerima</h6>
                <div>Nama: {order.to.contact_name}</div>
                <div>Telp: {order.to.phone}</div>
                <div>Alamat: {order.to.address}</div>
                <div>Kota: {order.to.city.name}</div>
              </Col>
            </Row>
          }
        </CardBody>
      </Card>
    )
  }
}
