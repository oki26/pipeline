// @flow
import type { OrderType, TripType, Manifest } from '../../../types'
import React from "react"
import ReactTable from "react-table"
import qrcode from 'qrcode'
import checkboxHOC from "react-table/lib/hoc/selectTable"
import {
	Card, CardBody, CardTitle,
	Button, Col, InputGroup, InputGroupAddon,
	Row, CardSubtitle, Input,
} from 'reactstrap'
import moment from "moment"
import Picker from 'rc-calendar/lib/Picker'
import RangeCalendar from 'rc-calendar/lib/RangeCalendar'
import enUS from 'rc-calendar/lib/locale/en_US'
import "react-table/react-table.css"
import { TotalInstrument, ManifestProviewModal, BarcodeCard } from '../../../components/dashboard-components'
import Backendless from '../../../services/backendless'
import { history } from '../../../index'

const CheckboxTable = checkboxHOC(ReactTable)
type Props = {}

type State = {
	isOpenModal: boolean,
	modal: boolean,
	loading: boolean,
	obj: Object,
	manifests: Array<Manifest>,
	loadingOrders: boolean,
	loadingDetail: boolean,
	dateRange: Array<string>,
	searchText: string,
	selection: Array<string>,
	selectionOrder: Array<OrderType>,
	selectAll: boolean,
	pageSize: number,
}

const tableHeaders = [
	{ Header: 'ID Manifest', accessor: 'objectId' },
	{ Header: 'arrived_time', accessor: row => moment(row.arrived_time).format('DD-MM-YYYY HH:mm'), id: 'arrived_time' },
	{ Header: 'cargo', accessor: row => row.cargo ? row.cargo.name : null, id: 'cargo' },
	{ Header: 'departured_time', accessor: row => moment(row.departured_time).format('DD-MM-YYYY HH:mm'), id: 'departured_time'  },
	{ Header: 'dest_code', accessor: 'dest_code' },
	{ Header: 'org_code', accessor: 'org_code' },
	{ Header: 'status_code', accessor: 'status_code' },
	{ Header: 'tracking_id', accessor: 'tracking_id' },
	{ Header: 'bagging', id: 'bagging', accessor: obj =>  `${obj.dest_code} - ${obj.org_code}` },
]

const calendar = (
	<RangeCalendar
		showWeekNumber={false}
		dateInputPlaceholder={['Dari Tanggal', 'Sampai Tanggal']}
		defaultValue={[moment().subtract(1, 'month').format(), moment()]}
		locale={enUS}
		disabledDate={current => current.isAfter(moment().add(1, 'days').hour(0))}
	/>
)

class Manifests extends React.Component<Props, State> {
	tableRef = null
	constructor(props: Props) {
		super(props)
		this.state = {
			loading: false,
			modal: false,
			obj: {},
			manifests: [],
			loadingOrders: false,
			loadingDetail: false,
			dateRange: [],
			searchText: '',
			selectAll: false,
			selection: [],
			selectionOrder: [],
			pageSize: 20,
			isOpenModal: false,
		}
	}

	getManifests = async (
		pageSize: number = 20, offset: number = 0,
		sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		try {
			this.setState({ loadingOrders: true })
			const { dateRange, searchText } = this.state
			
			// set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(2).setSortBy(sortQuery)

			if (dateRange.length === 2) {
					dateRange.sort((a, b) => moment(a).isAfter(b) ? -1 : 1)
					queryBuilder.setWhereClause(`created >= ${moment(dateRange[0]).format('x')} and created <= ${moment(dateRange[1]).format('x')}`)
			}

			if (searchText !== '') {
				queryBuilder.setWhereClause(`tracking_id LIKE '%${searchText}%'`)
			}
			const res: Array<Manifest> = await Backendless.Data.of('triplogistic_manifest').find(queryBuilder)
			const transformed: Array<Manifest> =  res.map(
				(data: Manifest, index: number) => ({ _id: data.objectId, ...data })
			)
			this.setState({ manifests: transformed, loadingOrders: false })
		} catch(er) {
			this.setState({ loadingOrders: false })
		}
	}


	timeout = null
	onChage = (e: Object) => {
		this.setState({ [e.target.name]: e.target.value }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getManifests, 1000)
		})
	}


  toggleSelection = async (key: string, shift: any, row: Object) => {
		let selection = [key]
		let selectionOrder = []
		try {
			const barcode_uri = await qrcode.toDataURL(row.order.tracking_id)
			selectionOrder = [{ ...row.order, barcode_uri }]
		} catch (er) {}
    this.setState({ selection, selectionOrder })
  }

	isSelected = (key: string) => this.state.selection.includes(key)

	
	render() {
		const { dateRange, searchText, manifests, loadingOrders, selectAll, pageSize, isOpenModal, selectionOrder } = this.state
		const { isSelected, toggleSelection } = this
		const tableProps = {
			selectAll,
      isSelected,
      toggleSelection,
			selectType: "radio",
			defaultPageSize: 20,
			filterable: false,
			manual: true,
		}

		return (
			<div>
				<TotalInstrument
					title={'Total Manifest'}
					totalInstrument={0}
				/>
				<Card>
					<CardBody>
						<Row>
							<Col sm="12" xs="12" md="4" lg="4" xl="4">
							<CardTitle>
								<i className="mdi mdi-database mr-2"></i> Manifest Pengiriman Barang
							</CardTitle>
								<CardSubtitle>List Manifest Pengiriman Barang</CardSubtitle>
							</Col>
							<Col sm="12" xs="12" md="8" lg="8" xl="8">
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="6" lg="6" xl="6">
									<InputGroup style={{ maxHeight: 36 }}>
										<Picker
											value={dateRange}
											onChange={(val) => this.setState({ dateRange: val }, () => this.getManifests())}
											animation="slide-up"
											calendar={calendar}
										>
											{
												({ value }) => (
														<Input
															placeholder="Filter Tanggal"
															value={(value[0] && value[1]) ? `${moment(value[0]).format('DD MMM YYYY')}  to  ${moment(value[1]).format('DD MMM YYYY')}` : ''}
														/>
												)
												
											}
										</Picker>
										<Button disabled={dateRange.length < 1} onClick={() => this.setState({ dateRange: [] }, () => this.getManifests())}>
											X
										</Button>
									</InputGroup>
									</Col>
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<InputGroup style={{ maxHeight: 36 }}>
											<Input
												type="text" placeholder="Cari"
												name="searchText" value={searchText}
												onChange={this.onChage}
											/>
											<InputGroupAddon addonType="append">
												<Button color="primary" 
												// onClick={() => onChangeSearch(searchText)}
												>
													<i className="ti-search"></i>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</Col>
								</Row>
							</Col>
						</Row>
					</CardBody>
					<CardBody>
						<CheckboxTable
							ref={r => (this.tableRef = r)}
							columns={tableHeaders}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={manifests}
							loading={loadingOrders}
							onFetchData={(tableState: Object) =>
								this.getManifests(tableState.pageSize, tableState.page * tableState.pageSize, tableState.sorted)}
							// PaginationComponent={this.renderPagination}
							{...tableProps}
						/>
					</CardBody>
				</Card>
				{selectionOrder.length > 0 &&
					<BarcodeCard
						title="Barcode AWB"
						datas={selectionOrder}
					/>
				}
			</div>
		)
	}
}

export default Manifests
