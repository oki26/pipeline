// @flow

export type OrderType = {
  created?: string,
  updated?: string,
  notes: string,
  objectId: string,
  from: AddressType,
  to: AddressType,
  package: PackageType,
  sla: SlaType,
  trips: Array<TripType>,
  owner: RegularUserType,
  payment: PaymentType,
  tracking_id: string,
  real_receiver_name: string,
  real_receiver_time: string,
  pickup_time: number,
  origin_code: string,
  destination_code: string,
  status_code: string
}

export type AddressType = {
  address: string,
  city_name: string,
  city: {
    name: string
  },
  contact_name: string,
  phone_number: string,
  email: string,
  created?: number,
  owner: Object, // cahnge this to regular user type
  postal_code: string,
  updated?: string,
  objectId: string,
  location: Geo,
}

export type SlaType = {
  start_time_sla: number,
  price_per_kilo: number,
  total_price: number,
  created?: number,
  end_time_sla: number,
  max_weight_sla: number,
  ownerId: string,
  updated?: string,
  description_sla: string,
  id_sla: string,
  name_sla: string,
  objectId: string,
}

export type PackageType = {
  objectId: string,
  id_packages: number,
  created?: number,
  weight_packages: number,
  name: string,
  ownerId: string,
  sender_name: string,
  phone_number_sender: string,
  name_packages: string,
  updated?: number,
  objectId: string,
  dimensions: string
}

export type UserType = {
  lastLogin: string,
  userStatus: string,
  first_name: string,
  last_name: string,
  phone_number: ?number,
  created?: number,
  feeder: boolean,
  staff: boolean,
  ownerId: string,
  socialAccount: string,
  updated?: string,
  objectId: string,
  regular: RegularUserType,
  email: string,
}

export type RegularUserType = {
  objectId?: string,
  avatar_show: string,
  avatar_thumbnail: string,
  balance: number,
  birthdate: Date | number,
  first_name: string,
  gender: string,
  is_active: boolean,
  is_blocked: boolean,
  is_online: boolean,
  is_verification: boolean,
  language: string,
  last_name: string,
  phone_number: string,
  point: number,
  religion: string,
  unikqu_activation: boolean,
  created?: Date | number,
  updated?: Date | number,
}

export type PaymentType = {
  amount: number,
  currency: string,
  discount_percentage: number,
  owner: any, // relation regular users
  payment_status: string,
  payment_proof: string,
  payment_method: string,
  promo_code: string,
  total_amount: number,
  created?: string,
  updated?: string,
  objectId?: string,
}

export type FeederType = {
  active: boolean,
  avatar_show: string,
  avatar_thumbnail: string,
  birthdate: string,
  blocked: boolean,
  city: Object, // city type
  country: Object, // country type
  current_location: ?{
    longitude: number,
    latitude: number
  },
  firstname: string,
  gender: string,
  jobs: Array<Object>, // array of Job
  language: string,
  lastname: string,
  online: boolean,
  phonenumber: string,
  rating: number,
  religion: string,
  verified: boolean,
  created?: string,
  updated?: string,
}

export type TravelType = {
  arrival_date: number | Date,
  available_baggage: number,
  departure_date: number | Date,
  destination: string,
  is_used: boolean,
  origin: string,
  point_destination: ?{
    longitude: number,
    latitude: number,
  },
  point_origin: ?{
    longitude: number,
    latitude: number,
  },
  ticket_photo: string,
  total_price: number,
  traveler: any,
  trip: any,
  created?: number | Date,
  updated?: number | Date,
}

export type TravelCarType = {
  arrival_date: number | Date,
  available_baggage: number,
  car_photo: string,
  departure_date: number | Date,
  destination: string,
  is_used: boolean,
  origin: string,
  point_destination: Geo,
  point_origin: Geo,
  total_price: number,
  traveler: Object,
  trip: Object,
  created?: number | Date,
  updated?: number | Date,
}

export type Geo = {
  longitude: number,
  latitude: number,
}

export type TripType = {
  objectId?: string,
  arrival_location: ?Geo,
  arrival_time: number | Date,
  departure_location: ?Geo,
  departure_time: number | Date,
  destination_city: Object,
  instrument_handle: string,
  is_delivered: boolean,
  messages: string,
  note: string,
  order: ?OrderType,
  origin_city: Object,
  percentage: number,
  sequence_number: number,
  created?: number | Date,
  updated?: number | Date,
}
export type CardObject = {
  type: string,
  title: string,
  textOne: string, 
  textTwo: string,
  color: string,
  data: TravelType | FeederType | TravelCarType | TripType,
}

export type InstrumentType = {
  origin: string,
  destination: string,
  arrival_date: number,
  departure_date: number,
  available_baggage: number,
  is_used: boolean,
  total_price: number,
  trip: Array<Object>,
  traveler: UserType

}

export type JobType = {
  delivered_time: number | Date,
  feeder: Object,
  picked_up_time: number | Date,
  trip: TripType,
  created?: number | Date,
  updated?: number | Date,
}

export type Manifest = {
  objectId: string,
  arrived_time: number | Date,
  cargo: Object,
  depatured_time: number | Date,
  dest_code: string,
  org_code: string,
  status_code: string,
  tracking_id: string,
  bagging: ?Array<Object>,
  created?: number | Date,
  updated?: number | Date,
}


export type FlightInvoice = {
  objectId: string,
  amount: number,
  attachment: string,
  billed_id: string,
  deadline: number | Date,
  id_chanel: number,
  delete_at: number | Date,
  status: string,
  ticketing_id: number,
  created?: number | Date,
  updated?: number | Date,
}

export type FlightOrder = {
  arrival_time: number | Date,
  destination: string,
  booking_date: number | Date,
  departure_time: number | Date,
  order_id: string,
  order_detail_id: string,
  origin: string,
  saw: string,
  token: string,
  status: string,
  ticket_id: string,
  invoice: FlightInvoice,
  passenger: Array<Object>,
  total_price: number,
  user: UserType,
  created?: number | Date,
  updated?: number | Date,
}

export type TripStoreType = {
  objectId: string,
  is_paid: boolean,
  owner: RegularUserType,
  payment: PaymentType,
  product: ProductType,
  quantity: number,
  status: string,
  total_price: number,
  created?: number | Date,
}

export type ProductType = {
  category: {
    name: string
  },
  description: string,
  featured: boolean,
  name: string,
  photos: [{
    photo_show: string,
    photo_thumbnail: string
  }],
  price: number,
  reviews: {},
  shop: ShopType,
  stock: number,

}

export type ShopType = {
  objectId: string,
  addresses: {},
  banner: string,
  birthdate: number | Date,
  description: string,
  email: string,
  logo: string,
  name: string,
  owner: RegularUserType,
  phone_number: string,
  pin: string,
  rating: number,
  created?: number | Date,
}