// @flow
import type { OrderType } from '../../../types'
import React from 'react'
import { 
  CustomInput, Collapse, Modal, 
  ModalHeader, ModalBody, ModalFooter, 
  FormGroup, Input, Label, TabContent, 
  TabPane, Nav, NavItem, NavLink, Card, 
  Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import Backendless from '../../../services/backendless'
import classnames from 'classnames';
import accounting from 'accounting';
import moment from 'moment';
import Lightbox from 'react-lightbox-component';

type RowDataType = {
	objectId: string,
	sla: string,
	package: string | null,
	weight: string | null,
	payment_status: string | null,
	amount: number | null,
	pickup_time: string | null,
	status_code: string | null,
	from: string | null,
	to: string | null,
	receiver: string | null,
	sender: string | null,
	order: OrderType,
}

type Props = {
  amount: string,
  payment_status: string,
  data: RowDataType,
  isOpen: boolean,
  toggle: Function,
  onReloadData: Function,
  className?: string,
}

type State = {
  amount: string | number | null,
  payment_status: string,
  order_status: string,
  receiver_name: string,
  receiver_time: string,
  activeTab: string
}

class ModalDetailOrder extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      amount: this.props.data.amount,
      payment_status: "",
      order_status: "",
      activeTab: '1',
      receiver_name: '',
      receiver_time: ''
    }
  }

  toggleTab = (tab: string) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onSubmitPaymentStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {payment_status} = this.state
    if (payment_status !== "") {
      await Backendless.Data.of('payment').save({objectId: data.order.payment.objectId, payment_status: payment_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  onSubmitOrderStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {order_status} = this.state
    if (order_status !== "") {
      await Backendless.Data.of('triplogistic_orders').save({objectId: data.order.objectId, status_code: order_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  render(){
    const {isOpen, toggle, className, data} = this.props
    const {amount, payment_status, order_status, receiver_name, receiver_time} = this.state

    return(
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Payment Status</ModalHeader>
        <ModalBody>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => { this.toggleTab('1'); }}
              >
                Detail Order
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={() => { this.toggleTab('2'); }}
              >
                Detail Paket
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Row>
                <Col sm="12">
                  <Card body style={{margin: 10}} >
                    <CardTitle>Pembayaran</CardTitle>
                    <FormGroup className="mt-2">
                      <Label for="cargo_name">Jumlah Tagihan</Label>
                      <Input disabled name="amount" value={data.amount ? data.amount : ""}
                        id="amount" placeholder="Jumlah Penagihan" />
                    </FormGroup>
                    <FormGroup className="mt-2">
                      <Row>
                        <Col sm="6">
                          <Label for="cargo_name">Bukti Transfer</Label>
                        </Col>
                        <Col sm="6">
                          {data.order ? data.order.payment ? data.order.payment.payment_proof ? (
                              <a href={data.order.payment.payment_proof} target="_blank" >
                                <img style={{width:'100%', height:'auto'}} src={data.order.payment.payment_proof} alt="Bukti Transfer" />
                              </a>
                            ) 
                            : (
                              <p style={{color:"#ff0000"}} >User Belum Mengupload Bukti Transfer</p>
                            ) 
                            : (
                              <p style={{color:"#ff0000"}} >User Belum Mengupload Bukti Transfer</p>
                            )
                            : (
                              <p style={{color:"#ff0000"}} >User Belum Mengupload Bukti Transfer</p>
                            )
                          }
                        </Col>
                      </Row>
                    </FormGroup>
                    <FormGroup className="mt-2">
                      <Label for="cargo_name">Status Pembayaran</Label>
                      <Input type="select" id="payment_status" 
                        onChange={e => this.setState({ payment_status: e.target.value })} value={payment_status !== '' ? payment_status : data.payment_status ? data.payment_status : ""} >
                        <option value="unpaid" >unpaid</option>
                        <option value="prepaid" >prepaid</option>
                        <option value="paid" >paid</option>
                        <option value="cancelled">cancelled</option>
                        <option value="expired">expired</option>
                      </Input>
                    </FormGroup>
                    <Button color="primary" onClick={this.onSubmitPaymentStatus} >Update Payment Status</Button>
                  </Card>
                  <Card body style={{margin: 10}} >
                    <CardTitle>Order Status</CardTitle>
                    <FormGroup className="mt-2">
                      <Label for="cargo_name">Status Paket</Label>
                      <Input type="select" id="payment_status" 
                        onChange={e => this.setState({ order_status: e.target.value })} value={order_status !== '' ? order_status : data.status_code ? data.status_code : ""} >
                        <option value="ORD" >Pending</option>
                        <option value="TIORD" >Picked up by feeder</option>
                        <option value="TIWO" >On The Way To Traveler</option>
                        <option value="TICRG">Picked up by traveler</option>
                        <option value="WD">Traveller arrived</option>
                        <option value="TIWD">Deliver by Feeder</option>
                        <option value="DO">Completed</option>
                      </Input>
                    </FormGroup>
                    {order_status === "DO" ? (
                      <div>
                        <FormGroup className="mt-2">
                          <Label>Nama Penerima</Label>
                          <Input id="receiver_name" 
                            onChange={e => this.setState({ receiver_name: e.target.value })} value={receiver_name} />
                        </FormGroup>
                        <FormGroup className="mt-2">
                          <Label>Waktu Diterima</Label>
                          <Input id="receiver_time" type="datetime-local"
                            onChange={e => this.setState({ receiver_time: e.target.value })}/>
                          <Label>{receiver_time ? moment(receiver_time).format('DD MMMM YYYY, HH:mm') : null}</Label>
                        </FormGroup>
                      </div>
                    ):(
                      data.status_code === "DO" ? (
                        <div>
                          <FormGroup className="mt-2">
                            <Label>Nama Penerima</Label>
                            <Input disabled id="receiver_name" value={data.order.real_receiver_name} />
                          </FormGroup>
                          <FormGroup className="mt-2">
                            <Label>Waktu Diterima</Label>
                            <Input disabled id="receiver_time" value={data.order.real_receiver_time ? moment(data.order.real_receiver_time).format('DD MMMM YYYY, HH:mm') : ""} />
                          </FormGroup>
                        </div>
                      ): null
                    )}
                    <Button color="primary" onClick={this.onSubmitOrderStatus} >Update Order Status</Button>
                  </Card>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="2">
              <Row>
                <Col sm="12">
                  <Card body style={{margin: 10}} >
                    <CardTitle>Detail Paket</CardTitle>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Isi Paket</b></CardText>
                        <CardText><b>Berat Paket</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.package || ""}</CardText>
                        <CardText>{data.weight || ""}</CardText>
                      </Col>
                    </Row>
                  </Card>
                  <Card body style={{margin: 10}} >
                    <CardTitle>Detail Pemilik Paket</CardTitle>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Pemilik Paket</b></CardText>
                        {/* <CardText><b>Email</b></CardText> */}
                        <CardText><b>No Telepon</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.package.sender_name : " "}</CardText>
                        {/* <CardText>{data.order ? data.order.package.ema : " "}</CardText> */}
                        <CardText>{data.order ? data.order.package.phone_number_sender : " "}</CardText>
                      </Col>
                    </Row>
                  </Card>
                  <Card body style={{margin: 10}} >
                    <CardTitle>Detail Pengirim Paket</CardTitle>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Nama Pengirim</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.from ? data.order.from.contact_name : "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Alamat</b></CardText>
                      </Col>
                      <Col sm="6">
                      <CardText>{data.order ? data.order.from ? data.order.from.address : "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Email</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.from ? data.order.from.email : "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>No Telepon</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.from ? data.order.from.phone_number : "" : ""}</CardText>
                      </Col>
                    </Row>
                  </Card>
                  <Card body style={{margin: 10}} >
                    <CardTitle>Detail Penerima Paket</CardTitle>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Nama Penerima</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.to ? data.order.to.contact_name : "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Alamat</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.to ? data.order.to.address: "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>Email</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.to ? data.order.to.email : "" : ""}</CardText>
                      </Col>
                    </Row>
                    <Row>
                      <Col sm="6">
                        <CardText><b>No Telepon</b></CardText>
                      </Col>
                      <Col sm="6">
                        <CardText>{data.order ? data.order.to ? data.order.to.phone_number : "" : ""}</CardText>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </ModalBody>
        <ModalFooter>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalDetailOrder