// @flow
import React, { Component } from 'react'
import { Modal, ModalHeader, ModalBody, Table } from 'reactstrap'
import moment from 'moment'
import type { TravelType, TravelCarType } from '../../../types'

type Props = {
  traveler: TravelCarType | TravelType,
  onClose: Function,
  isOpen: boolean
}

export default class ModalDetailTravel extends Component<Props> {
  render() {
    const { isOpen, onClose, traveler } = this.props
    return (
      <Modal isOpen={isOpen} backdrop={"static"} >
        <ModalHeader toggle={onClose}>Detail</ModalHeader>
        <ModalBody>
        <Table hover>
          <tbody>
            <tr>
              <th>Departure date</th>
              <td>{moment(traveler.departure_date).format("DD MMMM YYYY HH:mm")}</td>
            </tr>
            <tr>
              <th>Arrival date</th>
              <td>{moment(traveler.arrival_date).format("DD MMMM YYYY HH:mm")}</td>
            </tr>
            <tr>
              <th>from</th>
              <td>{traveler.origin}</td>
            </tr>
            <tr>
              <th>to</th>
              <td>{traveler.destination}</td>
            </tr>
            <tr>
              <th>traveler name</th>
              <td>{`${traveler.traveler.first_name} ${traveler.traveler.last_name}`}</td>
            </tr>
            <tr>
              <th>phone number</th>
              <td>{traveler.traveler.phone_number ? traveler.traveler.phone_number : 'not available'}</td>
            </tr>
          </tbody>
        </Table>
        </ModalBody>
        {/* <ModalFooter>
          <Button color="primary" onClick={this.toggleClose}>Do Something</Button>{' '}
          <Button color="secondary" onClick={this.toggleClose}>Cancel</Button>
        </ModalFooter> */}
      </Modal>
    )
  }
}
