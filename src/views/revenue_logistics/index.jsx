// @flow
import React from "react";
import ReactTable from "react-table";
import {
	Card,
	CardBody,
	CardTitle,
	Row, Col,
	Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import "react-table/react-table.css";
import * as data from "./reactable-data";
import { TotalRevenue, CryptoChart } from '../../components/dashboard-components';

type PropType ={

}

type State = {
	modal: boolean,
	obj: Object,
	jsonData: Array<Object>,
	valueCalendar: Array<string>,
	hoverValueCalendar: Array<string>
}

class Orders extends React.Component<PropType, State> {
	constructor(props: PropType) {
		super(props);
		this.state = {
			modal: false,
			obj: {},
			jsonData: data.ordersData,
			valueCalendar: [],
    	hoverValueCalendar: [],
		};
		this.toggle = this.toggle.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
	
	toggle = () => {
		this.setState({
			modal: !this.state.modal
		});
	}

	handleSubmit = (event: any) => {
		event.preventDefault();
		let id = event.target.id.value;
		let sla = event.target.sla.value;
		let packageid = event.target.packageid.value;
		let resi = event.target.resi.value;
		let sentStatus = event.target.sentStatus.value;
		let from = event.target.from.value;
		let to = event.target.to.value;
		let delivered = event.target.delivered.value;
		let sender = event.target.sender.value;
		let bill = event.target.bill.value;
		let invoiceStatus = event.target.invoiceStatus.value;
		let newObj = JSON.parse(JSON.stringify(this.state.jsonData));
		newObj[id] = [sla, packageid, resi, sentStatus, from, to, delivered, sender, bill, invoiceStatus];
		this.setState({
			jsonData: newObj,
			modal: !this.state.modal
		})
	}

	render() {
		const data2 = this.state.jsonData.map((prop, key) => {
			return {
				id: key,
				sla: prop[0],
				packageid: prop[1],
				resi: prop[2],
				sentStatus: prop[3],
				from: prop[4],
				to: prop[5],
				delivered: prop[6],
				sender: prop[7],
				bill: prop[8],
				invoiceStatus: prop[9],
				actions: (
					// we've added some custom button actions
					<div className="text-center">
						{/* use this button to add a edit kind of action */}
						<Button
							onClick={() => {
								let obj = data2.find(o => o.id === key);
								this.setState({
									modal: !this.state.modal,
									obj: obj
								});
							}}
							color="inverse"
							size="sm"
							round="true"
							icon="true"
						>
							<i className="fa fa-edit" />
						</Button>
						{/* use this button to remove the data row */}
						{/* <Button
							onClick={() => {
								let newdata = data2;
								newdata.find((o, i) => {
									if (o.id === key) {
										newdata.splice(i, 1);
										console.log(newdata);
										return true;
									}
									return false;
								});
								this.setState({ jsonData: newdata });
							}}
							className="ml-1"
							color="danger"
							size="sm"
							round="true"
							icon="true"
						>
							<i className="fa fa-times" />
						</Button> */}
					</div>
				)
			};
		});
		return (
			<div>
				<Modal isOpen={this.state.modal} toggle={this.toggle}>
					<ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
					<ModalBody>
						<Form onSubmit={this.handleSubmit}>
							<Input type="hidden" name="id" id="id" defaultValue={this.state.obj.id} />
							<FormGroup>
								<Label for="name">Sent Status</Label>
								<Input type="text" name="sentStatus" id="sentStatus" defaultValue={this.state.obj.sentStatus} />
							</FormGroup>
							<FormGroup>
								<Label for="designation">Invoice Status</Label>
								<Input type="text" name="invoiceStatus" id="invoiceStatus" defaultValue={this.state.obj.invoiceStatus} />
							</FormGroup>
							<FormGroup>
								<Button color="primary" type="submit">Save</Button>
								<Button color="secondary" className="ml-1" onClick={this.toggle}>Cancel</Button>
							</FormGroup>
						</Form>
					</ModalBody>
				</Modal>
				<h5 className="mb-4">Orders</h5>
				<Row>
					<Col sm={12}>
						<TotalRevenue 
							valueCalendar={this.state.valueCalendar}
							onChangeCalendar={valueCalendar => this.setState({valueCalendar})}
							hoverValueCalendar={this.state.hoverValueCalendar}
							onHoverChangeCalendar={hoverValueCalendar => this.setState({hoverValueCalendar})}
						/>
					</Col>
				</Row>
				<Row>
					<Col sm={12}>
						<CryptoChart />
					</Col>
				</Row>
				<Card>
					<CardTitle className="mb-0 p-3 border-bottom bg-light"><i className="mdi mdi-database mr-2"></i>All Orders</CardTitle>
					<CardBody>
						<ReactTable
							columns={[
								{
									Header: "ID",
									accessor: "id"
								},
								{
									Header: "SLA",
									accessor: "sla"
								},
								{
									Header: "ID Package",
									accessor: "packageid"
								},

								{
									Header: "Resi",
									accessor: "resi"
								},
								{
									Header: "Sent Status",
									accessor: "sentStatus"
								},
								{
									Header: "From",
									accessor: "from"
								},
								{
									Header: "To",
									accessor: "to"
								},
								{
									Header: "Delivered",
									accessor: "delivered"
								},
								{
									Header: "Sender",
									accessor: "sender"
								},
								{
									Header: "Amount of Bill",
									accessor: "bill"
								},
								{
									Header: "Invoice Status",
									accessor: "invoiceStatus"
								},
								{
									Header: "Actions",
									accessor: "actions",
									sortable: false,
									filterable: false
								}
							]}
							defaultPageSize={10}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={data2}
							filterable
						/>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default Orders;
