// @flow
import type { UserType } from '../../../types'
import React from 'react'
import { 
  CustomInput, Collapse, Modal, 
  ModalHeader, ModalBody, ModalFooter, 
  FormGroup, Input, Label, TabContent, 
  TabPane, Nav, NavItem, NavLink, Card, 
  Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import Backendless from '../../../services/backendless'
import classnames from 'classnames';
import accounting from 'accounting';
import Lightbox from 'react-lightbox-component';

type RowDataType = {
	objectId: string,
	first_name: string,
	last_name: string,
	point: number,
	order: UserType,
}

type Props = {
  amount: string,
  payment_status: string,
  data: RowDataType,
  isOpen: boolean,
  toggle: Function,
  onReloadData: Function,
  className?: string,
}

type State = {
  amount: string,
  type: string
}

class ModalUserPoint extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      amount: "",
      type: ""
    }
  }

  onSubmitPoint = async () => {
    const {data, toggle, onReloadData} = this.props
    const {amount, type} = this.state
    if (type === "ADD") {
      let point = (data.point + parseInt(amount))
      await Backendless.Data.of('regular_user').save({objectId: data.objectId, point})
      await toggle()
      onReloadData()
    }else if (type === "WITHDRAW") {
      let point = (data.point - parseInt(amount))
      await Backendless.Data.of('regular_user').save({objectId: data.objectId, point})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  render(){
    const {isOpen, toggle, className, data} = this.props
    const {amount, type} = this.state
    return(
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Edit User Point</ModalHeader>
        <ModalBody>
          <FormGroup className="mt-2">
            <Label for="name">User</Label>
            <Input disabled name="name" value={`${data.first_name} ${data.last_name}`}
              id="name" placeholder="nama" />
          </FormGroup>
          <FormGroup className="mt-2">
            <Label for="point">Point</Label>
            <Input disabled name="point" value={data.point ? data.point : 0}
              id="point" placeholder="Point" />
          </FormGroup>
          <FormGroup className="mt-2">
            <Label for="amount">Jumlah</Label>
            <Input type="number" min="0" name="amount" value={amount} id="amount" placeholder="Jumlah" 
              onChange={(e) => this.setState({amount: e.target.value})} />
          </FormGroup>
          <FormGroup className="mt-2">
            <Label for="type">Type</Label>
            <Input type="select" id="type" 
              onChange={e => this.setState({ type: e.target.value })} >
              <option value="" >Pilih Type</option>
              <option value="ADD" >Tambah Point</option>
              <option value="WITHDRAW" >kurangi Point</option>
            </Input>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button disabled={amount === '' || type === ''} color={amount === '' || type === '' ? 'secondary' : 'primary'} onClick={this.onSubmitPoint}  >Update</Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalUserPoint