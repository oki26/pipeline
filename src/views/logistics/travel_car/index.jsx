// @flow
import React from "react";
import {
	Row, Col, Container, Alert,
} from 'reactstrap'
import { InstrumentList, TotalInstrument } from '../../../components/dashboard-components';
import Backendless from '../../../services/backendless'

type State = {
	travel_train: Array<Object>,
	message: string,
	loading: boolean,
	messageType: string,
	selectedRegion: string,
	selectedStatus: string,
	regionOptions: Array<string>,
	searchText: string,
	travelTrainCount: number,
}

class Travel_train extends React.Component<{}, State> {
	timeout = null;
	state = {
		travel_train: [],
		message: '',
		messageType: 'info',
		loading: false,
		selectedRegion: '',
		selectedStatus: '',
		regionOptions: [],
		searchText: '',
		travelTrainCount: 0,
	}

	componentDidMount = () => {
		this.getCities()
	}

	getCities = async () => {
		try {
			const res = await Backendless.Data.of('cities').find()
			this.setState({
				regionOptions: res.map(city => city.name)
			})
		} catch(er) {
			console.log('fail get citie', er)
			this.setMessage(er.message || 'Gagal mendapatkan list kota', 'warning')
		}
	}
	onChangeParams = (text: string, type: string = 'searchText') => {
		this.setState({ [type]: text }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getFeeders, 1000)
		})
	}

 getFeeders = async (
		 page: number = 20, offset: number = 0,
		 sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		const { searchText, selectedRegion, selectedStatus } = this.state
		try {
			this.setState({ loading: true })
			// convert sort query to meet requirement backendless
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)
			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(page).setOffset(offset).setRelationsDepth(2).setSortBy(sortQuery)
			// filter search text
			if (searchText !== '') 
				queryBuilder.setWhereClause(`firstname LIKE '%${searchText}%' OR lastname LIKE '%${searchText}%'`)
			// filter status feeder 
			// query is one of 'active = true', 'active = false', or 'blocked = true'
			if (selectedStatus)
				queryBuilder.setWhereClause(selectedStatus)
			// filter region
			if (selectedRegion)
				queryBuilder.setWhereClause(`city.name LIKE '%${selectedRegion}%'`)
			const res: Array<Object> = await Backendless.Data.of('people_car_travel').find(queryBuilder)
			console.log(res)
			const count: number = await Backendless.Data.of('people_car_travel').getObjectCount()
			this.setState({ travel_train: res, loading: false, travelTrainCount: count })
		} catch (er) {
			console.log(er)
			this.setMessage(er.message || 'Gagal menghubungi server', 'warninng')
		}
	}

	setMessage(msg: string, type: string = 'info') {
		this.setState({ message: msg, loading: false, messageType: type })
		setTimeout(() => this.setState({ message: '' }), 5000)
	}

	render() {
		const {
			travel_train, loading, message, messageType, searchText,
			regionOptions, selectedRegion, selectedStatus, travelTrainCount,
		} = this.state
		return (
			<Container fluid>
				<Row>
					<Alert isOpen={message !== ''} color={messageType}>{message}</Alert>
				</Row>
				<Row>
						<h5 className="mb-4">Traveller Dengan Mobil</h5>
				</Row>
				<Row>
						<TotalInstrument
							title={'Total Travel Dengan Mobil'}
							totalInstrument={travelTrainCount}
							// feeders={travel_train}
							// focusLongitude={focusLongitude}
							// focusLatitude={focusLatitude}
						/>
				</Row>
				<Row>
					<Col className="p-0">
						<InstrumentList
							title={'List Traveler Dengan Mobil'}
							subtitle={'List bagasi mobil'}
							instruments={travel_train}
							loading={loading}
							searchText={searchText}
							onChangeSearch={(text: string) => this.onChangeParams(text, 'searchText')}
							onChangeRegions={(text: string) => this.onChangeParams(text, 'selecedRegion')}
							onChangeStatus={(text: string) => this.onChangeParams(text, 'selectedStatus')}
							onGetFeeders={this.getFeeders}
							selecedRegion={selectedRegion}
							selectedStatus={selectedStatus}
							regionOptions={regionOptions}
						/>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default Travel_train;
