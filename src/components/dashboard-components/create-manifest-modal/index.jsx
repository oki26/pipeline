// @flow
import type {OrderType} from '../../../types'
import React from 'react';
import { CustomInput, Collapse, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup } from 'reactstrap';
import Backendless from '../../../services/backendless'
import validator from 'validator'
import Async from 'react-select/lib/Async'

type Props = {
  isOpen: boolean,
  toggle: Function,
  onGetCargo: Function,
  className?: string,
  order: OrderType,
  onSubmit: Function,
}

type State = {
  cities: Array<Object>,
  cargo_name: string,
  city: ?Object,
  contact_name: string,
  contact_number: string,
  contact_email: string,
  departured_time: string,
  arrival_date: string,
  cargo_address: string,
  thirdPartyObj: ?Object,
  isSendWithCargo: boolean,
}
class CreateManifestModal extends React.Component<Props, State> {
  state = {
    cities: [],
    cargo_name: '',
    city: null,
    contact_name: '',
    contact_number: '',
    contact_email: '',
    departured_time: '',
    arrival_date: '',
    cargo_address: '',
    thirdPartyObj: null,
    isSendWithCargo: false,
  }

  componentDidMount() {
		this.getCities()
  }
  
  onChange = (e: Object) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  onChangeSearch = (obj: Object) => {
    if (!obj.data) return
    const { name, address, city, contacts  } = obj.data
    this.setState({
      thirdPartyObj: obj,
      cargo_name: name,
      cargo_address: address,
      city: city,
      contact_name: contacts[0] ? `${contacts[0].first_name} ${contacts[0].last_name}` : '',
      contact_email: contacts[0] ? contacts[0].email : '',
      contact_number: contacts[0] ? contacts[0].phone : '',
    })
  }

	getCities = async () => {
		try {
			const res = await Backendless.Data.of('cities').find()
			this.setState({
				cities: res
			})
		} catch(er) {
			console.log('fail get citie', er)
		}
  }
  
  onSubmit = async () => {
    try {
      const {
        cargo_name, city, contact_name, thirdPartyObj, cities,
        contact_number, contact_email, cargo_address, isSendWithCargo,
        departured_time, arrival_date
      } = this.state
      let tp = thirdPartyObj && isSendWithCargo ? thirdPartyObj.data : null
      // create third party
      if (thirdPartyObj === null && isSendWithCargo) {
        const nameArr = contact_name.split(' ')
        const contactObj = Backendless.Data.of('third_party_contacts').saveSync({
          email: contact_email,
          first_name: nameArr[0],
          last_name: nameArr.join(' '),
          phone: contact_number,
        })
        tp = Backendless.Data.of('third_parties').saveSync({
          address: cargo_address, name: cargo_name
        })
        const cityChild = cities.filter(c => c.objectId === city)
        tp = Backendless.Data.of('third_parties').setRelationSync(tp, 'city', cityChild)
        tp = Backendless.Data.of('third_parties').setRelationSync(tp, 'contacts', [contactObj])
      }
      this.props.onSubmit(tp, arrival_date, departured_time)
    } catch (er) {

    }
  }

  timeout = null
  loadOptions = (input: string, callback: Function) => {
    if (this.timeout) clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.autoComplate(input, callback), 1000)
  }

  autoComplate = async (input: string, callback: Function) => {
    try {
      const query = Backendless.DataQueryBuilder.create()
      query.setRelationsDepth(1)
      if (input) query.setWhereClause(`name LIKE '%${input}%'`)
      const res = await Backendless.Data.of('third_parties').find(query)
      const options = res.map(tp => ({ value: tp.name, data: tp, label: tp.name }))
      callback(options)
    } catch (er) {
      console.log(er)
      callback(er, {options: [], complate: false})
    }
  }

  render() {
    const { isOpen, toggle, className } = this.props
    const { 
      cities, cargo_name, city, contact_name, thirdPartyObj,
      contact_number, contact_email, departured_time,
      arrival_date, cargo_address,
      isSendWithCargo,
    } = this.state
    const valid_contact_number  = validator.isMobilePhone(contact_number)
    const valid_contact_email  = validator.isEmail(contact_email)
    const valid_cargo_address  = !validator.isEmpty(cargo_address)
    const valid_cargo_name  = !validator.isEmpty(cargo_name)
    const valid_city  = city !== null
    const valid_contact_name = !validator.isEmpty(contact_name)
    const valid_arrival_date = !validator.isEmpty(arrival_date)
    const valid_departured_time = !validator.isEmpty(departured_time)
    let validAll = valid_arrival_date && valid_departured_time
    if (isSendWithCargo) {
      validAll = valid_arrival_date && valid_departured_time && valid_contact_number && valid_contact_email &&
        valid_cargo_address && valid_cargo_name && valid_city && valid_contact_name
    }

    return (
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Buat Manifest</ModalHeader>
        <ModalBody>
          <div>
            <CustomInput
              checked={isSendWithCargo} type="checkbox"
              name="isSendWithCargo" id="send_with_cargo"
              label="Kirim Dengan Kargo / Kalog"
              onChange={() => this.setState({ isSendWithCargo: !isSendWithCargo })}
            /> 
          </div>
          <Collapse isOpen={isSendWithCargo} >
            <Async
              placeholder="Cari Third Party"
              name="thirdPartyObj"
              value={thirdPartyObj}
              onChange={this.onChangeSearch}
              loadOptions={this.loadOptions}
            />
            <FormGroup className="mt-2">
              <Label for="cargo_name">Nama Kargo</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} name="cargo_name" value={cargo_name}
                id="cargo_name" placeholder="Nama Kargo" />
            </FormGroup>
            <FormGroup>
              <Label for="cargo_address">Alamat</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} name="cargo_address" value={cargo_address}
                id="cargo_address" placeholder="Alamat"
              />
            </FormGroup>
            <FormGroup>
              <Label for="city">Kota</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} type="select" name="city"
                value={city ? city.objectId : null} id="city"
              >
                <option key={""} value={""}>{""}</option>
                {cities.map(city =>
                  <option key={city.name} value={city.objectId}>{city.name}</option>
                )}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="contact_name">Kontak Kargo</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_name"
                value={contact_name} id="contact_name" placeholder="Kontak Kargo"
              />
            </FormGroup>
            <FormGroup>
              <Label for="contact_number">Nomor Kontak</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_number"
                value={contact_number} id="contact_number" placeholder="Nomor Kontak"
              />
            </FormGroup>
            <FormGroup>
              <Label for="contact_email">Email Kontak</Label>
              <Input
                onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_email"
                value={contact_email} id="contact_email" placeholder="Kontak Email"
              />
            </FormGroup>
          </Collapse>
          <FormGroup className="mt-2">
            <Label for="departured_time">Keberangkatan</Label>
            <Input
              onChange={this.onChange} name="departured_time" value={departured_time}
              id="departured_time" type="datetime-local"
            />
          </FormGroup>
          <FormGroup className="mt-2">
            <Label for="arrival_date">Estimasi Kedatangan</Label>
            <Input
              onChange={this.onChange} name="arrival_date" value={arrival_date}
              id="arrival_date" type="datetime-local"
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color={validAll ? 'primary' : 'secondary'} disabled={!validAll} onClick={this.onSubmit}>Submit</Button>{' '}
          <Button
            color="secondary"
            onClick={() => this.setState({
              thirdPartyObj: null,
              city: null,
              cargo_name: '',
              cargo_address: '',
              contact_name: '',
              contact_number: '',
              contact_email: '',
            })}
          >
            Reset
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default CreateManifestModal;