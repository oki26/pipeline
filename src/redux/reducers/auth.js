import * as constants from '../constants/auth'

const initialState: Auth = {
  email: "",
  password: "",
  firstName: "",
  lastName: "",
  message: '',
  loading: false,
  success: true,
  phoneNumber: '',
  account: {},
  passwordAccount: ""
}

const authReducer = (state: Auth = initialState, action: actions): Object => {
  switch(action.type){
    case constants.AUTH_SET_EMAIL:
      return { ...state, email: action.payload.email, message: '' }
    case constants.AUTH_SET_PASSWORD:
      return { ...state, password: action.payload.password, message: '' }
    case constants.AUTH_SET_FIRSTNAME:
      return { ...state, firstName: action.payload.firstName, message: '' }
    case constants.AUTH_SET_LASTNAME:
      return { ...state, lastName: action.payload.lastName, message: '' }
    case constants.AUTH_SET_PHONE_NUMBER:
      return { ...state, phoneNumber: action.payload.phoneNumber, message: '' }
    case constants.AUTH_SET_MESSAGE:
      return { ...state, message: action.payload.message, loading: false }
    case constants.AUTH_SET_LOADING:
      return { ...state, loading: action.payload.loading }
    case constants.AUTH_SET_SUCCESS:
      return { ...state, success: action.payload.success, message: action.payload.message, loading: false }
    case constants.AUTH_SET_ACCOUNT:
      return { ...state, account: action.payload.account, passwordAccount: action.payload.password }
    case constants.AUTH_SET_LOGOUT:
      return state
    default:
      return state
  }
}

export default authReducer
