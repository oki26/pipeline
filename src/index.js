// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { Provider } from 'react-redux'
import HTML5Backend from 'react-dnd-html5-backend'
import { DragDropContext } from 'react-dnd'
import smoothscroll from 'smoothscroll-polyfill';
import reducer from './redux/reducers'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage'
import indexRoutes from './routes/index.jsx';
import './assets/scss/style.css';
smoothscroll.polyfill();

const persistConfig = {
  key: 'root',
  storage,
  // blacklist: ['auth']
}

const persistedReducer = persistReducer(persistConfig, reducer)

export const history = createBrowserHistory();
let store = createStore(persistedReducer, applyMiddleware(thunkMiddleware))
let persistor = persistStore(store)
const App = () => (
  <Provider store={store} >
    <PersistGate loading={null} persistor={persistor}>
      <Router history={history}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route path={prop.path} key={key} component={prop.component} />;
          })}
        </Switch>
      </Router>
    </PersistGate>
  </Provider>
)
const WrappedApp = DragDropContext(HTML5Backend)(App)
ReactDOM.render(<WrappedApp />, document.getElementById('root')); 
