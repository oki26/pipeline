// @flow
import React, { Component } from 'react'
import {
  Button, Modal, ModalHeader, ModalBody,
  ModalFooter, ListGroup, ListGroupItem,
  Row, Col,
} from 'reactstrap';
import type {OrderType} from '../../../types'
import BackendLess from '../../../services/backendless'
import { history } from '../../../index'

type Props = {
  isOpen: boolean,
  className?: string,
  toggle: Function,
  onSubmit: Function,
  orders: Array<OrderType>
}
type State = {
  loading: boolean,
}

export default class ManifestPreviewModal extends Component<Props, State> {
  state = {
    loading: false,
  }
  onSubmit = async () => {
    const { orders } = this.props;
    const order = orders[0]
    this.setState({ loading: true })
    try {
      const manifest = await BackendLess.Data.of('triplogistic_manifest').save({
        org_code: order.origin_code,
        dest_code: order.destination_code,
      })
      const res = await BackendLess.Data.of('triplogistic_manifest').setRelation(manifest, 'triplogistic_orders', orders)
      this.setState({ loading: false })
      console.log('res => ', res)
      alert('Create Manifest Success!')
      // history.push('logistics-manifest')
    } catch (er) {
      this.setState({ loading: false })
      console.log(er)
    }
  }


  render() {
    const { loading } = this.state
    const { isOpen, toggle, className, onSubmit, orders } = this.props
    const totalWeight = orders.reduce((carry, data) => carry += data.package.weight_packages, 0)
    return (
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Preview Manifest</ModalHeader>
        <ModalBody style={{ height: 350, overflow: 'auto' }}>
          <ListGroup>
            {orders.map((order, idx) =>
              <ListGroupItem key={`order-${idx}`}>
                <div className="pt-1">
                    <b>{order.package.name_packages}</b><br />
                    {order.package.weight_packages} Kg<br />
                </div>
                <div className="pt-1">
                  Pengirim: <b>{order.from.contact_name} ({order.from.phone_number})</b><br/>
                  {order.from.address}
                </div>
                <div className="pt-1">
                  Penerima: <b>{order.to.contact_name} ({order.to.phone_number})</b><br/>
                  {order.to.address}
                </div>
              </ListGroupItem>
            )}
          </ListGroup>
        </ModalBody>
        <ModalFooter>
          <div>
            Total Berat: {totalWeight} Kg
          </div>
          <br />
          <Button color="primary" disabled={loading} onClick={onSubmit}>Submit</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }
}
