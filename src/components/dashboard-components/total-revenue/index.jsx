// @flow
import React from "react";
import {
	Card,
  CardBody,
  Input,
} from 'reactstrap';
import moment from 'moment';
import Picker from 'rc-calendar/lib/Picker';
import RangeCalendar from 'rc-calendar/lib/RangeCalendar';
// import TimePickerPanel from 'rc-time-picker/lib/Panel';
import enUS from 'rc-calendar/lib/locale/en_US';
import 'rc-calendar/assets/index.css';
import { type } from "os";
const now = moment();
const defaultCalendarValue = now.clone();
defaultCalendarValue.add(-1, 'month');

function newArray(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}

function disabledDate(current) {
  const date = moment().add(1, 'day');
  date.hour(0);
  date.minute(0);
  date.second(0);
  return current.isAfter(date);  // can not select after before today
}


const formatStr = 'DD-MMMM-YYYY';
function format(v) {
  return v ? v.format(formatStr) : '';
}

function isValidRange(v) {
  return v && v[0] && v[1];
}

type PropType = {
  hoverValueCalendar: Array<string>,
  onHoverChangeCalendar: Function,
  valueCalendar: Array<string>,
  onChangeCalendar: Function
}

class TotalRevenue extends React.Component<PropType>{
	render() {
    const calendar = (
      <RangeCalendar
        hoverValue={this.props.hoverValueCalendar}
        onHoverChange={this.props.onHoverChangeCalendar}
        showWeekNumber={false}
        dateInputPlaceholder={['start', 'end']}
        defaultValue={[now, now.clone().add(1, 'months')]}
        locale={enUS}
        disabledDate={disabledDate}
      />
    );

		return (
			<Card>
				<CardBody>
					<div className="d-flex align-items-center mb-3">
						<div>
							<h1 className="font-bold mb-0">Rp 600,210,890</h1>
							<span>Total Revenue</span>
						</div>
						<div className="ml-auto">
							<Picker
                value={this.props.valueCalendar}
                onChange={this.props.onChangeCalendar}
                animation="slide-up"
                calendar={calendar}
              >
                {
                  ({ value }) => {
                    return (<span>
                        <Input
                          placeholder="please select"
                          style={{ width: 290 }}
                          readOnly
                          value={isValidRange(value) && `${format(value[0])}  to  ${format(value[1])}` || ''}
                        />
                        </span>);
                  }
                }
              </Picker>
						</div>
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non pharetra ligula, sit amet laoreet arcu.Integer.</p>
					<a href="" className="font-bold">Download Report Excel</a>
				</CardBody>
			</Card>
		);
	}
}

export default TotalRevenue;
