import React, { Component } from 'react'
import { Card, CardBody, CardHeader, Row, Col, Button } from 'reactstrap'
import html2pdf from "html2pdf.js"
import moment from 'moment'

export default class BarcodeCard extends Component {
  generatePDF = () => {
    const currentTime = new Date().toISOString()
    let element = document.getElementById('qrcodes')
    const pdffileName = `${'Barcode'}-${currentTime}.pdf`
    const opt = {
      margin: 5,
      filename: pdffileName,
      image: { type: "jpeg", quality: 0.98 },
      html2canvas: { scale: 1, dpi: 192, letterRendering: true },
      pagebreak: { mode: 'avoid-all' }
    }
    html2pdf().set(opt).from(element).save()
  }
  
  render() {
    const { title, datas = [] } = this.props

    return (
      <Card>
        <CardHeader className="d-flex justify-content-between">
          {title}
          <Button className="ml-3" outline color="primary" onClick={this.generatePDF}>
              Download PDF
          </Button>
        </CardHeader>
        <CardBody id="qrcodes">
          <Row>
            {datas.map((data, idx1) => 
              <Col className="p-1" key={`key1-${idx1}`} md={4} lg={4} xl={4} sm={6} xs={12}>
                <div className="border border-primary p-3" style={{ minHeight: 500   }}>
                  <div  className="d-flex align-items-center justify-content-center">
                    <img alt="barcode" src={data.barcode_uri} style={{ height: 150, width: 150 }}/>
                  </div>
                  <div  className="d-flex align-items-center justify-content-center">
                    <b>{data.tracking_id}</b>
                  </div>
                  {Object.keys(data).map((key, idx2) => {
                    if (!data[key]) return null
                    // if (typeof data[key] === 'object') {
                    //   return Object.keys(data[key]).map(childkey =>
                    //     typeof data[key][childkey] !== 'object' && <div><b>{key} {childkey}</b> : {data[key][childkey]}</div>
                    //   )
                    // }
                    if (
                      key === 'barcode_uri' || key === '___class' ||
                      key === 'ownerId' || key === 'updated' ||
                      key === 'objectId' || key === 'trackingId'
                    ) return null
                    if (key === 'created' || key === 'pickup_time') {
                      return <div key={`idx2-${idx2}`}>{key} : <br/> <b>{moment(data[key]).format('DD-MM-YYYY HH:mm')}</b></div>
                    }

                    if (typeof data[key] !== 'object') {
                      return <div key={`idx2-${idx2}`}>{key} : <br/> <b>{data[key]}</b></div>
                    }
                  })}
                </div>
              </Col>
            )}
          </Row>
        </CardBody>
      </Card>
    )
  }
}
