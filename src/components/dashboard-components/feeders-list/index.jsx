// @flow
import type { FeederType } from '../../../types'
import ReactTable from 'react-table'
import React from "react";
import img1 from '../../../assets/images/users/1.jpg';

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  Row, Col
} from 'reactstrap';

export type Props = {
  feeders: Array<FeederType>,
  loading: boolean,
  onChangeSearch: Function,
  onGetFeeders: Function,
  onChangeRegions: Function,
  onChangeStatus: Function,
  selecedRegion: string,
  searchText: string,
  selectedStatus: string,
  regionOptions: Array<string>,
  onPressLocation: (feeder: FeederType) => void
}

const tableHeaders = [
	{
		Header: 'Nama Feeder',
		accessor: 'firstname',
		Cell: ({ original }: { original: FeederType }) => (
			<div className="d-flex no-block align-items-center">
				<div className="mr-2">
					<img src={original.avatar_thumbnail ? original.avatar_thumbnail : img1} alt="user" className="rounded-circle" width="45" />
				</div>
				<div className="">
					<h5 className="mb-0 font-16 font-medium">{original.firstname} {original.lastname}</h5><span>{original.phonenumber}</span>
				</div>
			</div>
		)
	},
	{ 
		Header: 'Wilayah',
		accessor: 'city',
		Cell: ({ original }: { original: FeederType }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{original.city ? original.city.name : 'Tidak Ada'}
			</div>
		)
	},
]

class FeedersList extends React.Component<Props> {
  render() {
    const {
      feeders, loading, searchText,
      onChangeSearch, onGetFeeders, onChangeRegions,
      onChangeStatus, selecedRegion, selectedStatus,
      regionOptions, onPressLocation
     } = this.props;
    return (
      <Card>
        <CardBody>
          <Row>
            <Col sm="12" xs="12" md="4" lg="4" xl="4">
              <CardTitle>List Feeder</CardTitle>
              <CardSubtitle>Data Kurir Triplogic</CardSubtitle>
            </Col>
            <Col sm="12" xs="12" md="8" lg="8" xl="8">
              <Row>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  <Input
                    type="select" className="custom-select" placeholder="Pilih Wilayah"
                    onChange={e => onChangeRegions(e.target.value)}
                    value={selecedRegion}
                  >
                    <option value="">Semua Wilayah</option>
                      {regionOptions.map((region, idx) =>
                        <option value={region} key={`region-${idx}`}>{region}</option>
                      )}
                  </Input>
                </Col>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  <Input
                    type="select" className="custom-select" placeholder="Status Feeder"
                    onChange={e => onChangeStatus(e.target.value)}
                    value={selectedStatus}
                  >
                    <option value="0">See Working Status</option>
                    <option value="active = true">Active</option>
                    <option value="active = false">Non-Active</option>
                    <option value="blocked = true">Blokir</option>
                  </Input>
                </Col>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  <InputGroup style={{ maxHeight: 36 }}>
                    <Input
                      type="text" placeholder="Cari Feeder"
                      onChange={e => onChangeSearch(e.target.value)} value={searchText}
                    />
                    <InputGroupAddon addonType="append">
                      <Button color="primary" onClick={() => onChangeSearch(searchText)}>
                        <i className="ti-search"></i>
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                </Col>
              </Row>
            </Col>

          </Row>
        </CardBody>
        <CardBody>
          <ReactTable
            columns={tableHeaders.concat([
              { 
                Header: 'On Work',
                accessor: 'online',
                Cell: ({ original }: { original: FeederType }) => (
                  <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
                      <i className={`fa fa-circle ${original.online ? 'text-success' : 'text-secondary'}`} id="tlp1"></i>
                  </div>
                )
              },
              {
                Header: 'Actions',
                sortable: false, 
                accessor: 'actions',
                Cell: ({ original }: { original: FeederType }) => (
                  <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
                    <Button className="btn m-1" outline color="secondary" onClick={() => onPressLocation(original)}>
                      <i className="fa fa-location-arrow" id="tlp1"></i> Lokasi
                    </Button>{' '}
                    <Button className="btn m-1" outline color="info">
                      <i className="fa fa-eye" id="tlp1"></i> Open Profile
                    </Button>{' '}
                    <Button className="btn m-1" outline color="success">
                      <i className="fa fa-eye" id="tlp1"></i>Tugas
                    </Button>{' '}
                  </div>
                )
              }
            ])}
            defaultPageSize={20}
            showPaginationBottom={true}
            className="-striped -highlight"
            data={feeders}
            loading={loading}
            filterable={false}
            onFetchData={(tableState: Object) => 
              onGetFeeders(tableState.pageSize, tableState.page * tableState.pageSize, tableState.sorted)
            }
          />
        </CardBody>
      </Card>
    );
  }
}

export default FeedersList;
