// @flow
import type {OrderType} from '../../../types'
import React from 'react';
import { Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup } from 'reactstrap';
import Backendless from '../../../services/backendless'
import validator from 'validator'
import Async from 'react-select/lib/Async'
import './style.css'

type Props = {
  isOpen: boolean,
  toggle: Function,
  onGetCargo: Function,
  className?: string,
  order: OrderType
}

type State = {
  cities: Array<Object>,
  cargo_name: string,
  city: ?Object,
  contact_name: string,
  contact_number: string,
  contact_email: string,
  departure_date: string,
  origin_airport: string,
  arrival_date: string,
  destination_airport: string,
  price: string,
  thirdparty_name: string,
  cargo_address: string,
  thirdPartyObj: ?Object
}
class ModalExample extends React.Component<Props, State> {
  state = {
    cities: [],
    cargo_name: '',
    city: null,
    contact_name: '',
    contact_number: '',
    contact_email: '',
    departure_date: '',
    origin_airport: '',
    arrival_date: '',
    destination_airport: '',
    price: '',
    thirdparty_name: '',
    cargo_address: '',
    thirdPartyObj: null
  }

  componentDidMount() {
		this.getCities()
  }
  
  onChange = (e: Object) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  onChangeSearch = (obj: Object) => {
    if (!obj.data) return
    const { name, address, city, contacts  } = obj.data
    this.setState({
      thirdPartyObj: obj,
      cargo_name: name,
      cargo_address: address,
      city: city,
      contact_name: contacts[0] ? `${contacts[0].first_name} ${contacts[0].last_name}` : '',
      contact_email: contacts[0] ? contacts[0].email : '',
      contact_number: contacts[0] ? contacts[0].phone : '',
    })
  }

	getCities = async () => {
		try {
			const res = await Backendless.Data.of('cities').find()
			this.setState({
				cities: res
			})
		} catch(er) {
			console.log('fail get citie', er)
		}
  }
  
  onSubmit = async () => {
    try {
      const { cargo_name, city, contact_name, thirdPartyObj,
        contact_number, contact_email, departure_date, origin_airport,
        arrival_date, destination_airport, price, cargo_address, } = this.state
      let tp = thirdPartyObj ? thirdPartyObj.data : null
      // create third party
      if (thirdPartyObj === null) {
        const nameArr = contact_name.split(' ')
        const contactObj = Backendless.Data.of('third_party_contacts').saveSync({
          email: contact_email,
          first_name: nameArr[0],
          last_name: nameArr.join(' '),
          phone: contact_number,
        })
        tp = Backendless.Data.of('third_parties').saveSync({
          address: cargo_address, name: cargo_name
        })
        tp = Backendless.Data.of('third_parties').setRelationSync(tp, 'city', [city])
        tp = Backendless.Data.of('third_parties').setRelationSync(tp, 'contacts', [contactObj])
      }
      // 
      let cargoObj = Backendless.Data.of('cargo_airplane_travel').saveSync({
        departure_date,  arrival_date, price,
        origin_airport: String(origin_airport).toLowerCase(),
        destination_airport: String(destination_airport).toLowerCase(),
      })
      cargoObj =  Backendless.Data.of('cargo_airplane_travel').setRelationSync(cargoObj, 'third_party', [tp])
      this.props.onGetCargo()
      this.props.toggle();
    } catch (er) {

    }
  }

  timeout = null
  loadOptions = (input: string, callback: Function) => {
    if (this.timeout) clearTimeout(this.timeout)
    this.timeout = setTimeout(() => this.autoComplate(input, callback), 1000)
  }

  autoComplate = async (input: string, callback: Function) => {
    try {
      const query = Backendless.DataQueryBuilder.create()
      query.setRelationsDepth(1)
      if (input) query.setWhereClause(`name LIKE '%${input}%'`)
      const res = await Backendless.Data.of('third_parties').find(query)
      const options = res.map(tp => ({ value: tp.name, data: tp, label: tp.name }))
      callback(options)
    } catch (er) {
      console.log(er)
      callback(er, {options: [], complate: false})
    }
  }

  render() {
    const { isOpen, toggle, className } = this.props
    const { 
      cities, cargo_name, city, contact_name, thirdPartyObj,
      contact_number, contact_email, departure_date, origin_airport,
      arrival_date, destination_airport, price, cargo_address,
    } = this.state
    const valid_contact_number  = validator.isMobilePhone(contact_number)
    const valid_contact_email  = validator.isEmail(contact_email)
    const valid_departure_date  = !validator.isEmpty(departure_date)
    const valid_origin_airport  = !validator.isEmpty(origin_airport)
    const valid_arrival_date  = !validator.isEmpty(arrival_date)
    const valid_destination_airport  = !validator.isEmpty(destination_airport)
    const valid_price = validator.isNumeric(price)
    const valid_cargo_address  = !validator.isEmpty(cargo_address)
    const valid_cargo_name  = !validator.isEmpty(cargo_name)
    const valid_city  = city !== null
    const valid_contact_name = !validator.isEmpty(contact_name)
    const validAll = valid_contact_number && valid_contact_email && valid_departure_date && valid_origin_airport && valid_arrival_date && valid_destination_airport && valid_price
     && valid_cargo_address && valid_cargo_name && valid_city && valid_contact_name
    return (
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Tambah Kargo</ModalHeader>
        <ModalBody>
            <Async
              placeholder="Cari Third Party"
              name="thirdPartyObj"
              value={thirdPartyObj}
              onChange={this.onChangeSearch}
              loadOptions={this.loadOptions}
            />
          <FormGroup className="mt-2">
            <Label for="cargo_name">Nama Kargo</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} name="cargo_name" value={cargo_name} id="cargo_name" placeholder="Nama Kargo" />
          </FormGroup>
          <FormGroup>
            <Label for="cargo_address">Alamat</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} name="cargo_address" value={cargo_address} id="cargo_address" placeholder="Alamat" />
          </FormGroup>
          <FormGroup>
            <Label for="city">Kota</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} type="select" name="city" value={city ? city.name : null} id="city">
              <option key={""} value={""}>{""}</option>
              {cities.map(city =>
                <option key={city.name} value={city.name}>{city.name}</option>
              )}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="contact_name">Nama Kontak</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_name" value={contact_name} id="contact_name" placeholder="Nama Kontak" />
          </FormGroup>
          <FormGroup>
            <Label for="contact_number">Nomor Kontak</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_number" value={contact_number} id="contact_number" placeholder="Nomor Kontak" />
          </FormGroup>
          <FormGroup>
            <Label for="contact_email">Email Kontak</Label>
            <Input onChange={this.onChange} disabled={!!thirdPartyObj} type="text" name="contact_email" value={contact_email} id="contact_email" placeholder="Kontak Email" />
          </FormGroup>
          <Label>Asal</Label>
          <Row>
            <Col className="sm-12 xs-12 md-6 lg-6 xl-6">
              <Input onChange={this.onChange} type="date" name="departure_date" value={departure_date} id="departure_date" placeholder="Tanggal" />
            </Col>
            <Col className="sm-12 xs-12 md-6 lg-6 xl-6">
              <Input onChange={this.onChange} type="text" name="origin_airport" value={origin_airport} id="origin_airport" placeholder="Kode Bandara" />  
            </Col>
          </Row>
          <Label>Tujuan</Label>
          <Row>
            <Col className="sm-12 xs-12 md-6 lg-6 xl-6">
              <Input onChange={this.onChange} type="date" name="arrival_date" value={arrival_date} id="arrival_date" placeholder="Tanggal" />
            </Col>
            <Col className="sm-12 xs-12 md-6 lg-6 xl-6">
              <Input onChange={this.onChange} type="text" name="destination_airport" value={destination_airport} id="destination_airport" placeholder="Kode Bandara" /> 
            </Col>
          </Row>
          <FormGroup>
            <Label for="price">Harga</Label>
            <Input onChange={this.onChange} type="text" name="price" value={price} id="price" placeholder="Harga" />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color={validAll ? 'primary' : 'secondary'} disabled={!validAll} onClick={this.onSubmit}>Submit</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ModalExample;