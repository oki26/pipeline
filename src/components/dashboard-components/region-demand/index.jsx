import React from "react";

import {
	Card,
	CardBody,
	CardTitle,
	CardSubtitle, Input
} from 'reactstrap';

import Chart from 'react-c3-component';
import 'c3/c3.css';

class RegionDemand extends React.Component {
	render() {
		return (
			/*--------------------------------------------------------------------------------*/
			/* Used In Dashboard-3 [Ecommerce]                                                */
			/*--------------------------------------------------------------------------------*/
			<Card>
				<CardBody>
					<div className="d-md-flex align-items-center">
						<div>
							<CardTitle>Regions Demand</CardTitle>
							<CardSubtitle>Total Demand in Jabodetabek of the Week</CardSubtitle>
						</div>
            <div className="ml-auto d-flex no-block align-items-center">
              <div className="dl">
                <Input type="select" className="custom-select">
                  <option value="0">DKI Jakarta</option>
                  <option value="1">Banten</option>
                  <option value="2">Jawa Barat</option>
                  <option value="3">Jawa Tengah</option>
                  <option value="3">Jawa Timur</option>
                  <option value="3">Bali</option>
                  <option value="3">Sumatera Barat</option>
                </Input>
              </div>
            </div>
					</div>
					<div className="product-sales">
						<Chart
							style={{ height: '400px', width: '100%' }}
							config={{
								data: {
									columns: [
                    ['DKI Jakarta', 100, 3, 12, 10, 45, 12, 60],
                    ['Tangerang', 11, 5, 12, 9, 5, 100, 20],
                    ['Bogor', 20, 30, 10, 11, 4, 12, 10],
                    ['Depok', 10, 23, 1, 10, 2, 11, 61],
                    ['Bekasi', 11, 1, 32, 1, 45, 8, 64]
									],
									type: 'bar'
								},
								axis: {
									y: {
										show: true,
										tick: {
											count: 0,
											outer: false
										}
									},
									x: {
										show: true
									}
								},
								bar: {
									width: 8
								},
								padding: {
									top: 40,
									right: 10,
									bottom: 0,
									left: 20,
								},
								point: {
									r: 0
								},
								legend: {
									hide: false
								},
								color: {
									pattern: ['#ee5253', '#0abde3', '#ff9f43', '#10ac84', '#222f3e']
								}
							}}
						/>
					</div>
				</CardBody>
			</Card>
		);
	}
}

export default RegionDemand;
