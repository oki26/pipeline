// @flow
import type { UserType } from '../../../types'
import React from "react";
import ReactTable from "react-table";
import qrcode from 'qrcode'
import { connect } from 'react-redux'
import checkboxHOC from "react-table/lib/hoc/selectTable";
import {
	Card, CardBody, CardTitle,
	Button, Col, InputGroup, InputGroupAddon,
	Row, CardSubtitle, Input
} from 'reactstrap';
import moment from "moment"
import accounting from 'accounting'
import Picker from 'rc-calendar/lib/Picker'
import RangeCalendar from 'rc-calendar/lib/RangeCalendar'
import enUS from 'rc-calendar/lib/locale/en_US'
import "react-table/react-table.css";
import Backendless from '../../../services/backendless'
import { ModalUserBalance } from '../../../components/dashboard-components';
// import { history } from '../../../index'

const CheckboxTable = checkboxHOC(ReactTable);
type Props = {
	account: Object,
}

type RowDataType = {
	objectId: ?string,
	first_name: ?string,
	last_name: ?string,
	email: ?string,
	balance: ?string | ?number,
	order: UserType,
}

type State = {
	isOpenModal: boolean,
	modalAction: boolean,
	loading: boolean,
	obj: Object,
	orders: Array<RowDataType>,
	detailData: RowDataType | Object | null,
	loadingOrders: boolean,
	loadingDetail: boolean,
	dateRange: Array<string>,
	searchText: string,
	selection: Array<string>,
	selectionOrder: Array<UserType>,
	selectAll: boolean,
	pages: number,
  pageSize: number,
  page: number,
}

const tableHeaders = [
	{ Header: "ID", accessor: "objectId"	},
	{ Header: "Nama Depan", accessor: "first_name" },
	{ Header: "Nama Belakang", accessor: "last_name" },
	{ Header: "Email", accessor: "email" },
	{ Header: "Saldo", accessor: "balance" }
]

const calendar = (
	<RangeCalendar
		showWeekNumber={false}
		dateInputPlaceholder={['Dari Tanggal', 'Sampai Tanggal']}
		defaultValue={[moment().subtract(1, 'month').format(), moment()]}
		locale={enUS}
		disabledDate={current => current.isAfter(moment().add(1, 'days').hour(0))}
	/>
);

class Orders extends React.Component<Props, State> {
	tableRef = null
	constructor(props: Props) {
		super(props);
		this.state = {
			loading: false,
			modalAction: false,
			obj: {},
			orders: [],
			detailData: {},	
			loadingOrders: false,
			loadingDetail: false,
			dateRange: [],
			searchText: '',
			selectAll: false,
			selection: [],
			selectionOrder: [],
			pageSize: 20,
			isOpenModal: false,
			orderOfBag: [],
			pages: -1,
			page: 0,
		};
	}

	/* retrieve data orders */
	getUserBalance = async (
		pageSize: number = 20, offset: number = 0, page: number = 0,
		sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		try {
			this.setState({ loadingOrders: true })
			const { dateRange, searchText } = this.state
			
			// set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(2).setSortBy(sortQuery)
				.setWhereClause(`regular is not null`)

			// filter by date
			if (dateRange.length === 2) {
					// dateRange.sort((a, b) => moment(a).isAfter(b) ? -1 : 1)
					queryBuilder.setWhereClause(`created >= "${moment(dateRange[0]).format('MM-DD-YYYY')}" and created < "${moment(dateRange[1]).add(1, 'day').format('MM-DD-YYYY')}"`)
			}
			
			// query search
			if (searchText !== '') {
				queryBuilder.setWhereClause(`
					(email LIKE '%${searchText}%' or
					regular.first_name LIKE '%${searchText}%' or
					regular.last_name LIKE '%${searchText}%' or
					email LIKE '%${searchText}%' or
					regular.balance LIKE '%${searchText}%' ) and
					(regular is not null)`)
			}
			const res: Array<UserType> = await Backendless.Data.of('Users').find(queryBuilder)
			const count: number = await Backendless.Data.of('Users').getObjectCount(queryBuilder)
			const transformed: Array<RowDataType> =  res.map((data: UserType, index): RowDataType => {
				return ({
					_id: data.regular.objectId,
					objectId: data.regular.objectId,
					first_name: data.regular.first_name,
					last_name: data.regular.last_name,
					email: data.email,
					balance: accounting.formatMoney(data.regular.balance, 'Rp. ', 0, '.'),
					order: data
				})
			})
			this.setState({
				orders: transformed, loadingOrders: false,
				pages: count / offset, pageSize: pageSize, page: page
			})
		} catch(er) {
			this.setState({ loadingOrders: false })
			console.log(er)
		}
	}

	/** navigate to order detail page */
	// goDetailPage = (event: Event, row: { original: RowDataType }) => {
	// 	event.preventDefault();
	// 	history.push(`/order/${row.original.objectId || 'null'}`)
	// }

	timeout = null
	onChage = (e: Object) => {
		this.setState({ [e.target.name]: e.target.value }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getUserBalance, 1000)
		})
	}


  toggleSelection = async (key: string, shift: any, row: Object) => {
		let selection = [...this.state.selection];
		let selectionOrder = [...this.state.selectionOrder]
    const keyIndex = selection.indexOf(key);
    if (keyIndex >= 0) {
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ];
      selectionOrder = [
        ...selectionOrder.slice(0, keyIndex),
        ...selectionOrder.slice(keyIndex + 1)
      ];
    } else {
			selection.push(key);
			try {
				const barcode_uri = await qrcode.toDataURL(row.order.tracking_id)
				selectionOrder.push({ ...row.order, barcode_uri })
			} catch (er) {}
    }
    this.setState({ selection, selectionOrder });
  };

	/** toggle selection row at table */
  toggleAll = async () => {
    const selectAll = this.state.selectAll ? false : true;
    const selection = [];
		const selectionOrder = [];
		const promises = []
    if (selectAll && this.tableRef !== null) {
      const wrappedInstance = this.tableRef.getWrappedInstance();
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      currentRecords.forEach((item) => {
				selection.push(item._original._id);
				selectionOrder.push(item._original.order);
				if (item._original.order.tracking_id) {
					promises.push(qrcode.toDataURL(item._original.order.tracking_id))
				} else {
					promises.push(Promise.resolve(''))
				}
			});
			const results = await Promise.all(promises)
			results.forEach((barcode_uri, idx) => {
				selectionOrder[idx] = { ...selectionOrder[idx], barcode_uri }
			})
		}
    this.setState({ selectAll, selection, selectionOrder });
  };

	isSelected = (key: string) => this.state.selection.includes(key)

	renderPagination = ({ canNext, canPrevious, page, pageSize, sorted }: Object) => {
		const pages = [10, 20, 30, 40, 50]
		return (
			<Row className="pl-3 pr-3">
				<Col className="d-flex flex-row pt-2 pb-2 pr-3 pl-3">	
					<Button
						disabled={!canPrevious} color={canPrevious ?  'success' : 'secondary'} outline
						onClick={() => this.getUserBalance(pageSize, (page - 1) * pageSize, (page - 1), sorted)}
					>
						Previous
					</Button>
					<Input
						type="select" name="pageSize" value={pageSize} className="mr-3 ml-3"
						onChange={(e) => this.getUserBalance(e.target.value, page * e.target.value, page, sorted)}
					>
						{pages.map(num => <option key={`page-${num}`} value={num}>{num} Baris</option>)}
					</Input>
					<InputGroup className="mr-3" >
						<InputGroupAddon addonType="prepend" >Page</InputGroupAddon>
						<Input
							type="number" name="page" value={page}
							onChange={(e) => this.getUserBalance(pageSize, e.target.value * pageSize, e.target.value, sorted)}
						/>
					</InputGroup>
					<Button
						disabled={!canNext} color={canNext ?  'success' : 'secondary'}
						onClick={() => this.getUserBalance(pageSize, (page + 1) * pageSize, (page + 1), sorted)}
					>
						Next
					</Button>
				</Col>
			</Row>
		)
	}

	
	render() {
		const {
			dateRange, searchText, orders, loadingOrders,
			selectAll, pageSize, page, pages, modalAction,
			detailData
		} = this.state
		const { isSelected, toggleSelection, toggleAll } = this
		const tableProps = {
			selectAll,
      isSelected,
      toggleSelection,
			toggleAll,
			pageSize,
			selectType: "checkbox",
			defaultPageSize: 20,
			filterable: false,
			manual: true,
		}

		return (
			<div>
				<Card>
					<CardBody>
						<Row>
							<Col sm="12" xs="12" md="4" lg="4" xl="4">
							<CardTitle>
								<i className="mdi mdi-database mr-2"></i> User Balance
							</CardTitle>
								<CardSubtitle>List User Balance</CardSubtitle>
							</Col>
							<Col sm="12" xs="12" md="8" lg="8" xl="8">
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<InputGroup style={{ maxHeight: 36 }}>
											<Input
												type="text" placeholder="Cari"
												name="searchText" value={searchText}
												onChange={this.onChage}
											/>
											<InputGroupAddon addonType="append">
												<Button color="primary" 
												// onClick={() => onChangeSearch(searchText)}
												>
													<i className="ti-search"></i>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</Col>
								</Row>
							</Col>
						</Row>
					</CardBody>
					<CardBody>
						<CheckboxTable
							ref={r => (this.tableRef = r)}
							manual
              pages={pages}
							page={page}
							columns={tableHeaders
								.concat([
									{
										Header: "Action",
										accessor: "actions",
										sortable: false,
										filterable: false,
										Cell: (detailData) => (
											<center>
												<Button size="sm" color="primary" onClick={e => this.setState({modalAction: true, detailData: detailData.original})}>
													Action
												</Button>
											</center>)
									}
								])
							}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={orders}
							loading={loadingOrders}
							onFetchData={(tableState: Object) =>
								this.getUserBalance(tableState.pageSize, page * tableState.pageSize, page, tableState.sorted)}
							PaginationComponent={this.renderPagination}
							{...tableProps}
						/>
					</CardBody>
				</Card>
				<ModalUserBalance 
					toggle={() => this.setState({ modalAction: false, detailData: {} })}
					isOpen={modalAction}
					data={detailData}
					onReloadData={() => this.getUserBalance()}
				/>
			</div>
		);
	}
}

const mapState = (state: Object) => ({
	account: state.auth.account
})
export default connect(mapState, null)(Orders);
