import React from 'react';


class Footer extends React.Component {
  render() {
    return (
      <footer className="footer text-center">
        All Rights Reserved by PT Triplogic Semesta Raya. Designed and Developed by{' '}
        <a href="https://wrappixel.com">Triplogic Developer</a>.
      </footer>
    );
  }
}
export default Footer;
