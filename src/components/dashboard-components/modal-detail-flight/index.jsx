// @flow
import type { OrderType } from '../../../types'
import React from 'react'
import { 
  CustomInput, Collapse, Modal, 
  ModalHeader, ModalBody, ModalFooter, 
  FormGroup, Input, Label, TabContent, 
  TabPane, Nav, NavItem, NavLink, Card, 
  Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import Backendless from '../../../services/backendless'
import classnames from 'classnames';
import accounting from 'accounting';
import moment from 'moment';
import Lightbox from 'react-lightbox-component';

type RowDataType = {
	objectId: string,
	sla: string,
	package: string | null,
	weight: string | null,
	payment_status: string | null,
	amount: number | null,
	pickup_time: string | null,
	status_code: string | null,
	from: string | null,
	to: string | null,
	receiver: string | null,
	sender: string | null,
	order: OrderType,
}

type Props = {
  amount: string,
  payment_status: string,
  data: RowDataType,
  isOpen: boolean,
  toggle: Function,
  onReloadData: Function,
  className?: string,
}

type State = {
  amount: string | number | null,
  payment_status: string,
  order_status: string,
  receiver_name: string,
  receiver_time: string,
  activeTab: string
}

class ModalDetailFlight extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      amount: this.props.data.amount,
      payment_status: "",
      order_status: "",
      activeTab: '1',
      receiver_name: '',
      receiver_time: ''
    }
  }

  toggleTab = (tab: string) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onSubmitPaymentStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {payment_status} = this.state
    if (payment_status !== "") {
      await Backendless.Data.of('payment').save({objectId: data.order.payment.objectId, payment_status: payment_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  onSubmitOrderStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {order_status} = this.state
    if (order_status !== "") {
      await Backendless.Data.of('triplogistic_orders').save({objectId: data.order.objectId, status_code: order_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  render(){
    const {isOpen, toggle, className, data} = this.props
    const {amount, payment_status, order_status, receiver_name, receiver_time} = this.state

    console.log(data)
    return(
      <Modal isOpen={isOpen} toggle={toggle} className={className} size="lg" backdrop>
        <ModalHeader toggle={toggle}>Detail Order</ModalHeader>
        <ModalBody>
          <Row>
            <Col sm="12">
              <Card body style={{margin: 10}} >
                <CardTitle>Detail Paket</CardTitle>
                <Row>
                  <Col sm="6">
                    <CardText><b>Isi Paket</b></CardText>
                    <CardText><b>Berat Paket</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.package || ""}</CardText>
                    <CardText>{data.weight || ""}</CardText>
                  </Col>
                </Row>
              </Card>
              <Card body style={{margin: 10}} >
                <CardTitle>Detail Pemilik Paket</CardTitle>
                <Row>
                  <Col sm="6">
                    <CardText><b>Pemilik Paket</b></CardText>
                    {/* <CardText><b>Email</b></CardText> */}
                    <CardText><b>No Telepon</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.package.sender_name : " "}</CardText>
                    {/* <CardText>{data.order ? data.order.package.ema : " "}</CardText> */}
                    <CardText>{data.order ? data.order.package.phone_number_sender : " "}</CardText>
                  </Col>
                </Row>
              </Card>
              <Card body style={{margin: 10}} >
                <CardTitle>Detail Pengirim Paket</CardTitle>
                <Row>
                  <Col sm="6">
                    <CardText><b>Nama Pengirim</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.from.contact_name : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>Alamat</b></CardText>
                  </Col>
                  <Col sm="6">
                  <CardText>{data.order ? data.order.from.address : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>Email</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.from.email : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>No Telepon</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.from.phone_number : ""}</CardText>
                  </Col>
                </Row>
              </Card>
              <Card body style={{margin: 10}} >
                <CardTitle>Detail Penerima Paket</CardTitle>
                <Row>
                  <Col sm="6">
                    <CardText><b>Nama Penerima</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.to.contact_name : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>Alamat</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.to.address : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>Email</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.to.email : ""}</CardText>
                  </Col>
                </Row>
                <Row>
                  <Col sm="6">
                    <CardText><b>No Telepon</b></CardText>
                  </Col>
                  <Col sm="6">
                    <CardText>{data.order ? data.order.to.phone_number : ""}</CardText>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </ModalBody>
        <ModalFooter>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalDetailFlight