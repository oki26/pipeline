// @flow
import type { OrderType } from '../../../types'
import React from 'react'
import { 
  CustomInput, Collapse, Modal, 
  ModalHeader, ModalBody, ModalFooter, 
  FormGroup, Input, Label, TabContent, 
  TabPane, Nav, NavItem, NavLink, Card, 
  Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import Backendless from '../../../services/backendless'
import classnames from 'classnames';
import accounting from 'accounting';
import moment from 'moment';
import Lightbox from 'react-lightbox-component';

type RowDataType = {
	objectId: string,
	sla: string,
	package: string | null,
	weight: string | null,
	payment_status: string | null,
	amount: number | null,
	pickup_time: string | null,
	status_code: string | null,
	from: string | null,
	to: string | null,
	receiver: string | null,
	sender: string | null,
	order: OrderType,
}

type Props = {
  amount: string,
  payment_status: string,
  data: RowDataType,
  isOpen: boolean,
  toggle: Function,
  onReloadData: Function,
  className?: string,
}

type State = {
  amount: string | number | null,
  payment_status: string,
  order_status: string,
  receiver_name: string,
  receiver_time: string,
  activeTab: string
}

class ModalDetailOrder extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      amount: this.props.data.amount,
      payment_status: "",
      order_status: "",
      activeTab: '1',
      receiver_name: '',
      receiver_time: ''
    }
  }

  toggleTab = (tab: string) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onSubmitPaymentStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {payment_status} = this.state
    if (payment_status !== "") {
      await Backendless.Data.of('payment').save({objectId: data.order.payment.objectId, payment_status: payment_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  onSubmitOrderStatus = async () => {
    const {data, toggle, onReloadData} = this.props
    const {order_status} = this.state
    if (order_status !== "") {
      await Backendless.Data.of('triplogistic_orders').save({objectId: data.order.objectId, status_code: order_status})
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  render(){
    const {isOpen, toggle, className, data} = this.props
    const {amount, payment_status, order_status, receiver_name, receiver_time} = this.state

    return(
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Order Status</ModalHeader>
        <ModalBody>
          <FormGroup className="mt-2">
            <Label for="cargo_name">Status Paket</Label>
            <Input type="select" id="payment_status" 
              onChange={e => this.setState({ order_status: e.target.value })} 
              value={order_status !== '' ? order_status : data.status_code ? data.status_code : ""} >
              <option value="ORD" >Pending</option>
              <option value="TIORD" >Picked up by feeder</option>
              <option value="TIWO" >On The Way To Traveler</option>
              <option value="TICRG">Picked up by traveler</option>
              <option value="WD">Traveller arrived</option>
              <option value="TIWD">Deliver by Feeder</option>
              <option value="DO">Completed</option>
            </Input>
          </FormGroup>
          {order_status === "DO" ? (
            <div>
              <FormGroup className="mt-2">
                <Label>Nama Penerima</Label>
                <Input id="receiver_name" 
                  onChange={e => this.setState({ receiver_name: e.target.value })} value={receiver_name} />
              </FormGroup>
              <FormGroup className="mt-2">
                <Label>Waktu Diterima</Label>
                <Input id="receiver_time" type="datetime-local"
                  onChange={e => this.setState({ receiver_time: e.target.value })}/>
                <Label>{receiver_time ? moment(receiver_time).format('DD MMMM YYYY, HH:mm') : null}</Label>
              </FormGroup>
            </div>
          ):(
            data.status_code === "DO" ? (
              <div>
                <FormGroup className="mt-2">
                  <Label>Nama Penerima</Label>
                  <Input disabled id="receiver_name" value={data.order.real_receiver_name} />
                </FormGroup>
                <FormGroup className="mt-2">
                  <Label>Waktu Diterima</Label>
                  <Input disabled id="receiver_time" value={data.order.real_receiver_time ? moment(data.order.real_receiver_time).format('DD MMMM YYYY, HH:mm') : ""} />
                </FormGroup>
              </div>
            ): null
          )}
          <Button color="primary" onClick={this.onSubmitOrderStatus} >Update Order Status</Button>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle} >cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalDetailOrder