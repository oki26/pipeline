// @flow
import type { OrderType, TravelType, FeederType, TravelCarType, TripType, CardObject } from '../../../types'
import React, { Component } from 'react'
import moment from 'moment'
import { List } from 'react-content-loader'
import {
  Container, Row, Col, Card, CardTitle,
  CardBody, CardFooter, CardHeader, Alert, Button
} from 'reactstrap'
import { TriplogisticsOrderCard, ModalDetailTravel } from '../../../components/dashboard-components'
import './style.css'
import { DragableCard } from '../../../components/dashboard-components'
import CardBodyDropTarget from './card-drop-target'
import AddCargoModal from './add-cargo-modal'
import BussinessLogicOrderDetail, {
  FEEDERS,
  AIRLINE_TRAVELER,
  CAR_TRAVELER,
  TRAIN_TRAVELER,
  CARGO,
} from './bussiness-logic'
import type { Props, State } from './bussiness-logic'

export default class TriplogisticsOrderDetail extends BussinessLogicOrderDetail {
  constructor(props: Props) {
    super(props)
  }
  render() {
    const {
      order, feederOrigins, feederDests, alertMessage, alertType, trips,
      loadingFeeder, loadingOrder, loadingSubmit, modalType, feeder, travel, cargo
    } = this.state
    const {
      people_airplane_travel: airplanes,
      people_car_travel: cars,
      people_train_travel: trains,
      cargo_airplane_travel: cargos,
      people_airplane_travel_loading: loadingAirline,
      people_car_travel_loading: loadingCar,
      people_train_travel_loading: loadingTrain,
      cargo_airplane_travel_loading: loadingCargo,
    } = this.state

    return (
      <Container fluid>
        <Alert isOpen={!!alertMessage} color={alertType}
          toggle={() => this.setState({ alertMessage: '' })}>{alertMessage}</Alert>
        <Row>
          <Col>
            <TriplogisticsOrderCard order={order} loading={loadingOrder}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <CardTitle>Estafet Order</CardTitle>
            <Row>
              <Col sm="12" xs="12" md="4" lg="4" xl="4">
                <Card style={{ minHeight: '50%' }}>
                  <CardHeader className="p-3">
                    <CardTitle>Order</CardTitle>
                  </CardHeader>
                  <CardBodyDropTarget
                    className="p-3"
                    moveCard={this.moveCard}      
                    type="trips"
                  >
                    {trips.map((trip: CardObject, key: number) =>
                      <DragableCard
                        data={trip}
                        type={trip.type}
                        key={`ordertrip-${key}`} id={key + 1} index={key}
                        title={trip.title}
                        textOne={trip.textOne}
                        textTwo={trip.textTwo}
                        color={trip.color}
                        onPressDetail={() => {}}
                      />  
                    )}
                  </CardBodyDropTarget>
                  <CardFooter className="p-3 d-flex align-items-center justify-content-center">
                    <Button
                      color={trips.length < 1 || loadingSubmit ? "secondary" : "primary"}
                      style={{ width: '100%' }}
                      onClick={this.onSubmit}
                      disabled={trips.length < 1 || loadingSubmit}
                    >
                      Submit
                    </Button>
                  </CardFooter>
                </Card>
              </Col>
              <Col sm="12" xs="12" md="8" lg="8" xl="8">
                <Row>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#fff3cd" }}>
                        <CardTitle>Feeder Kota Asal</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingFeeder ? feederOrigins.map((user: Object, key: number) =>
                          <DragableCard
                            data={user}
                            type={FEEDERS}
                            key={`feeder1-${key}`} id={key + 1} index={key}
                            title={`${user.feeder.firstname} ${user.feeder.lastname}`}
                            textOne={user.feeder.phonenumber}
                            textTwo={user.feeder.online ? 'Online' : 'Offline'}
                            color="#fff3cd"
                            onPressDetail={() => {}}
                          />)
                          :
                          <List />
                        }
                      </CardBody>
                    </Card>
                  </Col>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#cce5ff" }}>
                        <CardTitle>Feeder Kota Tujuan</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingFeeder ? feederDests.map((user: Object, key: number) =>
                          <DragableCard
                            data={user}
                            type={FEEDERS}
                            key={`feeder2-${key}`} id={key + 1} index={key}
                            title={`${user.feeder.firstname} ${user.feeder.lastname}`}
                            textOne={user.feeder.phonenumber}
                            textTwo={user.feeder.online ? 'Online' : 'Offline'}
                            color="#cce5ff"
                            onPressDetail={() => {}}
                          />)
                          :
                          <List />
                        }
                      </CardBody>
                    </Card>
                  </Col>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#d4edda" }}>
                        <CardTitle>Traveler Pesawat</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingAirline ? airplanes.map((airline: TravelType, key: number) => airline.traveler &&
                          <DragableCard
                            data={airline}
                            type={AIRLINE_TRAVELER}
                            key={key} id={key + 1} index={key}
                            title={`${airline.traveler.first_name} ${airline.traveler.last_name} (${airline.available_baggage} Kg)`}
                            textOne={`${airline.origin} | ${moment(airline.departure_date).format('DD MMM HH:mm')}`}
                            textTwo={`${airline.destination} | ${moment(airline.arrival_date).format('DD MMM HH:mm')}`}
                            color="#d4edda"
                            onPressDetail={(data) => this.setState({ modalType: 'traveler', travel: data })}
                          />)
                          :
                          <List />
                        }
                      </CardBody>
                    </Card>
                  </Col>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#f5c6cb" }}>
                        <CardTitle>Traveler Kereta</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingTrain ? trains.map((train: TravelType, key: number) => train.traveler &&
                          <DragableCard
                            data={train}
                            type={TRAIN_TRAVELER}
                            key={key} id={key + 1} index={key}
                            title={`${train.traveler.first_name} ${train.traveler.last_name} (${train.available_baggage} Kg)`}
                            textOne={`${train.origin} | ${moment(train.departure_date).format('DD MMM HH:mm')}`}
                            textTwo={`${train.destination} | ${moment(train.arrival_date).format('DD MMM HH:mm')}`}
                            color="#f5c6cb"
                            onPressDetail={(data) => this.setState({ modalType: 'traveler', travel: data })}
                          />)
                          :
                            <List />
                          }
                      </CardBody>
                    </Card>
                  </Col>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#d6ac8b" }}>
                        <CardTitle>Traveler Mobil</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingCar ? cars.map((car: TravelCarType, key: number) => car.traveler &&
                          <DragableCard
                            data={car}
                            type={CAR_TRAVELER}
                            key={key} id={key + 1} index={key}
                            title={`${car.traveler.first_name} ${car.traveler.last_name} (${car.available_baggage} Kg)`}
                            textOne={`${car.origin} | ${moment(car.departure_date).format('DD MMM HH:mm')}`}
                            textTwo={`${car.destination} | ${moment(car.arrival_date).format('DD MMM HH:mm')}`}
                            color="#d6ac8b"
                            onPressDetail={(data) => this.setState({ modalType: 'traveler', travel: data })}
                          />)
                          :
                            <List />
                          }
                      </CardBody>
                    </Card>
                  </Col>
                  <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card>
                      <CardHeader className="p-3" style={{ backgroundColor: "#ff9bff" }}>
                        <CardTitle>Cargo</CardTitle>
                      </CardHeader>
                      <CardBody className="card-scrollable-y p-3">
                        {!loadingCargo ? cargos.map((cargo: Object, key: number) =>
                          <DragableCard
                            data={cargo}
                            type={CARGO}
                            key={key} id={key + 1} index={key}
                            title={cargo.third_party? String(cargo.third_party.name).toUpperCase(): 'third party missing'}
                            textOne={`${String(cargo.origin_airport).toUpperCase()} | ${moment(cargo.departure_date).format('DD MMM HH:mm')}`}
                            textTwo={`${String(cargo.destination_airport).toUpperCase()} | ${moment(cargo.arrival_date).format('DD MMM HH:mm')}`}
                            color="#ff9bff"
                            onPressDetail={() => {}}
                          />)
                          :
                            <List />
                          }
                      </CardBody>
                      <CardFooter className="p-3 d-flex align-items-center justify-content-center">
                        <Button
                          style={{ width: '100%', background: '#ff9bff', borderColor: 'transparent', color: '#555' }}
                          onClick={() => this.setState({ modalType: 'cargo_form' })}
                        >
                          Tambah Kargo
                        </Button>
                      </CardFooter>
                    </Card>
                  </Col>
                  {/* <Col sm="12" xs="12" md="6" lg="6" xl="6">
                    <Card className="p-3">
                      <CardTitle>Shuttle</CardTitle>
                      <CardBody className="card-scrollable-y p-0">

                      </CardBody>
                    </Card>
                  </Col> */}
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        {order &&
          <AddCargoModal
            toggle={() => this.setState({ modalType: '' })}
            onGetCargo={() => this.getCargoList(order)}
            isOpen={modalType === 'cargo_form'}
            order={order}
            />
        }
        {travel &&
            <ModalDetailTravel
              isOpen={modalType === 'traveler'}
              onClose={() => this.setState({ modalType: '' })}
              traveler={travel}
            />
        }
      </Container>
    )
  }
}
