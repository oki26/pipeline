// @flow
import type { OrderType } from '../../../types'
import React from "react";
import ReactTable from "react-table";
import qrcode from 'qrcode'
import { CSVLink } from "react-csv";
import { connect } from 'react-redux'
import checkboxHOC from "react-table/lib/hoc/selectTable";
import {
	Card, CardBody, CardTitle,
	Button, Col, InputGroup, InputGroupAddon,
	Row, CardSubtitle, Input, ListGroupItem,
	ListGroup
} from 'reactstrap';
import moment from "moment"
import accounting from "accounting"
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Picker from 'rc-calendar/lib/Picker'
import RangeCalendar from 'rc-calendar/lib/RangeCalendar'
import enUS from 'rc-calendar/lib/locale/en_US'
import "react-table/react-table.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ManifestProviewModal, BarcodeCard, CreateManifestModal, ModalDetailOrder, ModalChangePayment, ModalChangeOrder } from '../../../components/dashboard-components';
import Backendless from '../../../services/backendless'
import { history } from '../../../index'
// import OrderDetailLogic from "../triplogistics-order-detail/bussiness-logic";

const CheckboxTable = checkboxHOC(ReactTable);
type Props = {
	account: Object,
}

type RowDataType = {
	objectId: string,
	sla: string,
	tracking_id: string,
	order_status: string,
	package: string | null,
	weight: string | null,
	payment_status: string | null,
	amount: number | null,
	pickup_time: string | null,
	status_code: string | null,
	from: string | null,
	to: string | null,
	receiver: string | null,
	sender: string | null,
	order: OrderType,
}

type CsvDataType = {
	number: string | null | number,
	user: string | null,
	email: string | null,
	phone_user: string | null,
	sender: string | null,
	phone_sender: string | null,
	address_sender: string | null,
	from: string | null,
	to: string | null,
	receiver: string | null,
	phone_receiver: string | null,
	address_sender: string | null,
	sla: string | null,
	weight: string | null,
	package: string | null,
	amount: number | null,
	pickup_time: string | null,
	invoice_status: string | null,
}

type State = {
	bags: Array<Object>,
	isOpenModal: boolean,
	modalType: ?boolean,
	modalPayment: boolean,
	modalOrder: boolean,
	modalDetail: boolean,
	loading: boolean,
	obj: Object,
	orders: Array<RowDataType>,
	detailData: RowDataType | Object | null,
	loadingOrders: boolean,
	loadingDetail: boolean,
	dateRange: Array<string>,
	searchText: string,
	selection: Array<string>,
	selectionOrder: Array<OrderType>,
	selectAll: boolean,
	pageSize: number,
	pages: number,
	totalData: number,
	csvData: Array<CsvDataType>,
  page: number,
  revenue: number,
	orderOfBag: Array<OrderType>,
}

const csvHeader = [
	{ label: "Nomor", key: "number" },
	{ label: "User", key: "user" },
	{ label: "Email", key: "email" },
  { label: "Nomor Telepon User", key: "phone_user" },
  { label: "Nama Pengirim", key: "sender" },
  { label: "Nomor Telepon Pengirim", key: "phone_sender" },
  { label: "Alamat Pengirim", key: "address_sender" },
  { label: "Kota Asal", key: "from" },
  { label: "Kota Tujuan", key: "to" },
  { label: "Nama Penerima", key: "receiver" },
  { label: "Nomor Telepon Penerima", key: "phone_receiver" },
  { label: "Alamat Penerima", key: "address_receiver" },
  { label: "SLA / Service", key: "sla" },
  { label: "Berat Barang", key: "weight" },
  { label: "Isi Paket", key: "package" },
  { label: "Biaya Kirim", key: "amount" },
  { label: "Harga sla", key: "price_per_sla" },
  { label: "Waktu Pickup", key: "pickup_time" },
  { label: "Harga per SLA", key: "price_per_sla" },
  { label: "Invoice status", key: "invoice_status" },
]

const calendar = (
	<RangeCalendar
		showWeekNumber={false}
		dateInputPlaceholder={['Dari Tanggal', 'Sampai Tanggal']}
		defaultValue={[moment().subtract(1, 'month').format(), moment()]}
		locale={enUS}
	/>
);

class Orders extends React.Component<Props, State> {
	tableRef = null
	constructor(props: Props) {
		super(props);
		this.state = {
			loading: false,
			bags: [],
			modalType: false,
			modalPayment: false,
			modalOrder: false,
			modalDetail: false,
			obj: {},
			orders: [],	
			detailData: {},
			loadingOrders: false,
			loadingDetail: false,
			dateRange: [],
			searchText: '',
			selectAll: false,
			selection: [],
			selectionOrder: [],
			pageSize: 20,
			pages: -1,
			totalData: 0,
			csvData: [],
    	page: 0,
    	revenue: 0,
			isOpenModal: false,
			orderOfBag: []
		};
	}

	notify = () => toast.success("Copied To Clipboard!");

	orderStatus = (value: string): string => {
		switch (value) {
			case 'ORD':
				return "Pending"
			case 'TIORD':
				return "Picked up by feeder"
			case 'WO':
				return ""
			case 'TIWO':
				return "On The Way To Traveler"
			case 'CRG':
				return ""
			case 'TICRG':
				return "Picked up by traveler"
			case 'WD':
				return "Traveller arrived"
			case 'TIWD':
				return "Deliver by Feeder"
			case 'DO':
				return "Completed"
			default:
				return ""
		}
	}

	toggleViewBagOrders = async (objID: string, index: number) => {
		try {
			const bags = [...this.state.bags]
			if (bags[index].orders.length > 0) {
				bags[index].orders = []
				this.setState({ bags })
				return
			}
			this.setState({ loading: true })
			const q = Backendless.DataQueryBuilder.create()
			q.setWhereClause(`bagging.objectId LIKE '${objID}'`)
			const orderOfBag = await Backendless.Data.of('triplogistic_orders').find(q)
			orderOfBag.map(ord => ord.objectId)
			bags[index].orders = orderOfBag
			this.setState({ bags, loading: false })
		} catch (er) {
			this.setState({ loading: false })
			console.log(er)
		}
	}

	createManifest = async (thidParty: ?Object, arrived_time: string, departured_time: string) => {
		try {
			const { bags } = this.state
			const bag = bags[0]

			/* create manifest */
			const manifest = await Backendless.Data.of('triplogistic_manifest').save({
				org_code: bag.org_code,
				dest_code: bag.dest_code,
				arrived_time,
				departured_time
			})

			/* adding relation 1:1 from bag to manifest */
			const promises = []
			await Backendless.Data.of('triplogistic_manifest').addRelation(manifest, 'bagging', bags)
			bags.forEach(bag =>
				promises.push(Backendless.Data.of('triplogistic_bagging').setRelation(bag, 'manifest', [manifest]))
			)

			/* adding relation 1:1 from manifest to cargo */
			if (thidParty) {
				await Backendless.Data.of('triplogistic_manifest').addRelation(manifest, 'cargo', [thidParty])
			}
			this.setState({ bags: [], modalType: false })
			alert('success')
		} catch (er) {
			console.log(er)
		}
	}

	/* retrieve data orders without bag and list bags without manifest */
	getOrdersAndBags = async (
		pageSize: number = 20, offset: number = 0, page: number = 0,
		sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		try {
			this.setState({ loadingOrders: true })
			const { dateRange, searchText } = this.state
			
			// set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(2).setSortBy(["created DESC", ...sortQuery])
				.setWhereClause(`bagging is null`)

			// filter by date
			if (dateRange.length === 2) {
					// dateRange.sort((a, b) => moment(a).isAfter(b) ? -1 : 1)
					queryBuilder.setWhereClause(`pickup_time > '${moment(dateRange[0]).subtract(1,'day').format('MM/DD/YYYY 17:00')}' and pickup_time < '${moment(dateRange[1]).format('MM/DD/YYYY 17:00')}'`)
			}
			
			// query search
			if (searchText !== '') {
				queryBuilder.setWhereClause(`
					(tracking_id LIKE '%${searchText}%' or
					from.city_name LIKE '%${searchText}%' or
					to.city_name LIKE '%${searchText}%' or
					payment.payment_status LIKE '%${searchText}%' or
					from.contact_name LIKE '%${searchText}%' or
					to.contact_name LIKE '%${searchText}%' or
					owner.first_name LIKE '%${searchText}%') and 
					(bagging is null)`)
			}
			const res: Array<OrderType> = await Backendless.Data.of('triplogistic_orders').find(queryBuilder)
			// const revenue: Array<Object> = await Backendless.Data.of('triplogistic_orders').find(queryBuilder.setWhereClause(`payment.payment_status = 'paid'`).setProperties( `Sum(sla.total_price) as revenue`))
			const count: number = await Backendless.Data.of('triplogistic_orders').getObjectCount(queryBuilder)
			const transformed: Array<RowDataType> =  res.map((data: OrderType, index): RowDataType => {
				return ({
					_id: data.objectId,
					objectId: data.objectId,
					tracking_id: data.tracking_id,
					order_status: this.orderStatus(data.status_code),
					sla: data.sla ? `${data.sla.name_sla} ${data.sla.description_sla === "3 Hours Picked Up by Car" ? "by Car" : data.sla.description_sla === "3 Hours Picked Up by Motorcycle" ? "by Motor" : "" }` : 'Null',
					package: data.package ? data.package.name_packages : null,
					weight: data.package ? `${data.package.weight_packages} Kg` : null,
					payment_status: data.payment ? data.payment.payment_status: "unpaid",
					price_per_sla: data.sla ? accounting.formatMoney(data.sla.total_price, 'Rp. ', 0, '.') : null,
					amount: data.payment ? accounting.formatMoney(data.payment.amount, 'Rp. ', 0, '.') : null,
					pickup_time: data.pickup_time ? moment(data.pickup_time).format("HH:mm, DD-MM-YYYY"): null,
					status_code: data.status_code ? data.status_code: null,
					from: data.from ? `${data.from.city_name.toLowerCase().includes("kabupaten") ? "" : "Kota" } ${data.from.city_name}` : null,
					to: data.to ?  `${data.to.city_name.toLowerCase().includes("kabupaten") ? "" : "Kota" } ${data.to.city_name}` : null,
					receiver: data.to ? data.to.contact_name : null,
					sender: data.owner ? `${data.owner.first_name} ${data.owner.last_name}` : null,
					payment_proof: data.payment ? data.payment.payment_proof : null,
					order: data
				})
			})
			const transformToCSV: Array<CsvDataType> =  res.filter(data => {
				const status_payment = data.payment ? data.payment.payment_status : "unpaid"
				return status_payment === "paid"
				}).map((data: OrderType, index): CsvDataType => {
				return ({
					number: index+1,
					user: data.owner ? `${data.owner.first_name} ${data.owner.last_name}` : null,
					email: data.from ? data.from.email : null,
					phone_user: data.owner ? data.owner.phone_number : null,
					sender: data.from ? data.from.contact_name : null,
					phone_sender: data.from ? data.from.phone_number : null,
					address_sender: data.from ? data.from.address : null,
					from: data.from ? `${data.from.city_name.toLowerCase().includes("kabupaten") ? "" : "Kota" } ${data.from.city_name}` : null,
					to: data.to ?  `${data.to.city_name.toLowerCase().includes("kabupaten") ? "" : "Kota" } ${data.to.city_name}` : null,
					receiver: data.to ? data.to.contact_name : null,
					phone_receiver: data.to ? data.to.phone_number : null,
					address_receiver: data.to ? data.to.address : null,
					sla: data.sla ? data.sla.name_sla : 'Null',
					weight: data.package ? `${data.package.weight_packages} Kg` : null,
					package: data.package ? data.package.name_packages : null,
					amount: data.payment ? accounting.formatMoney(data.payment.amount, 'Rp. ', 0, '.') : null,
					pickup_time: data.pickup_time ? moment(data.pickup_time).format("HH:mm, DD-MM-YYYY"): null,
					invoice_status: data.payment ? data.payment.payment_status: "unpaid",
					price_per_sla: data.sla ? accounting.formatMoney(data.sla.total_price, 'Rp. ', 0, '.') : null,
				})
			})
			const queryBags = Backendless.DataQueryBuilder.create().setWhereClause('manifest is null')
			const bags = await Backendless.Data.of('triplogistic_bagging').find(queryBags)
			this.setState({
				orders: transformed, loadingOrders: false,
				csvData: transformToCSV,
				bags: bags.map(bag => ({...bag, orders: []})),
				totalData: count,
				pages: count / offset, pageSize: pageSize, page: page,
				// revenue: revenue !== [] ? revenue[0].revenue : 0
			})
		} catch(er) {
			this.setState({ loadingOrders: false })
			console.log(er)
		}
	}

	/* Adding relation order to bag */
	addOrderToBag = async () => {
		try {
			this.setState({ loadingOrders: true })
			const { selection, selectionOrder} = this.state
			const order: OrderType = selectionOrder[0]
			// const query = Backendless.DataQueryBuilder.create()

			// create bag
			const data = {
				dest_code: order.destination_code,
				org_code: order.origin_code,
			}
			const bag = await Backendless.Data.of('triplogistic_bagging').save(data)

			// add relation 1:1 from order to bag
			const promises = []
			selection.forEach(objIdOrder => {
				promises.push(
					Backendless.Data.of('triplogistic_orders').setRelation(objIdOrder, 'bagging', [{ objectId: bag.objectId }])
				)
			})
			await Promise.all(promises)
			alert('Success')
			this.setState({ isOpenModal: false })
			this.getOrdersAndBags()
		} catch (er) {
			console.log(er)
			this.setState({ loadingOrders: false })
		}
	}

	/* Delete bag */
	deleteBag = async (bagId: string) => {
		try {
			this.setState({ loading: true })
			await Backendless.Data.of('triplogistic_orders').bulkUpdate(`bagging.objectId LIKE '${bagId}'`, { bagging: null })
			await Backendless.Data.of(`triplogistic_bagging`).remove({ objectId: bagId })
			const bags = this.state.bags.filter(bag => bagId !== bag.objectId)
			this.setState({ bags, loading: false }) 
		} catch (er) {
			this.setState({ loading: false })
			console.log(er)
		}
	}


	/** navigate to order detail page */
	goDetailPage = (event: Event, row: { original: RowDataType }) => {
		event.preventDefault();
		history.push(`/order/${row.original.objectId || 'null'}`)
	}

	timeout = null
	onChage = (e: Object) => {
		this.setState({ [e.target.name]: e.target.value }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getOrdersAndBags, 1000)
		})
	}


  toggleSelection = async (key: string, shift: any, row: Object) => {
		let selection = [...this.state.selection];
		let selectionOrder = [...this.state.selectionOrder]
    const keyIndex = selection.indexOf(key);
    if (keyIndex >= 0) {
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ];
      selectionOrder = [
        ...selectionOrder.slice(0, keyIndex),
        ...selectionOrder.slice(keyIndex + 1)
      ];
    } else {
			selection.push(key);
			try {
				const barcode_uri = await qrcode.toDataURL(row.order.tracking_id)
				selectionOrder.push({ ...row.order, barcode_uri })
			} catch (er) {}
    }
    this.setState({ selection, selectionOrder });
  };

	/** toggle selection row at table */
  toggleAll = async () => {
    const selectAll = this.state.selectAll ? false : true;
    const selection = [];
		const selectionOrder = [];
		const promises = []
    if (selectAll && this.tableRef !== null) {
      const wrappedInstance = this.tableRef.getWrappedInstance();
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      currentRecords.forEach((item) => {
				selection.push(item._original._id);
				selectionOrder.push(item._original.order);
				if (item._original.order.tracking_id) {
					promises.push(qrcode.toDataURL(item._original.order.tracking_id))
				} else {
					promises.push(Promise.resolve(''))
				}
			});
			const results = await Promise.all(promises)
			results.forEach((barcode_uri, idx) => {
				selectionOrder[idx] = { ...selectionOrder[idx], barcode_uri }
			})
		}
    this.setState({ selectAll, selection, selectionOrder });
  };

	isSelected = (key: string) => this.state.selection.includes(key)

	renderPagination = ({ canNext, canPrevious, page, pageSize, sorted }: Object) => {
		const pages = [10, 20, 30, 40, 50, 100]
		return (
			<div>
			<Row className="pl-3 pr-3">
				<Col className="d-flex flex-row pt-2 pb-2 pr-3 pl-3">	
					<Button
						disabled={!canPrevious} color={canPrevious ?  'success' : 'secondary'} outline
						onClick={() => this.getOrdersAndBags(pageSize, (page - 1) * pageSize, (page - 1), sorted)}
					>
						Previous
					</Button>
					<Input
						type="select" name="pageSize" value={pageSize} className="mr-3 ml-3"
						onChange={(e) => this.getOrdersAndBags(e.target.value, page * e.target.value, page, sorted)}
					>
						{pages.map(num => <option key={`page-${num}`} value={num}>{num} Baris</option>)}
					</Input>
					<InputGroup className="mr-3" >
						<InputGroupAddon addonType="prepend" >Page</InputGroupAddon>
						<Input
							type="number" name="page" value={page}
							onChange={(e) => this.getOrdersAndBags(pageSize, e.target.value * pageSize, e.target.value, sorted)}
						/>
					</InputGroup>
					<Button
						disabled={!canNext} color={canNext ?  'success' : 'secondary'}
						onClick={() => this.getOrdersAndBags(pageSize, (page + 1) * pageSize, (page + 1), sorted)}
					>
						Next
					</Button>
				</Col>
				<Col className="d-flex align-items-end justify-content-end pt-2 pb-2 pr-3 pl-3">
					<Button
						disabled={this.state.selection.length < 1}
						color={this.state.selection.length >= 1 ?  'success' : 'secondary'}
						onClick={() => this.setState({ isOpenModal: true })}
					>
						Buat bag
					</Button>
				</Col>
			</Row>
			<Row>
				<Col>
					<p style={{marginLeft: '10px', fontWeight: 'bold'}} >total order {this.state.totalData}</p>
				</Col>
			</Row>
			</div>
		)
	}

	tableHeaders = [
		{ Header: "SLA", accessor: "sla", sortable: false, filterable: false },
		{ Header: "Tracking id", accessor: "tracking_id", sortable: false, filterable: false },
		{ Header: "Status pengiriman", accessor: "status_code", Cell: ({ original }: { original: RowDataType }) => {
			switch (original.order_status) {
				case "Pending":
					return (
						<center>
							<Button size="md" color="danger" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "Picked up by feeder":
					return (
						<center>
							<Button size="md" color="info" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "On The Way To Traveler":
					return (
						<center>
							<Button size="md" color="info" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "Picked up by Traveler":
					return (
						<center>
							<Button size="md" color="info" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "Traveller arrived":
					return (
						<center>
							<Button size="md" color="info" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "Deliver by Feeder":
					return (
						<center>
							<Button size="md" color="warning" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				case "Completed":
					return (
						<center>
							<Button size="md" color="success" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
				default:
					return (
						<center>
							<Button size="md" color="primary" onClick={() => this.setState({modalOrder: true, detailData: original})}>
								{original.order_status}
							</Button>
						</center>
					)
			} 
		}
		},
		{ Header: "Asal", accessor: "from", sortable: false, filterable: false },
		{ Header: "Tujuan", accessor: "to", sortable: false, filterable: false },
		{ Header: "Jam Dikirim", accessor: "pickup_time" },
		{ Header: "Pengirim", accessor: "sender", sortable: false, filterable: false },
		{ Header: "Harga per sla", accessor: "price_per_sla", sortable: false, filterable: false },
		{ Header: "Jumlah tagihan", accessor: "amount", sortable: false, filterable: false },
		{ Header: "Status Pembayaran", accessor: "payment.payment_status", Cell: ({ original }: { original: RowDataType }) => {
			switch (original.payment_status) {
				case "unpaid":
					return (
						<center>
							<Button size="md" color="danger" onClick={() => this.setState({modalPayment: true, detailData: original})}>
								{original.payment_status}
							</Button>
						</center>
					)
				case "prepaid":
					return (
						<center>
							<Button size="md" color="info" onClick={() => this.setState({modalPayment: true, detailData: original})}>
								{original.payment_status}
							</Button>
						</center>
					)
				case "paid":
					return (
						<center>
							<Button size="md" color="success" onClick={() => this.setState({modalPayment: true, detailData: original})}>
								{original.payment_status}
							</Button>
						</center>
					)
				default:
					return (
						<center>
							<Button size="md" color="link" onClick={() => this.setState({modalPayment: true, detailData: original})}>
								{original.payment_status}
							</Button>
						</center>
					)
			}
		}
		},
	]
	
	render() {
		const {
			dateRange, searchText, orders, loadingOrders,
			selectAll, pageSize, isOpenModal, selectionOrder,
			bags = [], loading, modalType, 
			modalPayment, modalDetail, modalOrder,
			detailData, page, pages, revenue
		} = this.state
		const { isSelected, toggleSelection, toggleAll } = this
		const tableProps = {
			selectAll,
      isSelected,
      toggleSelection,
			toggleAll,
			selectType: "checkbox"
		}

		return (
			<div>
				{bags.length > 0 &&
					<Card>
						<CardBody>
						<CardTitle>Bags Belum Maninfest</CardTitle>
							<ListGroup>
								{bags.map((bag, idx) =>
									<ListGroupItem key={bag.objectId}>
										<Row>
											<Col>
												Bag {idx + 1} - #{bag.tracking_id}
											</Col>
											<div>
												<Button
													className="mr-2"	
													color={loading ? "secondary" : "success"} disabled={loading}
													onClick={() => this.toggleViewBagOrders(bag.objectId, idx)}
												>
													<i className="mr-1 fa fa-eye" />
													{loading ?
														<span>Loading...</span>
														:
														<span>{bag.orders.length <= 0 ? "Lihat Detail" : "Tutup Detail"}</span>
													}
												</Button>
												<Button
													color={loading ? "secondary" : "danger"} disabled={loading}
													onClick={() => this.deleteBag(bag.objectId)}
												>
													<i className="mr-1 fa fa-trash-alt" />
													{loading ?
														<span>Loading...</span>
														:
														<span>Hapus Bag</span>
													}
												</Button>
											</div>
										</Row>
											{bag.orders && 
												<ListGroup className="mt-2">
													{bag.orders.map((orderChild: OrderType, idx) => (
														<ListGroupItem key={`ord-${orderChild.objectId}`}>
																<Row>
																	<Col></Col>
																</Row>
														</ListGroupItem>
												))}
												</ListGroup>
											}
									</ListGroupItem>
								)}
								<Button className="mt-2" onClick={() => this.setState({ modalType: true })} color="success">
									Buat Manifest
								</Button>
							</ListGroup>
						</CardBody>
					</Card>
				}
				<Card>
					<CardBody>
						<Row>
							<Col sm="12" xs="12" md="4" lg="4" xl="4">
							<CardTitle>
								<i className="mdi mdi-database mr-2"></i> Orders Logistics
							</CardTitle>
								<CardSubtitle>List Order Pengiriman Barang</CardSubtitle>
								{/* <p style={{fontSize: 20}} >Revenue {accounting.formatMoney(revenue, "Rp. ", 0, '.')}</p> */}
							</Col>
							<Col sm="12" xs="12" md="8" lg="8" xl="8">
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="6" lg="6" xl="6">
									<InputGroup style={{ maxHeight: 36 }}>
										<Picker
											value={dateRange}
											onChange={(val) => this.setState({ dateRange: val }, () => this.getOrdersAndBags())}
											animation="slide-up"
											calendar={calendar}
										>
											{
												({ value }) => {
													return (
															<Input
																placeholder="Filter Tanggal"
																value={(value[0] && value[1]) ? `${moment(value[0]).format('DD MMM YYYY')}  to  ${moment(value[1]).format('DD MMM YYYY')}` : ''}
															/>);
												}
											}
										</Picker>
										<Button disabled={dateRange.length < 1} onClick={() => this.setState({ dateRange: [] }, () => this.getOrdersAndBags())}>
											X
										</Button>
									</InputGroup>
									</Col>
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<InputGroup style={{ maxHeight: 36 }}>
											<Input
												type="text" placeholder="Cari"
												name="searchText" value={searchText}
												onChange={this.onChage}
											/>
											<InputGroupAddon addonType="append">
												<Button color="primary" 
												// onClick={() => onChangeSearch(searchText)}
												>
													<i className="ti-search"></i>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</Col>
								</Row>
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<CSVLink data={this.state.csvData} headers={csvHeader} filename={`invoice_tanggal_${moment().format("DD-MM-YYYY")}.csv`} >
											<Button color="success" style={{float: 'right'}} >
												Export to excel
											</Button>
										</CSVLink>
									</Col>
								</Row>
							</Col>
						</Row>
					</CardBody>
					<CardBody>
						<CheckboxTable
							ref={r => (this.tableRef = r)}
							manual
							pages={pages}
              page={page}
							columns={this.tableHeaders
								.concat([{
								Header: "Action",
								accessor: "actions",
								sortable: false,
								filterable: false,
								
								Cell: (detailData) => (
									<center>
										<Button size="md" color="primary" onClick={e => this.setState({modalDetail: true, detailData: detailData.original})}>
											Detail
										</Button>
									</center>)
								},
								{
								Header: "Copy",
								accessor: "copy",
								sortable: false,
								filterable: false,
								
								Cell: (detailData) => (
									<center>
										<CopyToClipboard text={`
PENGIRIMAN ONLINE
${detailData.original.sla}
${detailData.original.package}
${detailData.original.pickup_time}

Pengirim:
${detailData.original.order.from ? detailData.original.order.from.contact_name : detailData.original.sender }
${detailData.original.order.from ? detailData.original.order.from.phone_number : "nomor telfon tidak ada" }
alamat: ${detailData.original.order.from ? detailData.original.order.from.address : "tidak ada alamat"}

Penerima:
${detailData.original.receiver}
${detailData.original.order.to ? detailData.original.order.to.phone_number :  "nomor telfon tidak ada" }
alamat: ${detailData.original.order.to ? detailData.original.order.to.address : "tidak ada alamat"}

notes: ${detailData.original.order.notes}
										`} onCopy={this.notify}>
											<Button size="md" color="success">
												Copy
											</Button>
										</CopyToClipboard>
									</center>)
								}
							])
							}
							defaultPageSize={pageSize}
              pageSize={pageSize}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={orders}
							loading={loadingOrders}
							filterable={false}
							onFetchData={(tableState: Object) =>{
								this.getOrdersAndBags(tableState.pageSize, page * tableState.pageSize, page, tableState.sorted)
							}}
							PaginationComponent={this.renderPagination}
							{...tableProps}
						/>
					</CardBody>
				</Card>
				<ManifestProviewModal
					isOpen={isOpenModal}
					toggle={() => this.setState({ isOpenModal: false })}
					onSubmit={() => this.addOrderToBag()}
					orders={selectionOrder}
				/>
				{selectionOrder.length > 0 &&
					<BarcodeCard
						title="Barcode AWB"
						datas={selectionOrder}
					/>
				}
				<CreateManifestModal
					toggle={() => this.setState({ modalType: false })}
					isOpen={modalType}
					onSubmit={(thirdParty, arrived_time, departured_time) => this.createManifest(thirdParty, arrived_time, departured_time)}
				/>
				<ModalDetailOrder
					toggle={() => this.setState({ modalDetail: false, detailData: {} })}
					isOpen={modalDetail}
					data={detailData}
					onReloadData={() => this.getOrdersAndBags()}
				/>
				<ModalChangePayment
					toggle={() => this.setState({ modalPayment: false, detailData: {} })}
					isOpen={modalPayment}
					data={detailData}
					onReloadData={() => this.getOrdersAndBags()}
				/>
				<ModalChangeOrder
					toggle={() => this.setState({ modalOrder: false, detailData: {} })}
					isOpen={modalOrder}
					data={detailData}
					onReloadData={() => this.getOrdersAndBags()}
				/>
				<ToastContainer autoClose={2000} />
			</div>
		);
	}
}

const mapState = (state: Object) => ({
	account: state.auth.account
})
export default connect(mapState, null)(Orders);
