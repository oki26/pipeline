import React from 'react';
import {
  Row,
  Col,
  Button,
  FormGroup,
  Card,
  CardBody,
  CardTitle
} from 'reactstrap';
import { connect } from 'react-redux'
import * as actions from '../../redux/actions/authentication'
import Input from 'react-validation/build/input';
import Form from 'react-validation/build/form';
import { isEmail } from 'validator';

const required = (value, props) => {
  if (!value || (props.isCheckable && !props.checked)) {
    return <span className="text-danger is-visible">Please Fill the above field</span>;
  }
};

const email = (value) => {
    if (!isEmail(value)) {
        return <span className="text-danger is-visible">{value} is not a valid email.</span>;
    }
};

const isEqual = (value, props, components) => {
  const bothUsed =
    components.password[0].isUsed && components.confirm[0].isUsed;
  const bothChanged =
    components.password[0].isChanged && components.confirm[0].isChanged;

  if (
    bothChanged &&
    bothUsed &&
    components.password[0].value !== components.confirm[0].value
  ) {
    return (
      <span className="text-danger is-visible">Passwords are not equal.</span>
    );
  }
};

class FormValidate extends React.Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.state = {
      email: '',
      firstname: '',
      lastname: '',
      phonenumber: '',
      password: '',
      confirm: ''
    };
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleClick() {
    const {firstname, lastname, phonenumber, email, password} = this.state
    this.form.validateAll();
    this.props.register(firstname, lastname, phonenumber, email, password)
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log(event);
  }
  render() {
    const {email: emailState, firstname, lastname, phonenumber, password, confirm} = this.state
    return (
      <Row>
        <Col sm="12">
          <Card>
            <CardTitle className="p-3 border-bottom mb-0">
              <i className="mdi mdi-alert-box mr-2" />
              Register User
            </CardTitle>
            <CardBody>
              <Form
                ref={c => {
                  this.form = c;
                }}
                onSubmit={this.handleSubmit}
              >
                <FormGroup>
                  <label className="control-label" htmlFor="fname">
                    First name *
                  </label>
                  <div className="mb-2">
                    <Input
                      placeholder="Firstname"
                      type="text"
                      name="firstname"
                      validations={[required]}
                      onChange={this.onChange}
                      className={
                        this.state.firstname === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <label className="control-label" htmlFor="lname">
                    Last name *
                  </label>
                  <div className="mb-2">
                    <Input
                      placeholder="Lastname"
                      type="text"
                      name="lastname"
                      validations={[required]}
                      onChange={this.onChange}
                      className={
                        this.state.lastname === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <label className="control-label" htmlFor="uname">
                    phone number *
                  </label>
                  <div className="mb-2">
                    <Input
                      placeholder="phone number"
                      type="text"
                      name="phonenumber"
                      validations={[required]}
                      onChange={this.onChange}
                      className={
                        this.state.phonenumber === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <label className="control-label" htmlFor="uemail">
                    Email *
                  </label>

                  <div className="mb-2">
                    <Input
                      placeholder="Email"
                      type="email"
                      name="email"
                      validations={[required, email]}
                      onChange={this.onChange}
                      className={
                        this.state.email === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <label className="control-label" htmlFor="upassword">
                    Password *
                  </label>
                  <div className="mb-2">
                    <Input
                      placeholder="Password"
                      type="password"
                      name="password"
                      validations={[required, isEqual]}
                      onChange={this.onChange}
                      className={
                        this.state.password === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <label className="control-label" htmlFor="uconfirm_password">
                    Confirm password *
                  </label>
                  <div className="mb-2">
                    <Input
                      placeholder="Confirm password"
                      type="password"
                      name="confirm"
                      validations={[required, isEqual]}
                      onChange={this.onChange}
                      className={
                        this.state.confirm === ''
                          ? 'form-control'
                          : 'form-control'
                      }
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <Button 
                    color="primary" 
                    className="button" 
                    onClick={this.handleClick} 
                    disabled={
                      emailState === '' ||
                      firstname === '' ||
                      lastname === '' ||
                      phonenumber === '' ||
                      password === '' ||
                      confirm === ''
                    } >
                    Sign up
                  </Button>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = (store) => ({
  success: store.auth.success,
  message: store.auth.message,
  loading: store.auth.loading,
})

const mapDispatchToProps = {
  register: actions.register,
  setMessage: actions.setMessage,
  setLoading: actions.setLoading,
}

export default connect(mapStateToProps, mapDispatchToProps)(FormValidate)