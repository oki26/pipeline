// @flow
import Backendless from 'backendless'
import * as constants from '../constants/auth'

export function setLoading(loading: boolean) {
  return {
    type: constants.AUTH_SET_LOADING,
    payload: { loading }
  }
}

export function setMessage(message: string) {
  return {
    type: constants.AUTH_SET_MESSAGE,
    payload: { message }
  }
}

export function setSuccess(success: boolean, message: string) {
  return {
    type: constants.AUTH_SET_SUCCESS,
    payload: { success, message }
  }
}

export function setAccount(account: Object, password: string) {
  return {
    type: constants.AUTH_SET_ACCOUNT,
    payload: { account, password }
  }
}

export function logout() {
  return {
    type: constants.AUTH_SET_ACCOUNT,
    payload: { account: {}, password: "" }
  }
}

export function login(email: string, password: string) {
  return async (dispatch: Function) => {
    dispatch(setLoading(true))
    try{
      const loggedInUser = await Backendless.UserService.login( email, password, true );
			if (loggedInUser.staff === false) {
        throw {message: "You Are Not Admin"}
      }
      await dispatch(setAccount(loggedInUser, password))
      dispatch(setSuccess(true, `your email is ${loggedInUser.email}`))
    }catch(error){
      console.log(error.message)
      dispatch(setSuccess(false, error.message))
      alert(error.message)
    }
  }
}

export function register(first_name: string, last_name: string, phone_number: string, email: string, password: string) {
  return async (dispatch: Function) => {
    dispatch(setLoading(true))
    const user = new Backendless.User();
    user.first_name = first_name
    user.last_name = last_name
    user.phone_number = phone_number
    user.email = email
    user.password = password
    user.staff = true
    try{
      await Backendless.UserService.register( user );
      alert("user has been registered, check email for confirmation")
      window.location.replace("/register")
      dispatch(setSuccess(true, `user has been registered`))
    }catch(error){
      dispatch(setSuccess(false, error.message))
      alert(error.message)
      console.log(error.statusCode)
    }
  }
}