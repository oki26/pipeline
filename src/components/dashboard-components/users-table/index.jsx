// @flow
import React from "react";
import ReactTable from 'react-table'
import moment from 'moment'
import accounting from 'accounting'
import { 
  Card, CardBody, CardTitle,
  CardSubtitle, Button, Input, InputGroup,
  InputGroupAddon, Row, Col
} from 'reactstrap';
import type { UserType } from '../../../types'
import img1 from '../../../assets/images/users/1.jpg';
import BackendLess from '../../../services/backendless'
import { UserDetailModal } from '../index'

export type Props = {
}
export type State = {
  pages: number,
  pageSize: number,
  page: number,
  openModal: boolean,
  selectedUser: ?UserType,
  users: Array<UserType>,
  loading: boolean,
  selectedFilter: '',
  searchText: string,
}
const tableHeaders = [
	{
		Header: 'Nama User',
		accessor: 'username',
		Cell: ({ original }: { original: UserType }) => (
			<div className="d-flex no-block align-items-center">
				<div className="mr-2">
					<img src={original.regular ? original.regular.avatar_show ? original.regular.avatar_show : img1 : img1} alt="user" className="rounded-circle" width="45" />
				</div>
				<div className="">
					<h5 className="mb-0 font-16 font-medium">{original.regular ? original.regular.first_name : null} {original.regular ? original.regular.last_name : null}</h5>
          <span>{original.email}</span>
				</div>
			</div>
		)
	},
	{ 
		Header: 'Point',
		accessor: 'city',
		Cell: ({ original }: { original: UserType }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{original.regular ? original.regular.points ? original.regular.points : 0 : 0}
			</div>
		)
	},
	{ 
		Header: 'Gender',
		accessor: 'gender',
		Cell: ({ original }: { original: UserType }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{original.regular ? original.regular.gender ? original.regular.gender : 'tidak Ada' : 'Tidak Ada'}
			</div>
		)
	},
	{ 
		Header: 'Saldo',
		accessor: 'balance',
		Cell: ({ original }: { original: UserType }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{accounting.formatMoney(original.regular ? original.regular.balance : 0, 'Rp.', 0, '.')}
			</div>
		)
},
	{ 
		Header: 'Join Date',
		accessor: 'created',
		Cell: ({ original }: { original: UserType }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{original.regular ? moment(original.regular.created, 'x').format('DD-MM-YYYY') : 'Tidak Ada'}
			</div>
		)
	},
]

const selectOptions = [{
  title: 'Aktif',
  value: 'regular.is_active = true',
},{
  title: 'Non-aktif',
  value: 'regular.is_active = false',
},{
  title: 'Diblokir',
  value: 'regular.is_blocked = true',
},{
  title: 'Verified',
  value: 'regular.is_verification = true',
},{
  title: 'Unverified',
  value: 'regular.is_verification = false',
},]
class UsersTable extends React.Component<Props, State> {
  timeout = null
  state = {
    pages: -1,
    pageSize: 20,
    page: 0,
    users: [],
    loading: true,
    selectedFilter: '',
    selectedUser: null,
    openModal: false,
    searchText: ''
  }
  

  componentWillUnmount = () => {

  }

  onChange = (e: any) => {
    this.setState({ [e.target.name]: e.target.value }, () => {
      if (this.timeout) clearTimeout(this.timeout)
      this.timeout = setTimeout(() => this.onGetUsers(), 1500)
    })
  }

  showModalDetail = (user: UserType) => {
    this.setState({ selectedUser: user, openModal: true })
  }

  onGetUsers = async (
    pageSize: number = 20, offset: number = 0, page: number = 0,
    sorted: Array<{ id: string, desc: boolean }> = [],
 ) => {
    try {
      this.setState({ loading: true })
      console.log('HAHA', pageSize, offset, page)
      const { selectedFilter, searchText,  } = this.state
      // set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = BackendLess.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(2).setSortBy(sortQuery)
      queryBuilder.setWhereClause(`regular != null`)
      if (searchText !== '') 
        queryBuilder.setWhereClause(`email LIKE '%${searchText}%'`)
				// queryBuilder.setWhereClause(`firstname LIKE '%${searchText}%' OR lastname LIKE '%${searchText}%'`)
      if (selectedFilter)
        queryBuilder.setWhereClause(selectedFilter)
       const users: Array<UserType> = await BackendLess.Data.of('Users').find(queryBuilder)
       const count: number = await BackendLess.Data.of('Users').getObjectCount(queryBuilder)
      this.setState({ users, loading: false, pages: count / offset, pageSize: pageSize, page: page }) 
    } catch (err) {
      console.log(err)
      this.setState({ loading: false }) 
    }
  }

  renderPagination = ({ canNext, canPrevious, page, pageSize, sorted }: Object) => {
    const pages = [10, 20, 30, 40, 50]
		return (
			<Row className="pl-3 pr-3">
				<Col className="d-flex flex-row pt-2 pb-2 pr-3 pl-3">	
					<Button
						disabled={!canPrevious} color={canPrevious ?  'success' : 'secondary'} outline
						onClick={() => this.onGetUsers(pageSize, (page - 1) * pageSize, (page - 1), sorted)}
					>
						Previous
					</Button>
					<Input
						type="select" name="pageSize" value={pageSize} className="mr-3 ml-3"
						onChange={(e) => this.onGetUsers(e.target.value, page * e.target.value, page, sorted)}
					>
						{pages.map(num => <option key={`page-${num}`} value={num}>{num} Baris</option>)}
					</Input>
					<InputGroup className="mr-3" >
						<InputGroupAddon addonType="prepend" >Page</InputGroupAddon>
						<Input
							type="number" name="page" value={page}
							onChange={(e) => this.onGetUsers(pageSize, e.target.value * pageSize, e.target.value, sorted)}
						/>
					</InputGroup>
					<Button
						disabled={!canNext} color={canNext ?  'success' : 'secondary'}
						onClick={() => this.onGetUsers(pageSize, (page + 1) * pageSize, (page + 1), sorted)}
					>
						Next
					</Button>
				</Col>
			</Row>
		)
	}


  render() {
    const { users, loading, selectedFilter, searchText, pages, page, pageSize, openModal, selectedUser } = this.state
    return (
      <div>

        <Card>
          <CardBody>
            <Row>
              <Col sm="12" xs="12" md="4" lg="4" xl="4">
                <CardTitle>List User</CardTitle>
                <CardSubtitle>Data Customer Triplogic</CardSubtitle>
              </Col>
              <Col sm="12" xs="12" md="8" lg="8" xl="8">
                <Row className="justify-content-end align-items-end">
                  <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                    <Input
                      type="select" className="custom-select" placeholder="Filter"
                      onChange={e => this.onChange(e)}
                      name="selectedFilter" value={selectedFilter}
                    >
                      <option value="0">Filter Berdasarkan</option>
                      {selectOptions.map(opt => <option key={opt.value} value={opt.value}>{opt.title}</option>)}
                    </Input>
                  </Col>
                  <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                    <InputGroup style={{ maxHeight: 36 }}>
                      <Input
                        type="text" placeholder="Cari User" name="searchText"
                        onChange={e => this.onChange(e)} value={searchText}
                      />
                      <InputGroupAddon addonType="append">
                        <Button color="primary">
                          <i className="ti-search"></i>
                        </Button>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>
                </Row>
              </Col>

            </Row>
          </CardBody>
          <CardBody>
            <ReactTable
              manual
              pages={pages}
              page={page}
              columns={tableHeaders.concat([
                {
                  Header: 'Actions',
                  sortable: false, 
                  accessor: 'actions',
                  Cell: ({ original }: { original: UserType }) => (
                    <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
                      <Button className="btn m-1" outline color="info" onClick={() => this.showModalDetail(original)}>
                        <i className="fa fa-eye" id="tlp1"></i> Open Profile
                      </Button>
                    </div>
                  )
                }
              ])}
              defaultPageSize={20}
              pageSize={pageSize}
              showPaginationBottom={true}
              className="-striped -highlight"
              data={users}
              loading={loading}
              filterable={false}
              onFetchData={(tableState: Object) => {
                console.log("adsf",tableState.pageSize)
                console.log("QWER",tableState.page)
                console.log("QWERqw",tableState.pages)
                this.onGetUsers(tableState.pageSize, page * tableState.pageSize, page, tableState.sorted)
              }}
              PaginationComponent={this.renderPagination}
            />
          </CardBody>
        </Card>
        {openModal &&
          <UserDetailModal
            toggle={() => this.setState({ openModal: false })}
            user={selectedUser}
            isOpen={openModal}
          />
        }
      </div>
    );
  }
}

export default UsersTable;
