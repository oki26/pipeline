# Dashboard Triplogic
This is admin dashboard triplogic to handling orders. It uses [React](https://reactjs.org/) and [AdminTemplate]( https://drive.google.com/open?id=1KGCXTTMqec3He_OPdObZpY-EXUy563mQ). If you want to make UI, please search it first from the tamplate and copy it. this project just using redux a bit because every screen has different state.

## Kick start the project
  * After clone this project, run `npm install` in project root
  * Start the development server with `npm start`

## Folder structure
   * `src` all the files 
   * `src/routes/routing.jsx` list of route that listed at side bar

        Register the path of your screen here like the other, just follow the existing code.
        But if you want to add a route that not listed at the sidebar, just add a property `hide_menu: true`
        
        example:
        ```javascript
          { path: '/order/:orderID', name: 'Order Detail', hide_menu: true, component: TriplogisticsOrderDetail },
        ```
   * `src/views` the place for all the layout per section that listed in routing file
      <p>If you want to add a new screen, place here</p>
   * `src/components/dashboard-components/` all the components
      
      components in this folder listed at an index.js files so we can aware for all components we have so we can reduce the duplication of the component with easy when doing optimization and cleaning code

   * `src/redux` all the redux actions, constants and reducers placed here.
   * `src/services` contains the backendless and rest api service

## Production Build
  * Open the `package.json` file and edit the `homepage` property with the url you want
  * run the command `npm run build`, then production build files will available at `build` folder
  * redirect the domain to `build` folder or move the it's content to domain root folder