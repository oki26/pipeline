import React from "react";
import {
	Row, Col
} from 'reactstrap';
import { TriplogicSummary } from 'components/dashboard-components';

class Home extends React.Component {
	render() {
		return (
			<div>
			  <Row>
					<Col xs={12}>
						<TriplogicSummary />
					</Col>
				</Row>
			</div>
		);
	}
}

export default Home;
