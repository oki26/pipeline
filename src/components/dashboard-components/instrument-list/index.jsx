// @flow
import type { InstrumentType } from '../../../types'
import ReactTable from 'react-table'
import React from "react";
import moment from 'moment'
// import img1 from '../../../assets/images/users/1.jpg';

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Input,
  // InputGroup,
  // InputGroupAddon,
  Row, Col,
  Modal,
  ModalHeader,
  ModalBody,
  Table
} from 'reactstrap';

export type Props = {
  title: string,
  subtitle: string,
  instruments: Array<InstrumentType>,
  loading: boolean,
  onChangeSearch: Function,
  onGetFeeders: Function,
  onChangeRegions: Function,
  onChangeStatus: Function,
  selecedRegion: string,
  searchText: string,
  selectedStatus: string,
  regionOptions: Array<string>,
  onPressLocation: (instrument: InstrumentType) => void
}

export type State = {
  modal: boolean,
  detailInstrument: ?InstrumentType
}

const tableHeaders = [
	{
		Header: 'origin',
		accessor: 'origin',
		Cell: ({ original }) => (
      <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
        {original.origin}
      </div>
		)
  },
  {
		Header: 'departure time',
		accessor: 'departure_time',
		Cell: ({ original }) => (
      <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
        {moment(original.departure_date).format("DD/MM/YYYY HH:mm")}
      </div>
		)
  },
  {
		Header: 'destination',
		accessor: 'destination',
		Cell: ({ original }) => (
      <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
        {original.destination}
      </div>
		)
  },
  {
		Header: 'arrival time',
		accessor: 'arrival_time',
		Cell: ({ original }) => (
      <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
        {moment(original.departure_date).format("DD/MM/YYYY HH:mm")}
      </div>
		)
  },
	{ 
		Header: 'baggage',
		accessor: 'available_baggage',
		Cell: ({ original }) => (
			<div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
				{`${original.available_baggage} kg`}
			</div>
		)
  },
  { 
    Header: 'used',
    accessor: 'is_used',
    Cell: ({ original }) => (
      <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
          <i className={`fa fa-circle ${original.is_used ? 'text-success' : 'text-secondary'}`} id="tlp1"></i>
      </div>
    )
  },
]

class InstrumentList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      modal: false,
      detailInstrument: null
    };

  }

  toggleOpen = (original: InstrumentType) => {
    this.setState({
      modal: true,
      detailInstrument: original
    });
  }

  toggleClose = () => {
    this.setState({
      modal: false,
      detailInstrument: null
    });
  }

  render() {
    const {
      title, subtitle, instruments, loading,
      onGetFeeders, onChangeStatus, selectedStatus
     } = this.props;

     const {modal, detailInstrument} = this.state
     console.log(detailInstrument)
    return (
      <Card>
        <CardBody>
          <Row>
            <Col sm="12" xs="12" md="4" lg="4" xl="4">
              <CardTitle>{title}</CardTitle>
              <CardSubtitle>{subtitle}</CardSubtitle>
            </Col>
            <Col sm="12" xs="12" md="8" lg="8" xl="8">
              <Row>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  {/* <Input
                    type="select" className="custom-select" placeholder="Pilih Wilayah"
                    onChange={e => onChangeRegions(e.target.value)}
                    value={selecedRegion}
                  >
                    <option value="">Kota Keberangkatan</option>
                      {regionOptions.map((region, idx) =>
                        <option value={region} key={`region-${idx}`}>{region}</option>
                      )}
                  </Input> */}
                </Col>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  {/* <InputGroup style={{ maxHeight: 36 }}>
                    <Input
                      type="text" placeholder="Cari Tiket"
                      onChange={e => onChangeSearch(e.target.value)} value={searchText}
                    />
                    <InputGroupAddon addonType="append">
                      <Button color="primary" onClick={() => onChangeSearch(searchText)}>
                        <i className="ti-search"></i>
                      </Button>
                    </InputGroupAddon>
                  </InputGroup> */}
                </Col>
                <Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
                  <Input
                    type="select" className="custom-select" placeholder="Status Feeder"
                    onChange={e => onChangeStatus(e.target.value)}
                    value={selectedStatus}
                  >
                    <option value="">See Ticket Status</option>
                    <option value="is_used = true">Used</option>
                    <option value="is_used = false">not yet used</option>
                    {/* <option value="blocked = true">Blokir</option> */}
                  </Input>
                </Col>
              </Row>
            </Col>

          </Row>
        </CardBody>
        <CardBody>
          <ReactTable
            columns={tableHeaders
              .concat([
              
              {
                Header: 'Actions',
                sortable: false, 
                accessor: 'actions',
                Cell: ({ original }) => (
                  <div className="d-flex justify-content-center align-items-center" style={{ height: '100%' }}>
                    <Button className="btn m-1" outline color="info" onClick={() => this.toggleOpen(original)}>
                      <i className="fa fa-eye" id="tlp1"></i> Detail
                    </Button>
                  </div>
                )
              }
            ])
            }
            pages={-1}
            defaultPageSize={20}
            showPaginationBottom={true}
            className="-striped -highlight"
            data={instruments}
            loading={loading}
            filterable={false}
            onFetchData={(tableState: Object) => 
              onGetFeeders(tableState.pageSize, tableState.page * tableState.pageSize, tableState.sorted)
            }
          />
          {detailInstrument &&
            <Modal isOpen={modal} backdrop={"static"} >
              <ModalHeader toggle={this.toggleClose}>Detail</ModalHeader>
              <ModalBody>
              <Table hover>
                <tbody>
                  <tr>
                    <th>Departure date</th>
                    <td>{moment(detailInstrument.departure_date).format("DD MMMM YYYY HH:mm")}</td>
                  </tr>
                  <tr>
                    <th>Arrival date</th>
                    <td>{moment(detailInstrument.arrival_date).format("DD MMMM YYYY HH:mm")}</td>
                  </tr>
                  <tr>
                    <th>from</th>
                    <td>{detailInstrument.origin}</td>
                  </tr>
                  <tr>
                    <th>to</th>
                    <td>{detailInstrument.destination}</td>
                  </tr>
                  <tr>
                    <th>traveler name</th>
                    <td style={{color: detailInstrument.traveler ? '' : 'red' }}>
                      {detailInstrument.traveler ? `${detailInstrument.traveler.first_name} ${detailInstrument.traveler.last_name}`: "not available"}
                    </td>
                  </tr>
                  <tr>
                    <th>phone number</th>
                    <td style={{color: detailInstrument.traveler && detailInstrument.traveler.phone_number ? '' : 'red'}}>
                      {detailInstrument.traveler && detailInstrument.traveler.phone_number ? detailInstrument.traveler.phone_number : 'not available'}
                    </td>
                  </tr>
                </tbody>
              </Table>
              </ModalBody>
              {/* <ModalFooter>
                <Button color="primary" onClick={this.toggleClose}>Do Something</Button>{' '}
                <Button color="secondary" onClick={this.toggleClose}>Cancel</Button>
              </ModalFooter> */}
            </Modal>
          }
        </CardBody>
      </Card>
    );
  }
}

export default InstrumentList;
