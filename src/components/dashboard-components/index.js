import Statistics from './statistics/statistics';
import SalesSummary from './sales-summary/sales-summary';
import EmailCampaign from './email-campaign/email-campaign';
import ActiveVisitors from './active-visitors/active-visitors';
import Stats from './stats/stats';
import Projects from './projects/projects';
import RecentComment from './recent-comment/recent-comment';
import Chat from './chat/chat';
import CryptoCoins from './crypto-coins/crypto-coins';
import CryptoChart from './crypto-chart/crypto-chart';
import CryptoOrder from './crypto-order/crypto-order';
import CryptoExchange from './crypto-exchange/crypto-exchange';
import CryptoTrade from './crypto-trade/crypto-trade';
import CryptoTrade2 from './crypto-trade2/crypto-trade2';
import CryptoMarket from './crypto-market/crypto-market';
import Earnings from './earnings/earnings';
import EarningsOverview from './earnings-overview/earnings-overview';
import Sales from './sales/sales';
import Orders from './orders/orders';
import Reviews from './reviews/reviews';
import CampaignOverview from './campaign-overview/campaign-overview';
import SalesRatio from './sales-ratio/sales-ratio';
import Visits from './visits/visits';
import OrderStatus from './order-status/order-status';
import TaskList from './tasklist/tasklist';
import UserProfile from './user-profile/user-profile';
import UserProfile2 from './user-profile2/user-profile2';
import BrowserStats from './browser-stats/browser-stats';
import Feeds from './feeds/feeds';
import Weather from './weather/weather';
import TriplogicSummary from './triplogic-summary'
import RegionDemand from './region-demand'
import TotalRevenue from './total-revenue'
import FeedersList from './feeders-list'
import TotalFeeders from './total-feeders'
import TriplogisticsOrderCard from './triplogistics-order-card'
import DragableCard from './dragable-card'
import InstrumentList from './instrument-list'
import TotalInstrument from './total-instrument'
import ModalDetailTravel from './modal-detail-travel'
import UserTable from './users-table'
import UserDetailModal from './user-detail-modal'
import ManifestProviewModal from './manifest-preview-modal'
import BarcodeCard from './barcode-card'
import CreateManifestModal from './create-manifest-modal'
import ModalDetailOrder from './modal-detail-order'
import ModalDetailFlight from './modal-detail-flight'
import ModalUserBalance from './modal-user-balance'
import ModalUserPoint from './modal-user-point'
import ModalChangeOrder from './modal-change-order'
import ModalChangePayment from './modal-change-payment'
import ModalChangePaymentTravel from './modal-change-payment-travel'

export {
    Statistics,
    SalesSummary,
    EmailCampaign,
    ActiveVisitors,
    Stats,
    Projects,
    RecentComment,
    Chat,
    CryptoCoins,
    CryptoChart,
    CryptoOrder,
    CryptoExchange,
    CryptoTrade,
    CryptoTrade2,
    CryptoMarket,
    Earnings,
    EarningsOverview,
    Sales,
    Orders,
    Reviews,
    CampaignOverview,
    SalesRatio,
    Visits,
    OrderStatus,
    TaskList,
    UserProfile,
    UserProfile2,
    BrowserStats,
    Feeds,
    Weather,
    TriplogicSummary,
    RegionDemand,
    TotalRevenue,
    FeedersList,
    TriplogisticsOrderCard,
    TotalFeeders,
    DragableCard,
    InstrumentList,
    ModalDetailTravel,
    UserTable,
    UserDetailModal,
    BarcodeCard,
    ManifestProviewModal,
    CreateManifestModal,
    TotalInstrument,
    ModalDetailOrder,
    ModalDetailFlight,
    ModalUserBalance,
    ModalUserPoint,
    ModalChangeOrder,
    ModalChangePayment,
    ModalChangePaymentTravel
};
