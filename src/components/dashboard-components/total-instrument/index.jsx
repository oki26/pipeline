// @flow
import type { FeederType } from '../../../types'
import React from "react";
import {
	Row, Col, Card,
	CardBody, CardTitle
} from 'reactstrap';
// import ReactMapGL, { Marker } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

// import img1 from 'assets/images/users/1.jpg';
// import img2 from 'assets/images/users/2.jpg';
// import img3 from 'assets/images/users/3.jpg';
// import img4 from 'assets/images/users/4.jpg';

export type Props = {
	title: string,
	feeders: Array<FeederType>,
	totalInstrument: number,
	focusLongitude: ?number,
	focusLatitude: ?number,
}
export type State = {
	viewport: {
		width: number,
		height: number,
		latitude: number,
		longitude: number,
		zoom: number,
	}
}

class TotalFeeders extends React.PureComponent<Props, State> {
	constructor(props: Props) {
    super(props);
    this.state = {
      viewport: {
        width: 700,
        height: 300,
        latitude: -0.789275,
        longitude: 113.921327,
        zoom: 3
			},
    };
	}

	componentDidUpdate(prevProps: Props) {
		const {viewport} = this.state
		const {focusLongitude, focusLatitude, totalInstrument} = this.props;
		const conditionTwo = prevProps.focusLongitude !== focusLongitude;
		const conditionThree = prevProps.focusLatitude !== focusLatitude;
		if (focusLongitude && focusLatitude && conditionTwo && conditionThree) 
			this.setState({
				viewport: {...viewport, longitude: focusLongitude, latitude: focusLatitude, zoom: 14 }
			})
		if (prevProps.totalInstrument !== totalInstrument) {
			this.setState({})
		}
	}

	render() {
		const { title, totalInstrument } = this.props
		return (
			<Col className="p-0">
				<Card>
					<Row>
						<Col sm="12" lg="4">
							<CardBody>
								<CardTitle>{title}</CardTitle>
								<h2 className="font-medium mt-5 mb-0">{totalInstrument}</h2>
								{/* <span className="text-muted">This month we got 346 New Feeders</span> */}
								{/* <div className="image-box mt-4 mb-4">
									<a href="" className="mr-2" id="TooltipExample">
										<img src={img1} className="rounded-circle" width="45" alt="user" />
									</a>
									<a href="" className="mr-2" id="TooltipExample1">
										<img src={img2} className="rounded-circle" width="45" alt="user" />
									</a>
									<a href="" className="mr-2" id="TooltipExample2">
										<img src={img3} className="rounded-circle" width="45" alt="user" />
									</a>
									<a href="" className="mr-2" id="TooltipExample3">
										<img src={img4} className="rounded-circle" width="45" alt="user" />
									</a>
								</div> */}
								{/* <a href="" className="font-bold">Download Report Excel</a> */}
							</CardBody>
						</Col>
						{/* <Col sm="12" lg="8" className="border-left">
							<ReactMapGL
								{...this.state.viewport}
								onViewportChange={(viewport) => this.setState({viewport})}
								mapboxApiAccessToken="pk.eyJ1IjoiZnVhZGl0cm9ja3oiLCJhIjoiY2ptYzF6ajd1MDlyODNxbWlqejZudHlueSJ9.dquqGLznHgTnY25ZD8F9vA"
								mapStyle="mapbox://styles/fuaditrockz/cjnvw465i40dh2soft95an0k7"
							>
								{
									feeders.map((feeder: FeederType) => feeder.current_location &&
										(<Marker
											key={feeder.firstname}
											longitude={feeder.current_location.longitude}
											latitude={feeder.current_location.latitude}
											offsetLeft={-20} offsetTop={-10}
										>
										<i className={`fa fa-circle text-success`} ></i>
										</Marker>)
									)
								}
							</ReactMapGL>
						</Col> */}
					</Row>
				</Card>

			</Col>
		);
	}
}

export default TotalFeeders;
