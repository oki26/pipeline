// @flow
import React from 'react'
import ReactTable from 'react-table'
import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
	Row, Col
} from 'reactstrap';
import moment from 'moment'
import accounting from 'accounting'
import Picker from 'rc-calendar/lib/Picker'
import RangeCalendar from 'rc-calendar/lib/RangeCalendar'
import enUS from 'rc-calendar/lib/locale/en_US'
import Backendless from '../../../services/backendless'
import { ModalDetailFlight, ModalChangePaymentTravel } from '../../../components/dashboard-components';
import type { FlightInvoice } from '../../../types'

type State = {
  dateRange: Array<string>,
  searchText: string,
	flights: Array<Object>,
	loading: boolean,
	modalPayment: boolean,
	modalDetail: boolean,
	detailData: Object
}



const calendar = (
	<RangeCalendar
		showWeekNumber={false}
		dateInputPlaceholder={['Dari Tanggal', 'Sampai Tanggal']}
		defaultValue={[moment().subtract(1, 'month').format(), moment()]}
		locale={enUS}
		disabledDate={current => current.isAfter(moment().add(1, 'days').hour(0))}
	/>
);

export default class Flights extends React.Component<{}, State> {
  tableRef = null
  state = {
    dateRange: [],
    searchText: '',
		flights: [],
		loading: true,
		modalPayment: false,
		modalDetail: false,
		detailData: {}
	}
	
	tableHeaders = [
		{
			Header: 'Tiket ID',
			accessor: 'ticket_id',
		},
		{
			Header: 'Asal',
			headerClassName: 'color-yellow',
			columns: [
				{
					Header: 'Bandara',
					accessor: 'origin'
				},
				{ 
					Header: 'Berangkat',
					id: 'departure_time',
					accessor: (r: Object) => moment(r.departure_time).format('HH:mm, DD-MM-YYYY')
				},
			]
		},
		{
			Header: 'Tujuan',
			columns: [
				{
					Header: 'Bandara',
					accessor: 'destination',
				},
				{
					Header: 'Sampai',
					id: 'arrival_time',
					accessor: (r: Object) => moment(r.arrival_time).format('HH:mm, DD-MM-YYYY')
				},
			]
		},
		{
			Header: 'Invoice',
			headerClassName: 'color-yellow',
			columns: [
				{
					Header: 'Jumlah',
					accessor: (r: Object) => r.invoice ? accounting.formatMoney(r.invoice.amount, 'Rp. ', 0, '.') : null,
					id: 'invoice_amount',
					sortable: false,
					filterable: false,
				},
				{
					Header: 'Deadline',
					accessor: (r: Object) => r.invoice ? moment(r.invoice.deadline).format('HH:mm, DD-MM-YYYY') : null,
					id: 'invoice_deadline',
					sortable: false,
					filterable: false,
				},
				{
					Header: 'Status Pembayaran',
					accessor: (r: Object) => r.invoice ? r.invoice.status : null,
					id: 'status_payment',
					Cell: ({ original }: { original: Object }) => {
						switch (original.invoice ? original.invoice.status : "tidak ada") {
							case "unpaid":
								return (
									<center>
										<Button size="md" color="danger" onClick={() => this.setState({modalPayment: true, detailData: original})}>
											{original.invoice.status}
										</Button>
									</center>
								)
							case "prepaid":
								return (
									<center>
										<Button size="md" color="info" onClick={() => this.setState({modalPayment: true, detailData: original})}>
											{original.invoice.status}
										</Button>
									</center>
								)
							case "paid":
								return (
									<center>
										<Button size="md" color="success" onClick={() => this.setState({modalPayment: true, detailData: original})}>
											{original.invoice.status}
										</Button>
									</center>
								)
							default:
								return (
									<center>
										{console.log(original)}
										<Button size="md" color="link" onClick={() => this.setState({modalPayment: true, detailData: original})}>
											{original.invoice ? original.invoice.status : null}
										</Button>
									</center>
								)
						}
					}
				},
			]
		},
		{ 
			Header: 'Waktu booking',
			id: 'booking_date',
			accessor: (r: Object) => moment(r.booking_date).format('DD-MM-YYYY | HH:mm')
		},
		{
			Header: 'Pembeli',
			id: 'user',
			accessor: (r: Object) => r.user ? `${r.user.first_name} ${r.user.last_name}` : null
		},
		// Action required, open modal to see de detail
		// {
		// 	Header: 'passenger',
		// 	accessor: 'passenger'
		// },
	]

  onChage = (evt: Object) => {
    this.setState({ [evt.target.name]: evt.target.value })
  }

  componentDidMount = () => {
    this.getFlightTickets()
  }

	getFlightTickets = async (
		pageSize: number = 20, offset: number = 0,
		sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		try {
			this.setState({ loading: true })
			const { dateRange } = this.state
			let { searchText } = this.state
			searchText = String(searchText).toLowerCase()

			// set sort clause
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)

			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(pageSize).setOffset(offset).setRelationsDepth(1).setSortBy(sortQuery)

			// filter by date
			if (dateRange.length === 2) {
					// dateRange.sort((a, b) => moment(a).isAfter(b) ? -1 : 1)
					queryBuilder.setWhereClause(`created >= "${moment(dateRange[0]).format('YYYY-MMM-DD')}" and created <= "${moment(dateRange[1]).format('YYYY-MMM-DD')}"`)
			}
			
			// query search
			if (searchText !== '') {
				queryBuilder.setWhereClause(`
					origin LIKE '%${searchText}%' or
					destination LIKE '%${searchText}%' or
					invoice.status LIKE '%${searchText}%' or
					status LIKE '%${searchText}%' or
					user.first_name LIKE '%${searchText}%' or
					user.last_name LIKE '%${searchText}%'
				`)
			}
			const res: Array<FlightInvoice> = await Backendless.Data.of('airplane_ticket_order').find(queryBuilder)
			this.setState({ flights: res, loading: false })
		} catch(er) {
			this.setState({ loading: false })
			console.log(er)
		}
	}

  render() {
    const { dateRange, searchText, flights, loading, modalPayment, modalDetail, detailData } = this.state
    return (
      <div>
        <Card>
					<CardBody>
						<Row>
							<Col sm="12" xs="12" md="4" lg="4" xl="4">
							<CardTitle>
								<i className="mdi mdi-database mr-2"></i> Tiket Pesawat
							</CardTitle>
								<CardSubtitle>List pemesanan tiket pesawat, untuk konfirmasi dan mengirim tiket ke customer menggunakan dashboard tiket.com/affiliate</CardSubtitle>
							</Col>
							<Col sm="12" xs="12" md="8" lg="8" xl="8">
								<Row className="d-flex justify-content-end">
									<Col className="p-1" sm="12" xs="12" md="6" lg="6" xl="6">
									<InputGroup style={{ maxHeight: 36 }}>
										<Picker
											value={dateRange}
											onChange={(val) => this.setState({ dateRange: val }, () => this.getFlightTickets())}
											animation="slide-up"
											calendar={calendar}
										>
											{
												({ value }) => {
													return (
															<Input
																placeholder="Filter Tanggal"
																value={(value[0] && value[1]) ? `${moment(value[0]).format('DD MMM YYYY')}  to  ${moment(value[1]).format('DD MMM YYYY')}` : ''}
															/>);
												}
											}
										</Picker>
										<Button disabled={dateRange.length < 1} onClick={() => this.setState({ dateRange: [] }, () => this.getFlightTickets())}>
											X
										</Button>
									</InputGroup>
									</Col>
									<Col className="p-1" sm="12" xs="12" md="4" lg="4" xl="4">
										<InputGroup style={{ maxHeight: 36 }}>
											<Input
												type="text" placeholder="Cari"
												name="searchText" value={searchText}
												onChange={this.onChage}
											/>
											<InputGroupAddon addonType="append">
												<Button color="primary" 
												// onClick={() => onChangeSearch(searchText)}
												>
													<i className="ti-search"></i>
												</Button>
											</InputGroupAddon>
										</InputGroup>
									</Col>
								</Row>
							</Col>
						</Row>
					</CardBody>
					<CardBody>
						<ReactTable
							loading={loading}
							ref={r => (this.tableRef = r)}
							columns={this.tableHeaders
								// .concat([{
								// Header: "Actions",
								// accessor: "actions",
								// sortable: false,
								// filterable: false,
								// Cell: (row) => (
								// 	<center>
								// 		<Button size="md" color="success" onClick={e => this.setState({modalDetail: true, detailData: row})}>
								// 			Detail
								// 		</Button>
								// 	</center>)
								// }])
							}
							showPaginationBottom={true}
							className="-striped -highlight"
							data={flights}
							onFetchData={(tableState: Object) =>
								this.getFlightTickets(tableState.pageSize, tableState.page * tableState.pageSize, tableState.sorted)
							}
						/>
					</CardBody>
				</Card>
				<ModalChangePaymentTravel
					type={'airplane'}
					toggle={() => this.setState({ modalPayment: false, detailData: {} })}
					isOpen={modalPayment}
					data={detailData}
					onReloadData={() => this.getFlightTickets()}
				/>
				<ModalDetailFlight
					toggle={() => this.setState({ modalDetail: false, detailData: {} })}
					isOpen={modalDetail}
					data={detailData}
					onReloadData={() => this.getFlightTickets()}
				/>
      </div>
    )
  }
}
