import Fulllayout from '../layouts/fulllayout.jsx';
import Blanklayout from '../layouts/blanklayout.jsx';

var indexRoutes = [
    { path: "/authentication", name: "Authentication", component: Blanklayout },
    { path: '/', name: 'Dashboard', component: Fulllayout }
    
];

export default indexRoutes;
