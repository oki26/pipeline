import { combineReducers } from 'redux';
import general from './general'
import expedition from './expedition'
import auth from './auth'

export default combineReducers({
  auth,
  general,
  expedition,
})