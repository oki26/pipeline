import React, { Component } from 'react'
import { UserTable } from '../../../components/dashboard-components'

export default class Users extends Component {
  render() {
    return (
      <div>
        <UserTable />
      </div>
    )
  }
}
