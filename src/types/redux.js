import type { Order } from './index'

export type ActionsType = {
  type: string,
  payload: {
    [key:string]: any
  }
}