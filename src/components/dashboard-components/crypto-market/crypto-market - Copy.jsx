import React from 'react';
// react component for creating dynamic tables
import ReactTable from "react-table";
import {
	Card,
	CardBody,
	CardTitle
} from 'reactstrap';
import "react-table/react-table.css";
import * as data from "./data";

class CryptoMarket extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: data.MarketData.map((prop, key) => {
				return {
					id: key,
					coin: prop[0],
					currency: prop[1],
					price: prop[2],
					marketcap: prop[3],
					volume1d: prop[4],
					change1d: prop[5],
					change1w: prop[6]
				};
			})
		};
	}
	render() {
		return <div>
			{/*--------------------------------------------------------------------------------*/}
            {/* Used In Dashboard-3 [Cryptocurrency]                                           */}
            {/*--------------------------------------------------------------------------------*/}
			<Card>
				<CardBody className="border-bottom">
					<CardTitle><i className="mdi mdi-border-right mr-2"></i>Crypto Market</CardTitle>
				</CardBody>
				<CardBody>
					<ReactTable

						columns={[
							{
								Header: "#",
								accessor: "coin",
								sortable: false,
								filterable: false
							},
							{
								Header: "Currency",
								accessor: "currency"
							},
							{
								Header: "Price",
								accessor: "price"
							},
							{
								Header: "Market Cap",
								accessor: "marketcap"
							},
							{
								Header: "Volume 1D",
								accessor: "volume1d"
							},
							{
								Header: "Change % (1D)",
								accessor: "chnage1d"
							},
							{
								Header: "Change % (1W)",
								accessor: "change1w"
							}
						]}
						defaultPageSize={10}

						showPaginationBottom={true}
						className="-striped -highlight"
						data={this.state.data}
						filterable
					/>
				</CardBody>
			</Card>
			{/*--------------------------------------------------------------------------------*/}
			{/*End Inner Div*/}
			{/*--------------------------------------------------------------------------------*/}
		</div>
	}
}

export default CryptoMarket;
