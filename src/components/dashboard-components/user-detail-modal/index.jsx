// @flow
import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col } from 'reactstrap'
import type { UserType } from '../../../types'
import moment from 'moment'
import accounting from 'accounting'
import img1 from '../../../assets/images/users/1.jpg';

export type Props =  {
  user: UserType,
  title?: string,
  toggle: Function,
  isOpen: boolean
}

export default class UserDetailModal extends Component<Props> {
  render() {
    const { isOpen, toggle, title, user } = this.props
    return (
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}>{title || `${user.regular.first_name} ${user.regular.last_name}`}</ModalHeader>
        <ModalBody>
          <Row>
            <Col className="p-3 d-flex justify-content-center align-items-center">
              <img alt="User Profile" src={user.regular.avatar_show || img1} style={{ height: 200, width: 300, borderRadius: 10 }}/>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Nama</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{`${user.regular.first_name} ${user.regular.last_name}`}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Saldo</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{accounting.formatMoney(user.regular.balance, 'Rp.', 0, '.')}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Point</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.points ? user.regular.points : 0 }</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Nomor Telepon</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.phone_number}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Email</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.email}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Jenis Keamin</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.gender ? user.regular.gender : "Tidak Ada"}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Tanggal Lahir</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.birthdate !== null ? moment(user.regular.birthdate).format('DD-MMM-YYYY') : 'Tidak ada'}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Status Aktif</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.is_active ? 'Ya' : 'Tidak'}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Status Diblokir</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.is_blocked ? 'Ya' : 'Tidak'}</b></Col>
          </Row>
          <Row>
            <Col xs="12" sm="12" md="4" lg="4" xl="4">Terverifikasi</Col>
            <Col xs="12" sm="12" md="8" lg="8" xl="8"><b>{user.regular.is_verification ? 'Ya' : 'Tidak'}</b></Col>
          </Row>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Tutup</Button>
        </ModalFooter>
      </Modal>
    )
  }
}
