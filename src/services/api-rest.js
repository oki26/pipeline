// @flow
import axios from 'axios'
import config from '../config'

const api = axios.create({
   baseURL: config.BASE_URL_REST,
   timeout: 15000,
   headers: {
    'Content-Type': 'application/json',
   }
})

export const postData = async (url: string, data: Object, customConfig?: Object = {}) => {
  api.defaults.headers['Content-Type'] = 'multipart/form';
  const token = localStorage.getItem('token');
  if (token) {
    api.defaults.Authorization = ` Bearer ${token}`
  }
  const formData = new FormData();
  const keys = Object.keys(data);
  keys.forEach((key) => {
    formData.append(key, data[key]);
  });
  return api.post(url, formData, {
    timeout: 45000,
    ...customConfig,
  })
    .then(response => Promise.resolve(response.data))
    .catch(err => Promise.reject(err));
}

export const post = async (url: string, data: Object, customConfig?: Object = {}) => {
  const token = localStorage.getItem('token');
  if (token) {
    api.defaults.Authorization = ` Bearer ${token}`
  }
  return api.post(url, data, customConfig)
    .then(response => Promise.resolve(response.data))
    .catch(err => Promise.reject(err));
}
