// @flow
import type { CardObject } from '../../../types'
import React, { Component } from 'react'
import {  Row, Col, Card, CardTitle, CardText, Button} from 'reactstrap'
import { findDOMNode } from 'react-dom'
import {
	DragSource, DropTarget, ConnectDropTarget,
	ConnectDragSource, DropTargetMonitor, DropTargetConnector,
	DragSourceConnector, DragSourceMonitor
} from 'react-dnd'

export type CardProps = {
	type: string,
	title: string,
	textOne: string,
	textTwo: string,
	index: number,
	readOnly: boolean,
	dataType: string,
	color: string,
	data: Object,
	isDragging: boolean,
	connectDragSource: ConnectDragSource,
	onPressDetail: Function,
	connectDropTarget: ConnectDropTarget
}

const cardSource = {
	beginDrag(props: CardProps) {
		console.log('beginDrag')
		return props
	},
	endDrag(props: CardProps, monitor: DragSourceMonitor, component: ?any) {
		return monitor.didDrop()
	}
}


class DragableCard extends Component<CardProps> {
  render() {
    const {
			title, textOne, textTwo, isDragging, connectDragSource, color, onPressDetail, data
			// readOnly, dataType
		} = this.props
    const opacity = isDragging ? 0 : 1
    return connectDragSource(
				<div>
					<Card className="p-2 dragable-card" body style={{ background: color || '#f2f2f2', opacity }}> 
						<CardTitle>{title}</CardTitle>
						<div>{textOne}</div>
						<div>{textTwo}</div>
						<div style={{ width: '100%' }} className="d-flex justify-content-end align-items-end p2 text-primary">
							<a color={'link'} onClick={() => onPressDetail(data)} >Detail</a>
						</div>
					</Card>
				</div>
		)
  }
}

const collect = (connect: DragSourceConnector, monitor: DragSourceMonitor) => ({
	connectDragSource: connect.dragSource(),
	isDragging: monitor.isDragging(),
})
export default DragSource('DnD', cardSource, collect)(DragableCard)