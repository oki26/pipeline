// @flow
import * as constants from '../constants/general'

export function showAlert(type: string, title: string, message: string) {
  return {
    type: constants.ALERT_SHOW,
    payload: {
      alertMessage: message,
      alertTitle: title,
      alertType: type
    }
  }
}

export function dissmissAlert() {
  return {
    type: constants.ALERT_DISMISS
  }
}

export function setLoading(loading: boolean) {
  return {
    type: constants.LOADING,
    payload: { loading }
  }
}
