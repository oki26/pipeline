//@flow
import Backendless from 'backendless'
import config from '../config'

Backendless.serverURL = config.BACKENDLESS_URL;
Backendless.initApp(config.BACKENDLESS_APP_ID, config.BACKENDLESS_API_KEY);

export default Backendless
