// @flow
import type { FlightOrder } from '../../../types'
import React from 'react'
import { 
  CustomInput, Collapse, Modal, 
  ModalHeader, ModalBody, ModalFooter, 
  FormGroup, Input, Label, TabContent, 
  TabPane, Nav, NavItem, NavLink, Card, 
  Button, CardTitle, CardText, Row, Col
} from 'reactstrap';
import Backendless from '../../../services/backendless'
import classnames from 'classnames';
import accounting from 'accounting';
import moment from 'moment';
import Lightbox from 'react-lightbox-component';

type Props = {
  amount: string,
  type: string,
  payment_status: string,
  data: FlightOrder,
  isOpen: boolean,
  toggle: Function,
  onReloadData: Function,
  className?: string,
}

type State = {
  payment_status: string,
  order_status: string,
  receiver_name: string,
  receiver_time: string,
  activeTab: string
}

class ModalChangePaymentTravel extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      payment_status: "",
      order_status: "",
      activeTab: '1',
      receiver_name: '',
      receiver_time: ''
    }
  }

  toggleTab = (tab: string) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onSubmitPaymentStatus = async () => {
    const {data, toggle, onReloadData, type} = this.props
    const {payment_status} = this.state
    if (payment_status !== "") {
      if(type === 'airplane'){
        await Backendless.Data.of('airplane_ticket_invoices').save({objectId: data.invoice.objectId, status: payment_status})
      }else if (type === 'hotel') {
        await Backendless.Data.of('hotel_invoices').save({objectId: data.invoice.objectId, status: payment_status})        
      }
      await toggle()
      onReloadData()
    }else{
      toggle()
    }
  }

  render(){
    const {isOpen, toggle, className, data} = this.props
    const {payment_status, order_status, receiver_name, receiver_time} = this.state

    console.log(data)
    return(
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop>
        <ModalHeader toggle={toggle}>Payment Status</ModalHeader>
        <ModalBody>
          <FormGroup className="mt-2">
            <Label for="cargo_name">Jumlah Tagihan</Label>
            <Input disabled name="amount" value={accounting.formatMoney(data.invoice ? data.invoice.amount : 0, 'Rp. ', 0, '.')}
              id="amount" placeholder="Jumlah Penagihan" />
          </FormGroup>
          <FormGroup className="mt-2">
            <Row>
              <Col sm="6">
                <Label for="cargo_name">Bukti Transfer</Label>
              </Col>
              <Col sm="6">
                {data.invoice ? data.invoice.attachment ? (
                    <a href={data.invoice.attachment} target="_blank" >
                      <img style={{width:'100%', height:'auto'}} src={data.invoice.attachment} alt="Bukti Transfer" />
                    </a>
                  ) 
                  : (
                    <p style={{color:"#ff0000"}} >User Belum Mengupload Bukti Transfer</p>
                  ) 
                  : (
                    <p style={{color:"#ff0000"}} >User Belum Mengupload Bukti Transfer</p>
                  )
                }
              </Col>
            </Row>
          </FormGroup>
          <FormGroup className="mt-2">
            <Label for="cargo_name">Status Pembayaran</Label>
            <Input type="select" id="payment_status" 
              onChange={e => this.setState({ payment_status: e.target.value })} value={payment_status !== '' ? payment_status : data.invoice ? data.invoice.status : ""} >
              <option value="unpaid" >unpaid</option>
              <option value="prepaid" >prepaid</option>
              <option value="paid" >paid</option>
              <option value="cancelled">cancelled</option>
              <option value="expired">expired</option>
            </Input>
          </FormGroup>
          <Button color="primary" onClick={this.onSubmitPaymentStatus} >Update Payment Status</Button>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle} >cancel</Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default ModalChangePaymentTravel