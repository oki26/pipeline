import { ALERT_SHOW, ALERT_DISMISS, LOADING } from '../constants/general'

const initialState = {
  alertMessage: '',
  alertTitle: '',
  alertShow: false,
  alertType: 'info',
  loading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
  case ALERT_SHOW:
    return {
      ...state,
      alertShow: true,
      alertMessage: action.payload.alertMessage,
      alertTitle: action.payload.alertTitle,
      alertType: action.payload.alertType,
    }
  case ALERT_DISMISS: 
    return {
      ...state,
      alertMessage: '',
      alertTitle: '',
      alertShow: false,
      alertType: 'info',
    }
    case LOADING: 
      return { ...state, loading: action.payload.loading }
  default:
    return state
  }
}
