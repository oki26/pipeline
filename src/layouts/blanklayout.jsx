//@flow
import React from 'react';
import Backendless from 'backendless'
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux'
import authRoutes from '../routes/authroutes.jsx';
import * as actions from '../redux/actions/authentication'

type State = {
  loading: boolean
}

type Props = {
  account: {
    email: string
  },
  login: (email: string, password: string) => void,
  password: string
}

class Blanklayout extends React.Component<Props, State> {
  
  constructor(props: Props){
    super(props)
    this.state={
      loading: true
    }
  }

  async componentDidMount(){
    if (this.props.account.email !== undefined) {
      await this.props.login(this.props.account.email, this.props.password)
      window.location.replace("/")
    } else {
      this.setState({loading: false})
    }
  }

  render() {

    if(this.state.loading){
      return(
        <div
					id="main-wrapper"
				>
				<div className="page-wrapper d-block" style={{minHeight: '100vh'}} >
					<div className="page-content container-fluid">
					<div className="loading">
						<div className="loading center">
							<div className="loading-bar">
							</div>
						</div>
					</div>
				</div>
				</div>
				</div>   
      )
    }

    return (
    <div className="authentications">
      <Switch>
            {authRoutes.map((prop, key) => {
              if (prop.redirect)
                return (
                  <Redirect from={prop.path} to={prop.pathTo} key={key} />
                );
              return (
                <Route path={prop.path} component={prop.component}  key={key} />
              );
            })}
          </Switch>
    </div>
    
    )
  }
}


const mapStateToProps = (store) => ({
  account: store.auth.account,
  password: store.auth.passwordAccount
})

const mapDispatchToProps = {
  login: actions.login,
}

export default connect(mapStateToProps, mapDispatchToProps)(Blanklayout)