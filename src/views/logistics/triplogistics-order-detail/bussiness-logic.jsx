// @flow
import React, { Component } from 'react'
import type { OrderType, TravelType, FeederType, TravelCarType, TripType, CardObject } from '../../../types'
import moment from 'moment'
import Backendless from '../../../services/backendless'
export const FEEDERS = 'feeders'
export const AIRLINE_TRAVELER = 'people_airplane_travel'
export const CAR_TRAVELER = 'people_car_travel'
export const TRAIN_TRAVELER = 'people_train_travel'
export const CARGO = 'cargo_airplane_travel'

export type Props = {
  match: {
    params: {
      orderID: string
    }
  }
}

export type State = {
  order: ?OrderType,
  feederOrigins: Array<FeederType>,
  feederDests: Array<FeederType>,
  loadingFeeder: boolean,
  loadingOrder: boolean,
  loadingSubmit: boolean,
  alertMessage: string,
  alertType: string,
  trips: Array<TripType>,
  modalType: string,
  travel: ?TravelType,
  feeder: ?FeederType,
  cargo: ?Object,
  people_airplane_travel: Array<TravelType>,
  people_car_travel: Array<TravelCarType>,
  people_train_travel: Array<TravelType>,
  cargo_airplane_travel: Array<Object>,
  people_airplane_travel_loading: boolean,
  people_car_travel_loading: boolean,
  people_train_travel_loading: boolean,
  cargo_airplane_travel_loading: boolean,
}

export default class OrderDetailLogic extends Component<Props, State> {
    state = {
      order: null,
      trips: [],
      feederOrigins: [],
      feederDests: [],
      loadingFeeder: true,
      loadingOrder: true,
      loadingSubmit: false,
      alertMessage: '',
      alertType: 'warning',
      people_airplane_travel: [],
      people_car_travel: [],
      people_train_travel: [],
      cargo_airplane_travel: [],
      people_airplane_travel_loading: false,
      people_car_travel_loading: false,
      people_train_travel_loading: false,
      cargo_airplane_travel_loading: false,
      feeder: null,
      travel: null,
      cargo: null,
      modalType: '',
    }

    async componentDidMount() {
      try {
        const order =  await this.getOrderById(this.props.match.params.orderID)
        this.setState({ order, loadingOrder: false, trips: order.trips })
        this.getFeeders(order)
        this.gatMatchTraveler(order, AIRLINE_TRAVELER)
        this.gatMatchTraveler(order, TRAIN_TRAVELER)
        this.gatMatchTraveler(order, CAR_TRAVELER)
        this.getCargoList(order)
      } catch (er) {
        this.setState({
          loadingFeeder: false,
          people_airplane_travel_loading: false,
          people_car_travel_loading: false,
          people_train_travel_loading: false,
          cargo_airplane_travel_loading: false,
        })
      }
    }

    getOrderById = async (objectId: string) => {
      try {
        const order: OrderType = await Backendless.Data.of('triplogistic_orders').findById({
          objectId, loadRelations: 'from.city,owner,package,route_price,to.city,trips'
        })
        return order
      } catch (er) {
        return Promise.reject(er)
      }
    }
    
    getFeederByTrip = async (trip: TripType) => {
      try {
        const job = Backendless.Data.of('job')
        const query = Backendless.DataQueryBuilder.create()
        // query.setWhereClause(`job[trip].objectId = ${trip.objectId ? trip.objectId:''}`).setRelationsDepth(2)
        query.setWhereClause(`job[trip].objectId = '${'25B9380D-D307-2E71-FFAB-09832BF9DA00'}'`).setRelationsDepth(2)
        const jobFound = await job.find(query)
        if (jobFound.length < 1) {
          return null
        }
        if (!jobFound[0].feeder) {
          throw null
        }
        return jobFound[0].feeder.feeder
      } catch (er) {
        return null
      }
    }
    
    getTravelByTripId = async (tripObjectId: string, tableName: string): Promise<any> => {
      try {
        const tableConnect = Backendless.Data.of(tableName)
        const query = Backendless.DataQueryBuilder.create()
        query.setWhereClause(`${tableName}[trip].objectId = "${tripObjectId}"`).setRelationsDepth(2)
        // query.setWhereClause(`${tableName}[trip].objectId = '${'25B9380D-D307-2E71-FFAB-09832BF9DA00'}'`).setRelationsDepth(2)
        const found = await tableConnect.find(query)
        if (found.length < 1) return null
        return found[0]
      } catch (er) {
        return null
      }
    }
    
    getCargoByTrip = async (trip: TripType): Promise<any> => {
      try {
        const tableConnect = Backendless.Data.of(TRAIN_TRAVELER)
        const query = Backendless.DataQueryBuilder.create()
        // query.setWhereClause(`${TRAIN_TRAVELER}[trip].objectId = '${trip.objectId ? trip.objectId : ""}'`).setRelationsDepth(2)
        query.setWhereClause(`${TRAIN_TRAVELER}[trip].objectId = '${'25B9380D-D307-2E71-FFAB-09832BF9DA00'}'`).setRelationsDepth(2)
        const found = await tableConnect.find(query)
        if (found.length < 1) return null
        return found[0]
      } catch (er) {
    
      }
    }  

  
    setAlert = (alertMessage: string, alertType: string = 'warning', loadingKey: string = 'loadingSubmit') => {
      this.setState({ alertMessage, alertType, [loadingKey]: false })
      // setTimeout(() => this.setState({ alertMessage: '', alertType }), 5000)
    }
  
    /**
     * Getting available feeder at origin and destination city
     */
    getFeeders = async (order: OrderType) => {
      try {
        const { from, to } = order
        const originCity = from.city.name
        const destCity = to.city.name
        const queryOrigin = Backendless.DataQueryBuilder.create()
        const queryDest = Backendless.DataQueryBuilder.create()
        queryOrigin.setWhereClause(`feeder.city.name LIKE '%${originCity}%' and feeder.active = true`).setRelationsDepth(2)
        queryDest.setWhereClause(`feeder.city.name LIKE '%${destCity}%' and feeder.active = true`).setRelationsDepth(2)
        const feederOrigins: Array<Object> = await Backendless.Data.of('Users').find(queryOrigin)
        const feederDests: Array<Object> = await Backendless.Data.of('Users').find(queryDest)
        this.setState({ loadingFeeder: false, feederOrigins, feederDests })
      } catch (er) {
        this.setAlert(`Gagal Mendapatkan Data Feeder: ${er.message}`, 'warning', 'loadingFeeder')
      }
    }
  
    /**
     * Getting available airplane traveler that match with order
     */
    gatMatchTraveler = async (order: OrderType, tableName: string) => {
      try {
        const tableConnect = Backendless.Data.of(tableName)
        const queryBuilderTravel = Backendless.DataQueryBuilder.create()
        const minimumTravel = moment(order.pickup_time).add(4, 'hour').format('x')
        const packageWeight = order.package.weight_packages;
        const originPackage = order.from.city.name
        const destPackage = order.to.city.name
        queryBuilderTravel.setRelationsDepth(1)
        //   .setWhereClause(`
        //   departure_date > ${minimumTravel} and baggage_kg >= ${packageWeight} and
        //   origin LIKE '${originPackage}' and destination LIKE '${destPackage}'
        // `)
        const results: Array<TravelType> = await tableConnect.find(queryBuilderTravel)
        this.setState({ [`${tableName}_loading`]: false, [tableName]: results })
      } catch (er) {
        this.setAlert(`Gagal Mendapatkan Data ${tableName}: ${er.message}`, 'warning', `${tableName}_loading`)
      }
    }
  
    /**
     * Getting available cargo that match with order
     */
    getCargoList = async (order: OrderType) => {
      try {
        const queryBuilderTravel = Backendless.DataQueryBuilder.create()
        const minimumTravel = moment(order.pickup_time).add(4, 'hour').format('x')
        queryBuilderTravel.setWhereClause(`departure_date > ${minimumTravel}`).setRelationsDepth(1)
        const cargos: Array<Object> = await Backendless.Data.of(CARGO).find(queryBuilderTravel)
        this.setState({ cargo_airplane_travel_loading: false, [CARGO]: cargos })
      } catch (er) {
        this.setAlert(`Gagal Mendapatkan Data Cargo: ${er.message}`, 'warning', `${CARGO}_loading`)
      }
    }

    createTripCargoAirplane = async (thirdP: ThirdParty, cargo: CargoAirPlane, trip: Trip): Trip => {
      try {
        if (!cargo.objectId) {
          const savedCargo = await Backendless.Data.of( "cargo_train_travel" ).save( cargo )
          cargo = { ...cargo, ...savedCargo }
        }
        await Backendless.Data.of("cargo_train_travel").setRelation( cargo, "third_party", [thirdP] )
        trip.origin_city = trip.order.from.city
        trip.destination_city = trip.order.to.city
        trip.instrument_handle = "cargo_train_travel"
        trip = await Backendless.Data.of( "trips" ).save( trip )
        await Backendless.Data.of( "cargo_train_travel" ).setRelation( cargo, "trip", [trip] )
        await Backendless.Data.of( "trips" ).setRelation( trip, "order", [trip.order] )
        return trip
      } catch (err) {
        console.log(err)
        return null
      }
  }

  createTripFeederJob = async (user: Object, trip:TripType): ?TripType => {
    const queryBuilder = Backendless.DataQueryBuilder.create()
            .setRelated( [ "feeder" ] );
    if (!user.objectId) {
        queryBuilder.setWhereClause().setWhereClause( `email = '${user.email}'` )
    } else {
        queryBuilder.setWhereClause().setWhereClause( `objectId = '${user.objectId}'` )
    }
    try {
      user = await Backendless.Data.of( "Users" ).find( queryBuilder )
      const job = await Backendless.Data.of( "job" ).save( {} )
      await Backendless.Data.of( "job" ).setRelation( job, "feeder", [user.feeder] )
      await Backendless.Data.of( "feeders" ).addRelation( user.feeder, "job", [job] )
      trip.instrument_handle = "feeders"
      trip = await Backendless.Data.of( "trips" ).save( trip )
      await Backendless.Data.of( "job" ).setRelation( job, "trip", [trip] )
      await Backendless.Data.of( "trips" ).setRelation( trip, "order", [trip.order] )
      return trip
    } catch (err) {
      console.log(err)
      return null
    }
  }

    moveCard = async (idx, id, cardObject: CardObject) => {
      // create trips
      const tripsTable = Backendless.Data.of('trips')
      const { order, trips } = this.state
      let data = null
      if (!order) {
        return null
      }
      if (trips.length < 1 && cardObject.type !== FEEDERS) {
        this.setAlert('Penjemput barang harus feeder')
        return
      }
      const trip: TripType = {
        arrival_location: '',
        arrival_time: '',
        departure_location: '',
        departure_time: '',
        destination_city: '',
        instrument_handle: '',
        is_delivered: '',
        messages: '',
        note: '',
        order: '',
        origin_city: '',
        percentage: '',
        sequence_number: '',
      } 

      switch (cardObject.type) {
        case FEEDERS:
          // create feeder job and trips
          const trip = {};
          await this.createTripFeederJob(cardObject.data, trip)
          this.setState((prevState: State) => {
            const trips = prevState.trips.slice();
            trips.push(cardObject)
            return { trips }
          })
          break;
        case AIRLINE_TRAVELER:
          // create trips
          break;
        case TRAIN_TRAVELER:
          // create trips
          break;
        case CAR_TRAVELER:
          // create trips
          break;
        case CARGO:
          // create trips
          break;
        default:
          break;
      }
      // insert trips to orderTrops state
      // remove instument from instrument list
    }

    removeTripChain = (trip: TripType) => {
      // get instruemnt by trip
      // insert instrument to targeted instrument list
      // delete trips
      // remove trips from trips state
    }

    // moveCard = (dragIdx: number, hoverIdx: ?number, orderTrip: CardObject) => {
      // this.setState((prevState: State) => {
      //   const trips = prevState.trips.slice();
      //   trips.push(orderTrip)
      //   return { trips }
      // })
    // }
  
    onSubmit = async () => {
      const { trips, order } = this.state
      try {
        // apiPost('/services/submitOrderInstrument', {
        //   order: order,
        //   orderInstrument: trips.map(trip => ({ data: trip.data, type: trip.type }))
        // })
      } catch (er) {
  
      }
    }
  
}
