// @flow
import React from "react";
import {
	Row, Col, Container, Alert,
} from 'reactstrap'
import { FeedersList, TotalFeeders } from '../../../components/dashboard-components';
import type { FeederType } from '../../../types'
import Backendless from '../../../services/backendless'

type State = {
	feeders: Array<Object>,
	message: string,
	loading: boolean,
	messageType: string,
	selecedRegion: string,
	selectedStatus: string,
	regionOptions: Array<string>,
	searchText: string,
	selectedFeeder: ?FeederType,
	feederCount: number,
}

class Feeders extends React.Component<{}, State> {
	timeout = null;
	state = {
		feeders: [],
		message: '',
		messageType: 'info',
		loading: false,
		selecedRegion: '',
		selectedStatus: '',
		regionOptions: [],
		searchText: '',
		selectedFeeder: null,
		feederCount: 0,
	}

	componentDidMount = () => {
		this.getCities()
	}

	getCities = async () => {
		try {
			const res = await Backendless.Data.of('cities').find()
			this.setState({
				regionOptions: res.map(city => city.name)
			})
		} catch(er) {
			console.log('fail get citie', er)
			this.setMessage(er.message || 'Gagal mendapatkan list kota', 'warning')
		}
	}
	onChangeParams = (text: string, type: string = 'searchText') => {
		this.setState({ [type]: text }, () => {
			if (this.timeout) clearTimeout(this.timeout)
			this.timeout = setTimeout(this.getFeeders, 1000)
		})
	}

 getFeeders = async (
		 page: number = 20, offset: number = 0,
		 sorted: Array<{ id: string, desc: boolean }> = [],
	) => {
		const { searchText, selecedRegion, selectedStatus } = this.state
		try {
			this.setState({ loading: true })
			// convert sort query to meet requirement backendless
			const sortQuery: Array<string> = sorted.map((s) => `${s.id}${s.desc ? ' DESC': ''}`)
			// apply serverside pagination
			const queryBuilder = Backendless.DataQueryBuilder.create()
			queryBuilder.setPageSize(page).setOffset(offset).setRelationsDepth(1).setSortBy(sortQuery)
			// filter search text
			if (searchText !== '') 
				queryBuilder.setWhereClause(`firstname LIKE '%${searchText}%' OR lastname LIKE '%${searchText}%'`)
			// filter status feeder 
			// query is one of 'active = true', 'active = false', or 'blocked = true'
			if (selectedStatus)
				queryBuilder.setWhereClause(selectedStatus)
			// filter region
			if (selecedRegion)
				queryBuilder.setWhereClause(`city.name LIKE '%${selecedRegion}%'`)
			const res: Array<FeederType> = await Backendless.Data.of('feeders').find(queryBuilder)
			const count: number = await Backendless.Data.of('feeders').getObjectCount()
			this.setState({ feeders: res, loading: false, feederCount: count })
		} catch (er) {
			console.log(er)
			this.setMessage(er.message || 'Gagal menghubungi server', 'warninng')
		}
	}

	setMessage(msg: string, type: string = 'info') {
		this.setState({ message: msg, loading: false, messageType: type })
		setTimeout(() => this.setState({ message: '' }), 5000)
	}

	render() {
		const {
			feeders, loading, message, messageType, searchText,
			regionOptions, selecedRegion, selectedStatus, feederCount,
			selectedFeeder,
		} = this.state
		let focusLongitude = null;
		let focusLatitude =  null;
		if (selectedFeeder) {
			focusLongitude = selectedFeeder.current_location ?  selectedFeeder.current_location.longitude : null
			focusLatitude = selectedFeeder.current_location ?  selectedFeeder.current_location.latitude : null
		}
		return (
			<Container fluid>
				<Row>
					<Alert isOpen={message !== ''} color={messageType}>{message}</Alert>
				</Row>
				<Row>
						<h5 className="mb-4">Feeders</h5>
				</Row>
				<Row>
						<TotalFeeders
							feeders={feeders}
							totalFeeder={feederCount}
							focusLongitude={focusLongitude}
							focusLatitude={focusLatitude}
						/>
				</Row>
				<Row>
					<Col className="p-0">
						<FeedersList
							feeders={feeders}
							loading={loading}
							searchText={searchText}
							onChangeSearch={(text: string) => this.onChangeParams(text, 'searchText')}
							onChangeRegions={(text: string) => this.onChangeParams(text, 'selecedRegion')}
							onChangeStatus={(text: string) => this.onChangeParams(text, 'selectedStatus')}
							onGetFeeders={this.getFeeders}
							selecedRegion={selecedRegion}
							selectedStatus={selectedStatus}
							regionOptions={regionOptions}
							onPressLocation={(feeder: FeederType) =>
								this.setState(
									{ selectedFeeder: feeder },
									() => window.scroll({ top: 0, left: 0, behavior: 'smooth' }
								)
							)}
						/>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default Feeders;
