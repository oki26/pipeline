// @flow
import React, { Component } from 'react'
import { CardBody } from 'reactstrap'
import { DropTarget, DropTargetConnector, DropTargetMonitor } from 'react-dnd';
import { findDOMNode } from 'react-dom'


class CardDropTarget extends Component<Object> {
  render() {  
    const { hovered, connectDropTarget, className } = this.props
    const backgroundColor = hovered ? 'white' : 'red'
    return connectDropTarget(
			<div className={`card-body ${className}`} style={[{ background: backgroundColor, height: '100%', width: '100%' }, this.props.style]}>
				{this.props.children}
			</div>
    )
  }
}

const spec = {
  drop(props: Object, monitor: DropTargetMonitor, component: ?Object) {
		const cardItem = monitor.getItem();

		// block card drop instrument to another instrument
		if (props.type !== 'trips' && props.type !== cardItem.type) {
			return;
		}

		props.moveCard(0,0, {
			type: cardItem.type,
			title: cardItem.title,
			textOne: cardItem.textOne,
			textTwo: cardItem.textTwo,
			data: cardItem.data,
			color: cardItem.color,
		})
	},
	hover(props: Object, monitor: DropTargetMonitor, component: ?Object) {
		return monitor.isOver()
	}
}
const collect = (connect: DropTargetConnector, monitor: DropTargetMonitor) => ({
  connectDropTarget: connect.dropTarget(),
  hovered: monitor.isOver(),
})
export default DropTarget('DnD', spec, collect)(CardDropTarget);