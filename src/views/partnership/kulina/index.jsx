// @flow
import React from "react"
import {
  Container, Row, FormGroup, Label, Input, Button, Card,
  CardImg, CardBody, CardText, CardTitle, CardSubtitle, Col
} from 'reactstrap'
import html2pdf from "html2pdf.js"
import { connect } from "react-redux";
import { RingLoader } from 'react-spinners'
import * as expeditionAction from '../../../redux/actions/expedition'


type Props = {
  loading: boolean,
  getExpeditions: Function,
  barcodes: Array<Object>
} 
type State = {
  fromDate: string,
  toDate: string,
}
class Kulina extends React.Component<Props, State> {
  constructor(props){
    super(props)
    this.state = {
      fromDate: "",
      toDate: "",
    }
  }

  generatePDF = async () => {
    const currentTime = new Date().toISOString()
    let element = document.getElementById("kln-barcodes")
    const pdffileName = `KBPDF-${currentTime}.pdf`
    html2pdf().set(
      {
        margin: 5,
        filename: pdffileName,
        image: { type: "jpeg", quality: 1 },
        html2canvas: { dpi: 192, letterRendering: false },
        pagebreak: {
          mode: [
            'avoid-all'
          ]
        }
      }
    ).from(element).save()
  }

  renderCard = (barcode, idx) => (
    <Col key={`card-${idx}`} xs="12" sm="12" md="3" lg="3" xl="3" className="p-0">
      <Card style={{ marginRight: 2, marginLeft: 2, borderColor: '#aaa', borderWidth: 1 }}>
        <CardImg top width="100%" src={barcode.url} alt={barcode.exped_id} />
        <CardBody>
          <CardTitle>{barcode.exped_id}</CardTitle>
          <CardSubtitle>{barcode.pack_id}</CardSubtitle>
          <CardText>{barcode.name}, {barcode.note}</CardText>
          <CardText>Pengirim: <br/> {barcode.senderName}</CardText>
          <CardText>Penerima: <br/> {barcode.reciverName}</CardText>
          <CardText>Kontak Penerima: <br/> {barcode.receiverPhone}</CardText>
          <CardText>Dari: <br/> {barcode.pickAddr}</CardText>
          <CardText>ke: <br/> {barcode.dropAddr}</CardText>
        </CardBody>
      </Card> 
    </Col>
  )

  render(){
    const { getExpeditions, barcodes, loading } = this.props
    const { fromDate, toDate } = this.state
    console.log(fromDate, toDate)
    return (
      <Container fluid>
        <Row>
          <h4>Cetak Barcode Kulina</h4>
        </Row>
        <Row>
          <FormGroup>
            <Label for="fromDate">Dari Tanggal</Label>
            <Input
              type="date" name="fromDate" id="fromDate" placeholder="Pilih Tanggal"
              onChange={e => this.setState({ fromDate: new Date(e.target.value).toISOString() })}
            />
          </FormGroup>
          <FormGroup className="ml-3 mr-3">
            <Label for="toDate">Ke Tanggal</Label>
            <Input
              type="date" name="toDate" id="toDate" placeholder="Pilih Tanggal" 
              onChange={e => this.setState({ toDate: new Date(e.target.value).toISOString() })}
            />
          </FormGroup>
        </Row>
        <Row>
          <Button
            disabled={fromDate === '' || toDate === ''}
            color="primary"
            onClick={() => getExpeditions(fromDate, toDate)}>
            Cari
          </Button>
          {barcodes.length > 0 &&
            <Button
              disabled={fromDate === '' || toDate === ''}
              color="success"
              className="ml-3"
              onClick={this.generatePDF}>
              Download PDF
            </Button>
          }
        </Row>

        {loading &&
          <Row className="mt-3 d-flex justify-content-center align-items-center" id="kln-barcodes" style={{ minHeight: 400 }} >
            <RingLoader
              sizeUnit={"px"}
              size={100}
              color={'#123abc'}
              loading={loading}
            />
          </Row>
        }

        {!loading && barcodes.length <= 0 &&
          <Row className="mt-3 d-flex justify-content-center align-items-center" id="kln-barcodes" style={{ minHeight: 400 }} >
            <h1>Tidak Ada Data</h1>
          </Row>
        }
      
        <Row className="mt-3" id="kln-barcodes">
          {!loading && barcodes.map((barcode, idx) =>
            this.renderCard(barcode, idx)
          )}
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  loading: state.general.loading,
  barcodes: state.expedition.barcodes
})

const mapDispatchToProps = {
  getExpeditions: expeditionAction.getExpeditions  
}

export default connect(mapStateToProps, mapDispatchToProps)(Kulina)