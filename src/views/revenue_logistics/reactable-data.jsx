const ordersData = [
	["Regular", "11630", "1800100010", "Pending", "Kota Bandung", "Kota Jakarta Pusat", "2018-10-11 19:00:00", "Herman", "Rp 18.644", "Paid"],
	["Regular", "11631", "1800100010", "Success", "Kota Bandung", "Kota Jakarta Timur", "2018-10-11 19:00:00", "Cilok", "Rp 18.644", "Paid"],
	["Regular", "11632", "1800100010", "On Progress", "Kota Jakarta Pusat", "Kota Jakarta Timur", "2018-10-11 19:00:00", "Linda", "Rp 18.644", "Paid"],
	["Regular", "11633", "1800100010", "Pending", "Kota Jakarta Pusat", "Kota Jakarta Barat", "2018-10-11 19:00:00", "Bayu", "Rp 18.644", "Paid"],
	["Regular", "11633", "1800100010", "Pending", "Kota Bogor", "Kota Jakarta Utara", "2018-10-11 19:00:00", "Linda", "Rp 18.644", "Paid"],
	["Regular", "11634", "1800100010", "Success", "Kota Bogor", "Kota Jakarta Barat", "2018-10-11 19:00:00", "Arya", "Rp 18.644", "Paid"],
	["Regular", "11634", "1800100010", "Pending", "Kota Jakarta Pusat", "Kota Jakarta Utara", "2018-10-11 19:00:00", "Linda", "Rp 18.644", "Paid"],
	["Regular", "11635", "1800100010", "On Progress", "Kota Bogor", "Kota Jakarta Pusat", "2018-10-11 19:00:00", "Cilok", "Rp 19.644", "Paid"],
	["Regular", "11636", "1800100010", "Pending", "Kota Bandung", "Kota Jakarta Selatan", "2018-10-11 19:00:00", "Linda", "Rp 18.644", "Paid"],
	["Regular", "11636", "1800100010", "Pending", "Kota Bandung", "Kota Jakarta Selatan", "2018-10-11 19:00:00", "Rehan", "Rp 20.644", "Paid"],
	["Regular", "11636", "1800100010", "Success", "Kota Bekasi", "Kota Jakarta Selatan", "2018-10-11 19:00:00", "Adit", "Rp 18.644", "Paid"],
	["Regular", "11636", "1800100010", "On Progress", "Kota Bali", "Kota Jakarta Barat", "2018-10-11 19:00:00", "Arya", "Rp 23.644", "Paid"],
	["Regular", "11637", "1800100010", "Success", "Kota Bekasi", "Kota Jakarta Timur", "2018-10-11 19:00:00", "Bayu", "Rp 25.644", "Paid"],
	["Regular", "11638", "1800100010", "Pending", "Kota Bali", "Kota Jakarta Selatan", "2018-10-11 19:00:00", "Manda", "Rp 26.644", "Paid"],
	["Regular", "11639", "1800100010", "Pending", "Kota Bali", "Kota Jakarta Barat", "2018-10-11 19:00:00", "Desi", "Rp 27.644", "Paid"],
	["Regular", "11610", "1800100010", "Pending", "Kota Bali", "Kota Jakarta Pusat", "2018-10-11 19:00:00", "Esti", "Rp 39.644", "Paid"],
	["Regular", "11610", "1800100010", "Pending", "Kota Bandung", "Kota Jakarta Pusat", "2018-10-11 19:00:00", "Isa", "Rp 18.644", "Paid"],
]

export { ordersData };
